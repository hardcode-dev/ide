#pragma once

#include "module.h"
extern "C" {
#include "hc_snapshot.h"
}

#include <QWidget>

class ModuleTreeWidget;
class ModuleTabWidget;


class MainWindow: public QWidget
{
    Q_OBJECT

public:
    MainWindow (QWidget* parent = nullptr);

    void loadSnapshot (hc_snapshot_t* snapshot);

private slots:
    void editModule (const QStringList& full_name, const QSharedPointer<Module>& module);

private:
    struct ModuleCallbackUserData {
        MainWindow* _this;
        QStringList& module_name;
    };
    ModuleTreeWidget* module_tree_widget;
    ModuleTabWidget* module_tab_widget;
};
