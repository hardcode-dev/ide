#include "mainwindow.h"
#include "qdebug.h"
#include "moduletreewidgetitem.h"
#include <qobject.h>

#include <QSizePolicy>
#include <QBoxLayout>
#include <QPalette>
#include <QApplication>

MainWindowTemp::MainWindowTemp(QWidget *parent)
    : QWidget{parent}
{

    QVBoxLayout* layout = new QVBoxLayout;
    {
        module_tree_widget = new ModuleTreeWidget ();
        const auto group = QPalette::Active;
        const auto highlight_role = QPalette::Highlight;
        const auto standart_background_role = QPalette::Base;
        auto palette = QApplication::palette();
        QColor current_theme_color_item = palette.color(group, highlight_role);
        QString current_theme_color_background_item = "background-color: " + current_theme_color_item.name() + ";" ;
        QColor current_theme_color_standart = palette.color(group, standart_background_role);
        QString current_theme_color_background_standart = "background-color: " + current_theme_color_standart.name() + ";" ;
        //const QColor& c_under = QColor(47,140,198), const QColor& c_current = QColor(47,140,198),
        //const QColor& c_default = QColor(255,255,255), const QString& bc_current = "background-color: rgb(47,140,198);",
        //const QString& bc_under = "background-color: rgb(47,140,198);", const QString& bc_default = "background-color: rgb(255,255,255);"


        //module_tree_widget->setMouseTracking(true);
        bool c = connect(module_tree_widget, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), this, SLOT(itemChanged(QTreeWidgetItem*,QTreeWidgetItem*)));assert(c);
        c = connect(module_tree_widget, SIGNAL(itemExpanded(QTreeWidgetItem*)), this, SLOT(updateLabelsSize(QTreeWidgetItem*)));assert(c);
        c = connect(module_tree_widget, SIGNAL(itemCollapsed(QTreeWidgetItem*)), this, SLOT(updateLabelsSize(QTreeWidgetItem*)));assert(c);
        c = connect(module_tree_widget, SIGNAL(resizeLabel()), this, SLOT(onWidgetResize()));assert(c);

        {
            QSharedPointer<Module> module(new Module);
            module->_class = "1";
            module->function_class = "2";
            module->function_body = "3";
            module->function_parameters = "4";
            module->function_return_type = "5";
            module_tree_widget->addModule({}, "", module);
            QTreeWidgetItem* item0 = module_tree_widget->topLevelItem(0);
            ModuleTreeWidgetItem* item = new ModuleTreeWidgetItem("MAAAS", item0,
                                                                  current_theme_color_item, current_theme_color_item,
                                                                  current_theme_color_standart, current_theme_color_background_item,
                                                                  current_theme_color_background_item, current_theme_color_background_standart);
            //QTreeWidgetItem* item0 = module_tree_widget->topLevelItem(0);
            //ModuleTreeWidgetItem* item = new ModuleTreeWidgetItem("MasAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            c = connect(item, SIGNAL(addFunction()), this, SLOT(addFunction()));assert(c);
            c = connect(item, SIGNAL(editFunction()), this, SLOT(editFunction()));assert(c);
            c = connect(item, SIGNAL(deleteFunction()), this, SLOT(deleteFunction()));assert(c);
            //item->resizeLabel(10000);
            module_tree_widget->setItemWidget(item0, 0, item);
        }
        {
            QSharedPointer<Module> module(new Module);
            module->_class = "1";
            module->function_class = "2";
            module->function_body = "3";
            module->function_parameters = "4";
            module->function_return_type = "5";
            module_tree_widget->addModule({}, "", module);
            QTreeWidgetItem* item0 = module_tree_widget->topLevelItem(0);
            ModuleTreeWidgetItem* item = new ModuleTreeWidgetItem("ASDSAASDAS", item0,
                                                                  current_theme_color_item, current_theme_color_item,
                                                                  current_theme_color_standart, current_theme_color_background_item,
                                                                  current_theme_color_background_item, current_theme_color_background_standart);
            c = connect(item, SIGNAL(addFunction()), this, SLOT(addFunction()));assert(c);
            c = connect(item, SIGNAL(editFunction()), this, SLOT(editFunction()));assert(c);
            c = connect(item, SIGNAL(deleteFunction()), this, SLOT(deleteFunction()));assert(c);
            //item->resizeLabel(10000);
            module_tree_widget->setItemWidget(item0, 0, item);
            {
                QSharedPointer<Module> module1(new Module);
                module1->_class = "1";
                module1->function_class = "2";
                module1->function_body = "3";
                module1->function_parameters = "4";
                module1->function_return_type = "5";
                module_tree_widget->addModule({}, "", module);
                QTreeWidgetItem* item01 = new QTreeWidgetItem();
                ModuleTreeWidgetItem* item1 = new ModuleTreeWidgetItem("AaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
                                                                       item01,
                                                                       current_theme_color_item, current_theme_color_item,
                                                                       current_theme_color_standart, current_theme_color_background_item,
                                                                       current_theme_color_background_item, current_theme_color_background_standart);
                c = connect(item1, SIGNAL(addFunction()), this, SLOT(addFunction()));assert(c);
                c = connect(item1, SIGNAL(editFunction()), this, SLOT(editFunction()));assert(c);
                c = connect(item1, SIGNAL(deleteFunction()), this, SLOT(deleteFunction()));assert(c);
                item0->addChild(item01);
                //item->resizeLabel(10000);
                module_tree_widget->setItemWidget(item01, 0, item1);
            }
        }
        layout->addWidget(module_tree_widget);
    }
    module_tree_widget->setIndentation(24);
    updateAllLabel();
    setLayout(layout);
}

void MainWindowTemp::itemChanged(QTreeWidgetItem* cur,QTreeWidgetItem* prev)
{
    if (prev) {
        ModuleTreeWidgetItem* pr = qobject_cast<ModuleTreeWidgetItem*>(module_tree_widget->itemWidget(prev, 0));
        if (pr) {
            pr->changeCurrentItem(false);
            pr->setButtonVisibility(false);
        }
    }
    if (cur) {
        ModuleTreeWidgetItem* cr = qobject_cast<ModuleTreeWidgetItem*>(module_tree_widget->itemWidget(cur, 0));
        if (cr) {
            //module_tree_widget->setStyleSheet(CURRENT_BACKGROUND_STYLE_SHEET);
            cr->changeCurrentItem(true);
            cr->setButtonVisibility(true);
        }
    }
}

void MainWindowTemp::addFunction() {
    qWarning() << "TODO add function";
}

void MainWindowTemp::editFunction() {
    qWarning() << "TODO edit function";
}

void MainWindowTemp::deleteFunction() {
    qWarning() << "TODO delete function";
}

void MainWindowTemp::updateAllLabel() {
    int max_size = 0;
    findMaxLabelSize(module_tree_widget->invisibleRootItem(), max_size, -1);
    resizeAllOpenItems(module_tree_widget->invisibleRootItem(), max_size, -1);
}

void MainWindowTemp::updateLabelsSize(QTreeWidgetItem* item){
    updateAllLabel();
}

void MainWindowTemp::onWidgetResize() {
    updateAllLabel();
}

void MainWindowTemp::resizeAllOpenItems(QTreeWidgetItem* item, int max_size, int level){
    if (item) {
        ModuleTreeWidgetItem* cur_item = qobject_cast<ModuleTreeWidgetItem*>(module_tree_widget->itemWidget(item, 0));
        if (cur_item) {
            cur_item->resizeLabel(max_size - (level * 24));
            if (item->isExpanded()) {
                for (int i = 0; i < item->childCount(); ++i) {
                    resizeAllOpenItems(item->child(i), max_size, level+1);
                }
            }
        }
        else {
            if (item->isExpanded() or item == module_tree_widget->invisibleRootItem()) {
                for (int i = 0; i < item->childCount(); ++i) {
                    resizeAllOpenItems(item->child(i), max_size, level+1);
                }
            }
        }
    }
}

void MainWindowTemp::findMaxLabelSize(QTreeWidgetItem* item, int& max_size, int level){
    if (item) {
        ModuleTreeWidgetItem* cur_item = qobject_cast<ModuleTreeWidgetItem*>(module_tree_widget->itemWidget(item, 0));
        if (cur_item) {
            if (cur_item->getLabelSize() + level * 24 > max_size) {
                max_size = cur_item->getLabelSize() + level * 24;
                if (max_size > module_tree_widget->width() - cur_item->getButtonsWidgetWidth() - 48) {
                    max_size = module_tree_widget->width() - cur_item->getButtonsWidgetWidth() - 48;//cur_item->getButtonsWidgetWidth();
                }
            }
            if (item->isExpanded()) {
                for (int i = 0; i < item->childCount(); ++i) {
                      findMaxLabelSize(item->child(i), max_size, level + 1);
                }
            }
        } else {
            if (item->isExpanded() or item == module_tree_widget->invisibleRootItem()) {
                for (int i = 0; i < item->childCount(); ++i) {
                    findMaxLabelSize(item->child(i), max_size, level + 1);
                }
            }
        }
    }
}
