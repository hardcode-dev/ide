#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "moduletreewidget.h"

#include <QLineEdit>
#include <QWidget>
#include <QSplitter>
#include <QTreeWidgetItem>

class MainWindowTemp : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindowTemp(QWidget *parent = nullptr);

public slots:
    void itemChanged(QTreeWidgetItem* current, QTreeWidgetItem* prev);
    void addFunction();
    void editFunction();
    void deleteFunction();
    void updateLabelsSize(QTreeWidgetItem*);
    void onWidgetResize();

private:
    void updateAllLabel();
    ModuleTreeWidget* module_tree_widget;
    void resizeAllOpenItems(QTreeWidgetItem* item, int max, int level);
    void findMaxLabelSize(QTreeWidgetItem* item, int& max, int level);
    QTreeWidgetItem* prev_item = nullptr;
};

#endif // MAINWINDOW_H
