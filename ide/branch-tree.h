#ifndef BRANCHTREE_H
#define BRANCHTREE_H

#include <cstdint>
#include <vector>
#include <memory>
#include <queue>

#include "service.qgrpc.client.h"

class BranchNode {
public:
    BranchNode(uint32_t branch_id, uint32_t commit_id);
    std::vector<std::shared_ptr<BranchNode>> childs;
    std::weak_ptr<BranchNode> parent;
    uint32_t branch_id;
    uint32_t parent_commit_id;
};

class BranchTree
{
public:
    BranchTree() = default;

    BranchTree(const ::google::protobuf::RepeatedPtrField< ::HardCodeStorage::BranchIdWithParent >& branches);

    void buildTree(const ::google::protobuf::RepeatedPtrField< ::HardCodeStorage::BranchIdWithParent >& branches);

    std::shared_ptr<BranchNode> getRoot();
    std::shared_ptr<BranchNode> getCurrent();
    std::weak_ptr<BranchNode> getParent();
    std::vector<std::shared_ptr<BranchNode>>& getChilds();
    std::shared_ptr<BranchNode> findBranch(uint32_t branch_id);
    bool goChildBranch(uint32_t branch_id);
    bool goParentBranch();
    bool goRootBranch();
    void setCurrentBranch(std::shared_ptr<BranchNode> new_current);
    void addChildForCurrent(uint32_t branch_id, uint32_t parent_commit);

private:

    std::shared_ptr<BranchNode> findNodeWhileBuilding(uint32_t branch_id);

    std::shared_ptr<BranchNode> root;
    std::shared_ptr<BranchNode> current;
    std::vector<std::shared_ptr<BranchNode>> builded_nodes;
};

#endif // BRANCHTREE_H
