#ifndef STANDARTTREEITEM_H
#define STANDARTTREEITEM_H

#include <QStandardItem>
#include <QFont>

class StandartTreeItem : public QStandardItem
{
public:
    StandartTreeItem();

    QString GetType();

    StandartTreeItem(QString txt = "", uint32_t font_size = 12, bool set_bold = false, QColor color = QColor(0, 0, 0), QString new_type = "repository");
private:

    QString type;
};

#endif // STANDARTTREEITEM_H
