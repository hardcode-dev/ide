#include "snapshot-manager.h"
#include <QtDebug>

Module::Module(const std::string& new_path, const HardCodeStorage::ModuleContent& new_modification,
       bool new_created, bool new_deleted, const std::string& new_time) {
    setProperties(new_path, new_modification, new_created, new_deleted, new_time);
}

Module::Module(const std::string& new_path, bool new_created,
       bool new_deleted, const std::string& new_time) {
    setProperties(new_path, new_created, new_deleted, new_time);
}

void Module::setContent(HardCodeStorage::ModuleContent& new_modification) {
    content = new_modification;
}

void Module::setProperties(const std::string& new_path, const HardCodeStorage::ModuleContent& new_modification,
                    bool new_created, bool new_deleted, const std::string& new_time) {
    full_path = new_path;
    content = new_modification;
    created = new_created;
    deleted = new_deleted;
    time_version = new_time;
}


void Module::setProperties(const std::string& new_path, bool new_created,
                    bool new_deleted, const std::string& new_time) {
    full_path = new_path;
    deleted = new_deleted;
    created = new_created;
    time_version = new_time;
}

void SnapshotManager::clear(){
    modules.clear();
}

bool SnapshotManager::changeSnapshot(const HardCodeStorage::Commit& commit) {
    HardCodeStorage::Modification a;
    for (const HardCodeStorage::Modification &mod : commit.type()) {
        if (!addModule(mod)) {
            return false;
        }
    }
    return true;
}

bool SnapshotManager::addModule(const HardCodeStorage::Modification& new_module) {
    if (new_module.has_delete_()) {
        if (modules.find(new_module.delete_().module_path(0)) == modules.end()) {
            Module mod(new_module.delete_().module_path(0), false, true);
            modules[mod.full_path] = mod;
        } else {
            modules[new_module.delete_().module_path(0)].deleted = true;
        }
    } else if (new_module.has_create()) {
        if (modules.find(new_module.create().module_path(0)) == modules.end()) {
            Module mod(new_module.create().module_path(0), new_module.create().content(), true, false);
            modules[mod.full_path] = mod;
        } else {
            modules[new_module.create().module_path(0)].created = true;
        }
    } else if (new_module.has_update()) {
        if (modules.find(new_module.update().module_path(0)) == modules.end()) {
            Module mod(new_module.update().module_path(0), new_module.update().content(), false, false);
            modules[mod.full_path] = mod;
            /*mod.setProperties(new_module.update().module_path(0), true);
            create_mod = true;*/ // если с конца идти. Для других модулей также
        } else {
            modules[new_module.update().module_path(0)].content = new_module.update().content();
        }
    }
    return true;
}

Module& SnapshotManager::operator[](const std::string& path){
    return modules[path];
}

Module& SnapshotManager::atModule(const std::string& path) {
    if (modules.find(path) != modules.end()) {
        return modules[path];
    }
    throw std::out_of_range("Out of range in modules");
}

void SnapshotManager::printModules() {
    for (const auto &i : modules) {
        qWarning() << QString::fromStdString(i.first) << " - " << QString::fromStdString(i.second.content.function_body());
    }
    qWarning() << "\n";
}

std::unordered_map<std::string, Module>::iterator SnapshotManager::begin() {
    return modules.begin();
}

std::unordered_map<std::string, Module>::iterator SnapshotManager::end() {
    return modules.end();
}

