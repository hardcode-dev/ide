#include "application.h"

#include "logindialog.h"
#include "mainwindowprev.h"
extern "C" {
#include "hc_snapshot.h"
}

#include <QMessageBox>
#include <QDebug>

void Application::setMonospaceFont (QWidget* widget)
{
    QFont font = widget->font ();
    font.setFamily ("Courier New");
    widget->setFont (font);
}

Application::Application (int& argc, char** argv)
    : QApplication (argc, argv)
{
    if (argc == 2)
        snapshot_dir = argv[1];
}
Application::~Application ()
{
    if (current_window)
        current_window->deleteLater ();
}

void Application::start ()
{
    switchToMainWindow ();
}
void Application::setCurrentWindow (QWidget* new_window)
{
    if (current_window)
        current_window->deleteLater ();
    new_window->show ();
    current_window = new_window;
}
void Application::showError (const QString& title, const QString& description)
{
    setCurrentWindow (new QMessageBox (QMessageBox::Critical, title, description));
}
void Application::switchToMainWindow ()
{
    if (snapshot_dir.isEmpty ()) {
        showError ("Failed to load snapshot!", "No snapshot directory specified\nPlease specify snapshot directory with command line argument");
        return;
    }

    char error_message[512];
    hc_snapshot_t* snapshot = hc_snapshot_load (snapshot_dir.toUtf8 (), error_message, sizeof (error_message));
    if (!snapshot) {
        showError ("Failed to load snapshot!", QString ("Failed to load snapshot: ") + error_message);
        return;
    }
    MainWindow* main_window = new MainWindow;
    main_window->loadSnapshot (snapshot);
    setCurrentWindow (main_window);
}
