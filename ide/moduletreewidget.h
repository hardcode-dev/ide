#pragma once

#include "module.h"
extern "C" {
#include "hc_snapshot.h"
}

#include <QTreeWidget>
#include <QApplication>


class ModuleTreeWidget: public QTreeWidget
{
    Q_OBJECT

public:
    ModuleTreeWidget (QWidget* parent = nullptr);

    void loadSnapshot (hc_snapshot_t* snapshot);
    bool addModule (const QStringList& parent, const QString& name, const QSharedPointer<Module>& module);

public slots:
    void tryEditModule (const QStringList& full_name);
    void itemChanged(QTreeWidgetItem* current, QTreeWidgetItem* prev);
    void addModule(QTreeWidgetItem* item);
    void editModule(QTreeWidgetItem* item);
    void deleteModule(QTreeWidgetItem* item);

    void updateLabelsSize(QTreeWidgetItem*);
    void onWidgetResize();

signals:
    void moduleActivated (const QStringList& full_name, const QSharedPointer<Module>& module);
    void resizeLabel();

protected:
    void keyPressEvent (QKeyEvent *event) override;
    void resizeEvent(QResizeEvent *event);

private slots:
    void moduleActivatedCallback (const QModelIndex& index);

private:
    static int hcModuleCallback (const char *name, hc_module_t *module, void *user_data);
    void collapseAll ();
    void collapseAllAt (const QModelIndex& index);
    void expandOneLevelAt (const QModelIndex& index);
    void expandAllAt (const QModelIndex& index);
    void expandAll ();
    void updateAllLabel();
    ModuleTreeWidget* module_tree_widget;
    void resizeAllOpenItems(QTreeWidgetItem* item, int max, int level);
    void findMaxLabelSize(QTreeWidgetItem* item, int& max, int level);
    QTreeWidgetItem* prev_item = nullptr;

private:
    struct ModuleCallbackUserData {
        ModuleTreeWidget* _this;
        QStringList& module_name;
    };
    enum class ModuleField {
        Name = Qt::UserRole,
        FullName = Qt::UserRole + 1,
        Content = Qt::UserRole + 2,
    };

    const QPalette::ColorGroup group = QPalette::Active;
    const QPalette::ColorRole highlight_role = QPalette::Highlight;
    const QPalette::ColorRole standart_background_role = QPalette::Base;
    QPalette palette = QApplication::palette();
    QColor current_theme_color_item = palette.color(group, highlight_role);
    QString current_theme_color_background_item = "background-color: " + current_theme_color_item.name() + ";" ;
    QColor current_theme_color_standart = palette.color(group, standart_background_role);
    QString current_theme_color_background_standart = "background-color: " + current_theme_color_standart.name() + ";" ;

};
