#include "logindialog.h"

#include <QGridLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QLabel>
#include <QPushButton>


LoginDialog::LoginDialog (const QString& endpoint, const QString& login, const QString& password, QWidget *parent)
    : QDialog (parent)
{
    QVBoxLayout* layout = new QVBoxLayout;

    layout->addStretch (1);
    {
        QGridLayout* grid_layout = new QGridLayout;

        {
            {
                QLabel* label = new QLabel ("Endpoint:", this);
                grid_layout->addWidget (label, 0, 0);
            }
            {
                endpoint_line_edit = new QLineEdit (endpoint, this);
                grid_layout->addWidget (endpoint_line_edit, 0, 1);
            }
        }
        {
            {
                QLabel* label = new QLabel ("Login:", this);
                grid_layout->addWidget (label, 1, 0);
            }
            {
                login_line_edit = new QLineEdit (login, this);
                grid_layout->addWidget (login_line_edit, 1, 1);
            }
        }
        {
            {
                QLabel* label = new QLabel ("Password:", this);
                grid_layout->addWidget (label, 2, 0);
            }
            {
                password_line_edit = new QLineEdit (password, this);
                password_line_edit->setEchoMode (QLineEdit::Password);
                grid_layout->addWidget (password_line_edit, 2, 1);
            }
        }

        grid_layout->setColumnStretch (1, 1);

        layout->addLayout (grid_layout);
    }
    {
        QHBoxLayout* hlayout = new QHBoxLayout;
        hlayout->addStretch (1);
        {
            QPushButton* button = new QPushButton ("&Connect", this);
            connect (button, SIGNAL (clicked ()), this, SLOT (loginRequestCallback ()));
            hlayout->addWidget (button);
        }
        layout->addLayout (hlayout);
    }
    layout->addStretch (1);

    setLayout (layout);
}

void LoginDialog::loginRequestCallback ()
{
    QString endpoint = endpoint_line_edit->text ();
    QString login = login_line_edit->text ();
    QString password = password_line_edit->text ();

    emit loginRequested (endpoint, login, password);
}
