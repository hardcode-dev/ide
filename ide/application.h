#pragma once

#include <optional>
#include <QApplication>


class Application: public QApplication
{
    Q_OBJECT

public:
    static void setMonospaceFont (QWidget* widget);

public:
    Application (int& argc, char** argv);
    ~Application ();

    void start ();

private:
    void setCurrentWindow (QWidget* new_window);
    void showError (const QString& title, const QString& description);
    void switchToMainWindow ();

private:
    QString snapshot_dir;
    QWidget* current_window = nullptr;
    std::optional<std::string> authorization_token;
};
