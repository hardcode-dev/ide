#pragma once

#include <QWidget>
#include <QPlainTextEdit>
#include <QBoxLayout>
#include <QSplitter>
#include <QStackedLayout>
#include <QTextBlock>
#include <QStackedWidget>
#include <QPainterPath>
#include <vector>
#include <map>
#include <memory>

class ScrollboundEditor: public QWidget
{
    Q_OBJECT

public:
    class Overlay;
    class Column;
    class Line;

    friend class Overlay;

    ScrollboundEditor(int colnum, QWidget *parent=nullptr);
    inline const QVector<QSharedPointer<Column> >& get_columns() { return columns; }
    QRect get_line_bounding_rect(const QTextBlock& line, int column_index);
    QVector<QPoint> construct_matching_points(const std::vector<const QTextBlock*>& text_lines, bool bottom);
    QPolygon construct_matching_polyline(const std::vector<const QTextBlock*>& text_lines, bool bottom);
    QPainterPath construct_matching_path(const std::vector<const QTextBlock*>& text_lines, bool bottom);
    std::vector<Line> get_visible_lines(int column_index, bool pad_with_invisible=false);
    inline Overlay* get_overlay() { return &*overlay; }
    virtual void on_column_update_request(Column*) {};

public slots:
    void update_column_margins();

protected:
    QStackedLayout layers;
    QSplitter splitter;
    QVector<QSharedPointer<QStackedWidget> > column_margin_wrappers;
    QVector<QSharedPointer<Column> > columns;
    std::unique_ptr<Overlay> overlay;

    void resizeEvent(QResizeEvent* event) override;
    void showEvent(QShowEvent* event) override;

    virtual void overlay_paint_event(QPaintEvent*) {};
};

class ScrollboundEditor::Overlay: public QWidget
{
    Q_OBJECT;

public:
    friend class ScrollboundEditor;

    QSize sizeHint() const override;
    void paintEvent(QPaintEvent* event) override;

protected:
    ScrollboundEditor& editor;
    Overlay(ScrollboundEditor* editor);
};

class ScrollboundEditor::Column: public QPlainTextEdit
{
    Q_OBJECT

public:
    class Gutter;
    friend class Gutter;

    Column(QWidget *parent=nullptr);
    Column(const QString &text, QWidget *parent=nullptr);

    using QPlainTextEdit::firstVisibleBlock;
    using QPlainTextEdit::blockBoundingGeometry;
    using QPlainTextEdit::blockBoundingRect;
    using QPlainTextEdit::contentOffset;
    using QPlainTextEdit::viewportMargins;

signals:
    void followModuleRequested (const QString& flat_name);

private:
    void after_constructor();
    void tryFollowModule ();

protected:
    QFont& getMonospaceFont();
    std::shared_ptr<Gutter> gutter;

    void on_update_request(const QRect &rect, int dy);
    void keyPressEvent(QKeyEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

private:
    bool ctrl_pressed = false;
};

class ScrollboundEditor::Line
{
public:
    friend class ScrollboundEditor;

    inline ScrollboundEditor* get_editor() { return _editor; }
    inline ScrollboundEditor::Column* get_column() { return _column; }
    inline QTextBlock& get_block() { return _block; }
    inline int get_column_index() { return _column_index; }
    inline int get_line_number() { return _line_number; }
    inline double get_top() { return _top; }
    inline double get_bottom() { return _bottom; }

    inline Line(const Line& other) : Line(other._editor, other._column, other._block, other._column_index, other._top) {}

protected:
    inline Line(ScrollboundEditor* editor, ScrollboundEditor::Column* column, QTextBlock block, int column_index, double top)
        : _editor(editor), _column(column), _block(std::move(block)), _column_index(column_index), _top(top)
    {
        _line_number = _block.blockNumber() + 1;
        _bottom = top + _column->blockBoundingRect(block).height();
    }

    ScrollboundEditor* _editor;
    ScrollboundEditor::Column* _column;
    QTextBlock _block;
    int _column_index;
    int _line_number;
    double _top;
    double _bottom;
};

class ScrollboundEditor::Column::Gutter: public QWidget
{
    Q_OBJECT

public:
    class Element;
    friend class Element;

    Gutter(Gutter *other);
    Gutter(Column *column);

    void update_widths();
    bool insert(std::shared_ptr<Element> element, QString name);
    void on_editor_update_request(const QRect &rect, int dy);

  private:
    ScrollboundEditor::Column *_column;
    QHBoxLayout layout;
    std::map<QString, std::shared_ptr<Element>> elements;
};

class ScrollboundEditor::Column::Gutter::Element: public QWidget
{
    Q_OBJECT

public:
    Element(Gutter *gutter);

    virtual void update_width() = 0;
    virtual int get_width() const { return _width; }
    virtual void paint(QPaintEvent *event) = 0;


    QSize sizeHint() const override { return QSize(get_width(), maximumHeight()); }

protected:
    void paintEvent(QPaintEvent *event) override {
        paint(event);
    }
    int _width;
    Gutter *_gutter;
};

class LineNumberGutterElement : public ScrollboundEditor::Column::Gutter::Element
{
public:
    LineNumberGutterElement(ScrollboundEditor::Column::Gutter *gutter, ScrollboundEditor::Column *column);

    void update_width() override;
    void paint(QPaintEvent *event) override;

protected:
    ScrollboundEditor::Column *_column;
};
