#include "scrollboundeditor.h"

#include "hl_insn.h"

#include <QPainterPath>
#include <cstdio>
#include <QPainter>
#include <QTextBlock>
#include <QApplication>


static bool charIsModuleNameAlpha (QChar c)
{
    return (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || (c == '.') || (c == '_');
}

ScrollboundEditor::ScrollboundEditor(int colnum, QWidget *parent)
    : QWidget(parent)
{
    splitter.setOrientation(Qt::Horizontal);
    for (int i = 0; i < colnum; ++i) {
        char buf[32];
        std::snprintf(buf, sizeof(buf), "Column %d text", i);
        auto margin_wrapper = new QStackedWidget(this);
        auto editor = new ScrollboundEditor::Column(buf, margin_wrapper);
        columns.push_back(QSharedPointer<ScrollboundEditor::Column>(editor));
        column_margin_wrappers.push_back(QSharedPointer<QStackedWidget>(margin_wrapper));
        margin_wrapper->insertWidget(0, editor);
        splitter.addWidget(margin_wrapper);
    }
    splitter.setSizes ({1, 0, 0});
    update_column_margins();
    connect(&splitter, &QSplitter::splitterMoved, this, &ScrollboundEditor::update_column_margins);
    layers.addWidget(&splitter);
    overlay = std::unique_ptr<ScrollboundEditor::Overlay>(
        new ScrollboundEditor::Overlay(this)
    );
    setLayout(&layers);
    overlay->raise();
}

static bool is_widget_not_visible(QWidget* widget)
{
    if (widget == nullptr)
        return true;
    if (!widget->isVisible())
        return true;
    if (widget->width() <= 0)
        return true;
    if (widget->height() <= 0)
        return true;
    return false;
}

void ScrollboundEditor::update_column_margins()
{
    const int ex = fontMetrics().horizontalAdvance(QLatin1Char('x'));
    const int margin_size = 2 * ex;

    bool first = true;
    for (auto it = column_margin_wrappers.begin(); it != column_margin_wrappers.end(); ++it) {
        auto& widget = **it;
        if (is_widget_not_visible(&widget))
            continue;
        auto margins = widget.contentsMargins();
        margins.setLeft(first ? 0 : margin_size);
        widget.setContentsMargins(margins);
        first = false;
    }

    first = true;
    for (auto it = column_margin_wrappers.rbegin(); it != column_margin_wrappers.rend(); ++it) {
        auto& widget = **it;
        if (is_widget_not_visible(&widget))
            continue;
        auto margins = widget.contentsMargins();
        margins.setRight(first ? 0 : margin_size);
        widget.setContentsMargins(margins);
        first = false;
    }
}

QRect ScrollboundEditor::get_line_bounding_rect(const QTextBlock& line, int column_index)
{
    auto column = columns[column_index];
    auto rect = column->blockBoundingGeometry(line).translated(column->contentOffset());
    auto margins = column->viewportMargins();
    auto rectSize = rect.size().toSize();
    rectSize.setWidth(rectSize.width() + margins.left() + margins.right());
    return QRect(column->mapTo(this, rect.topLeft().toPoint()), rectSize);
};

QVector<QPoint> ScrollboundEditor::construct_matching_points(const std::vector<const QTextBlock*>& text_lines, bool bottom)
{
    QVector<QPoint> points;
    
    for (size_t i = 0; i < text_lines.size(); ++i) {
        auto text_block = text_lines[i];
        if (text_block == nullptr)
            continue;
        if (is_widget_not_visible(&*columns[i]))
            continue;
        auto rect = get_line_bounding_rect(*text_block, i);
        if (bottom) {
            points.push_back(rect.bottomLeft());
            points.push_back(rect.bottomRight());
        } else {
            points.push_back(rect.topLeft());
            points.push_back(rect.topRight());
        }
    }
    return points;
}

QPolygon ScrollboundEditor::construct_matching_polyline(const std::vector<const QTextBlock*>& text_lines, bool bottom)
{
    return QPolygon(construct_matching_points(text_lines, bottom));
}

QPainterPath ScrollboundEditor::construct_matching_path(const std::vector<const QTextBlock*>& text_lines, bool bottom)
{
    QPainterPath path;
    auto points = construct_matching_points(text_lines, bottom);
    QPoint prev;
    bool first = true;

    for (auto it = points.begin(); it != points.end(); ++it) {
        if (!first) {
            auto mid_x = (it->x() + prev.x()) / 2;
            path.cubicTo(
                    {{mid_x, prev.y()}},
                    {{mid_x, it->y()}},
                    {*it}
            );
        } else {
            first = false;
            path.moveTo({*it});
        }
        path.lineTo({prev = *(++it)});
    }

    return path;
}

std::vector<ScrollboundEditor::Line> ScrollboundEditor::get_visible_lines(int column_index, bool pad_with_invisible)
{
    std::vector<ScrollboundEditor::Line> lines;
    auto column = columns[column_index];
    double visible_bottom = column->height();
    auto block = column->firstVisibleBlock();
    double top = column->blockBoundingGeometry(block).translated(column->contentOffset()).top();
    if (pad_with_invisible && block.blockNumber() > 0) {
        block = block.previous();
        top -= column->blockBoundingRect(block).height();
    }
    do {
        double bottom = top + column->blockBoundingRect(block).height();
        if (block.isVisible() && bottom >= 0)
            lines.push_back(Line(this, &*column, block, column_index, top));
        block = block.next();
        top = bottom;
    } while (block.isValid() && top <= visible_bottom);
    if (pad_with_invisible) {
        if (block.isValid()) {
            lines.push_back(Line(this, &*column, block, column_index, top));
        }
    }
    return lines;
}

void ScrollboundEditor::resizeEvent(QResizeEvent*)
{
    overlay->resize(size());
}

void ScrollboundEditor::showEvent(QShowEvent*)
{
    update_column_margins();
}

ScrollboundEditor::Overlay::Overlay(ScrollboundEditor* editor)
    : QWidget(editor), editor(*editor)
{
    setAttribute(Qt::WA_TransparentForMouseEvents);
    setFocusPolicy(Qt::NoFocus);
}

QSize ScrollboundEditor::Overlay::sizeHint() const
{
    return editor.splitter.sizeHint();
}

void ScrollboundEditor::Overlay::paintEvent(QPaintEvent* event)
{
    editor.overlay_paint_event(event);
}

ScrollboundEditor::Column::Column(QWidget *parent)
    : QPlainTextEdit(nullptr, parent)
{
    after_constructor();
};

ScrollboundEditor::Column::Column(const QString &text, QWidget *parent)
    : QPlainTextEdit(text, parent)
{
    after_constructor();
}

void ScrollboundEditor::Column::after_constructor()
{
    document()->setDefaultFont(getMonospaceFont());
    setFont(getMonospaceFont());
    gutter = std::make_shared<Gutter>(new Gutter(this));
    connect(this, &ScrollboundEditor::Column::updateRequest, this, &ScrollboundEditor::Column::on_update_request);
}

void ScrollboundEditor::Column::tryFollowModule ()
{
    QTextCursor cursor = textCursor ();

    int pos = cursor.positionInBlock ();
    QString line = cursor.block ().text ();

    if (!line.size())
        return;

    int left = pos;
    while (left > 0) {
        if (!charIsModuleNameAlpha (line[left - 1]))
            break;
        --left;
    }
    int right = pos;
    while (right < line.size ()) {
        if (!charIsModuleNameAlpha (line[right]))
            break;
        ++right;
    }
    if (left < right) {
        QString possible_module_name = line.mid (left, right - left);
        emit followModuleRequested (possible_module_name);
    }
}

QFont& ScrollboundEditor::Column::getMonospaceFont()
{
    static QFont font{"monospace"};
    static bool first = true;
    if (first) {
        font.setStyleHint(QFont::Monospace);
        first = false;
    }
    return font;
}

void ScrollboundEditor::Column::on_update_request(const QRect&, int)
{
    auto* wrapper = parentWidget();
    if (wrapper == nullptr || wrapper->parentWidget() == nullptr)
        return;
    auto* editor = dynamic_cast<ScrollboundEditor*>(wrapper->parentWidget()->parentWidget());
    if (editor == nullptr) {
        return;
    }
    editor->on_column_update_request(this);
}

void ScrollboundEditor::Column::keyPressEvent(QKeyEvent *event)
{
    if (event->key () == Qt::Key_Return && (event->modifiers () & Qt::ControlModifier)) {
        tryFollowModule ();
    } else {
        QPlainTextEdit::keyPressEvent (event);
    }
}

void ScrollboundEditor::Column::mouseReleaseEvent(QMouseEvent *event)
{
    Qt::KeyboardModifiers modifiers = QApplication::queryKeyboardModifiers ();

    if (event->button () == Qt::LeftButton && (modifiers & Qt::ControlModifier))
        tryFollowModule ();

    QPlainTextEdit::mouseReleaseEvent (event);
}

ScrollboundEditor::Column::Gutter::Gutter(ScrollboundEditor::Column::Gutter *other)
    : QWidget(other) {}

ScrollboundEditor::Column::Gutter::Gutter(ScrollboundEditor::Column *column)
    : QWidget(column), _column(column)
{
    connect(column, &ScrollboundEditor::Column::updateRequest, this,
            &ScrollboundEditor::Column::Gutter::on_editor_update_request);
    layout.setSpacing(0);
    layout.setContentsMargins(0, 0, 0, 0);
    setLayout(&layout);
    auto line_numbers = new LineNumberGutterElement(this, _column);
    insert(std::shared_ptr<ScrollboundEditor::Column::Gutter::Element>(line_numbers), "line-number");
    update_widths();
}

bool ScrollboundEditor::Column::Gutter::insert(std::shared_ptr<ScrollboundEditor::Column::Gutter::Element> element, QString name)
{
    if (elements.find(name) != elements.end())
        return false;
    elements[name] = element;
    layout.addWidget(element.get());
    return true;
}

void ScrollboundEditor::Column::Gutter::on_editor_update_request(
    const QRect &rect, int) {
    update(0, rect.y(), width(), rect.height());

    if (rect.contains(_column->viewport()->rect()))
        update_widths();
}

ScrollboundEditor::Column::Gutter::Element::Element(ScrollboundEditor::Column::Gutter *gutter)
    : _width(0), _gutter(gutter) {}


void ScrollboundEditor::Column::Gutter::update_widths()
{
    for (auto it = elements.begin(); it != elements.end(); ++it)
        it->second->update_width();
    _column->setViewportMargins(sizeHint().rwidth(), 0, 0, 0);
}

LineNumberGutterElement::LineNumberGutterElement(ScrollboundEditor::Column::Gutter *gutter, ScrollboundEditor::Column *column)
    : Element(gutter), _column(column)
{
    column->connect(column, &ScrollboundEditor::Column::blockCountChanged, this, &LineNumberGutterElement::update_width);
    update_width();
}

void LineNumberGutterElement::update_width()
{
    // https://doc.qt.io/qt-5/qtwidgets-widgets-codeeditor-example.html
    const int num_digits = 3;  // TODO
    _width = 3 + _column->fontMetrics().horizontalAdvance(QLatin1Char('9')) * num_digits;
}

void LineNumberGutterElement::paint(QPaintEvent *event)
{
    QPainter painter(this);
    painter.setPen(palette().color(QPalette::Window));  // Inverting background with foreground for better contrast
    auto rect = event->rect();
    painter.fillRect(rect, palette().color(QPalette::WindowText));
    auto block = _column->firstVisibleBlock();
    int line_number = block.blockNumber() + 1;
    double top = _column->blockBoundingGeometry(block).translated(_column->contentOffset()).top();
    do {
        double bottom = top + _column->blockBoundingRect(block).height();
        if (block.isVisible() && bottom >= rect.top()) {
          QString line_number_label = QString::number(line_number);
          painter.drawText(rect.left(), top, rect.width(),
                           _column->fontMetrics().height(), Qt::AlignRight,
                           line_number_label);
        }
        block = block.next();
        top = bottom;
        ++line_number;
    } while (block.isValid() && top <= rect.bottom());
}
