#include "hclangeditor.h"
#include "function_analyzer.h"
extern "C" {
#include "hc_syn_rpn.h"
};

#include <QApplication>

static hc_syn_rpn_t* build_sample_syn_rpn ()
{
    hc_syn_rpn_t* syn_rpn = hc_syn_rpn_create ();
    {
        hc_syn_rpn_move_instruction (syn_rpn, HC_SYN_INSN_TYPENAME_INT, 0, 3, NULL);
        {
            hc_syn_insn_value_t value = {
                ._string = {
                    .data = memcpy (malloc (1), "a", 1),
                    .len = 1,
                },
            };
            hc_syn_rpn_move_instruction (syn_rpn, HC_SYN_INSN_PLAIN_IDENTIFIER, 4, 1, &value);
        }
        {
            hc_syn_insn_value_t value = {._long = 5};
            hc_syn_rpn_move_instruction (syn_rpn, HC_SYN_INSN_LITERAL_INTEGER, 8, 1, &value);
        }
        // hc_syn_rpn_move_instruction (syn_rpn, HC_SYN_INSN_DECLARATION_WTIH_INIT, 9, 1, NULL);
        // hc_syn_rpn_move_instruction (syn_rpn, HC_SYN_INSN_DECLARATION_STATEMENT_END, 9, 1, NULL);
        hc_syn_rpn_move_instruction (syn_rpn, HC_SYN_INSN_STAPLE_BLOCK_ITEMS, 9, 1, NULL);
        {
            hc_syn_insn_value_t value = {._long = 0};
            hc_syn_rpn_move_instruction (syn_rpn, HC_SYN_INSN_LITERAL_INTEGER, 18, 1, &value);
        }
        // hc_syn_rpn_move_instruction (syn_rpn, HC_SYN_INSN_CONTROL_RETURN, 19, 1, NULL);
        // hc_syn_rpn_move_instruction (syn_rpn, HC_SYN_INSN_CONTROL_FLOW_STATEMENT_END, 19, 1, NULL);
        hc_syn_rpn_move_instruction (syn_rpn, HC_SYN_INSN_STAPLE_BLOCK_ITEMS, 19, 1, NULL);
    }

    return syn_rpn;
}
static void print_syn_rpn_ranges (hc_syn_rpn_t* syn_rpn)
{
    fprintf (stderr, "Original source code: [[\n");
    fprintf (stderr, "int a = 5;\n");
    fprintf (stderr, "return 0;\n");
    fprintf (stderr, "]]\n");

    fprintf (stderr, "Syn RPN: [[\n");
    const hc_syn_insn_t* insn = hc_syn_rpn_get_instructions (syn_rpn);
    for (int i = 0; i < hc_syn_rpn_get_instruction_count (syn_rpn); ++i)
        fprintf (stderr, " - [% 4d:% 4d] %d\n", int (insn[i].source_off), int (insn[i].source_off + insn[i].source_size - 1), int (insn[i].type));
    fprintf (stderr, "]]\n");
}

int main (int argc, char* argv[])
{
    {
        hc_syn_rpn_t* syn_rpn = build_sample_syn_rpn ();
        print_syn_rpn_ranges (syn_rpn);
    }

    QApplication a(argc, argv);
    // ScrollboundEditor editor(3);   
    // editor.setWindowTitle(editor.tr("Scrollbound editor"));
    // editor.show();

    HCLangEditor& editor = *new HCLangEditor;
    const analyzed_code::StringType source_code_str = R"BANANA(word outer_iteration_count = 81;
mut word accumulator = 0;

for (word i: 0, outer_iteration_count - 1) {
    word x = i*2;
    accumulator += x;
    for desc (word j: 17, 5)
        accumulator += j;
}
)BANANA";

    analyzed_code::FunctionVMInput* vm_input = new analyzed_code::FunctionVMInput{
        40,
        {
            {"outer_iteration_count", 0},
            {"accumulator", 8},
            {"x", 16},
            {"stash[1]", 24},
        },
        {
            {true, 0, "SET_64", "REG_M", "81", ""},
            {true, 0, "STORE_64", "$outer_iteration_count", "REG_M", ""},

            {true, 1, "SET_64", "REG_M", "0", ""},
            {true, 1, "STORE_64", "$accumulator", "REG_M", ""},

            {true, 3, "SET_64", "REG_C", "0", ""},
            {true, 3, "LOAD_64", "REG_A", "$outer_iteration_count", ""},
            {true, 3, "SET_64", "REG_B", "1", ""},
            {true, 3, "SUBTRACT_INT64", "", "", ""},
            {true, 3, "COPY_64", "REG_D", "REG_A", ""},
            {true, 3, "RANGED_LOOP_ASCENDING", "REG_C", "REG_D", ""},

            {true, 4, "COPY_64", "REG_A", "REG_RANGED_LOOP_VARIABLE", ""},
            {true, 4, "SET_64", "REG_B", "2", ""},
            {true, 4, "MULTIPLY_INT64", "", "", ""},
            {true, 4, "STORE_64", "$x", "REG_A", ""},

            {true, 5, "LOAD_64", "REG_A", "$accumulator", ""},
            {true, 5, "LOAD_64", "REG_B", "$x", ""},
            {true, 5, "ADD_INT64", "", "", ""},
            {true, 5, "STORE_64", "$accumulator", "REG_A", ""},

            {true, 6, "SET_64", "REG_C", "17", ""},
            {true, 6, "SET_64", "REG_D", "5", ""},
            {true, 6, "RANGED_NESTED_LOOP_DESCENDING", "REG_C", "REG_D", "$stash[0]"},

            {true, 7, "LOAD_64", "REG_A", "$accumulator", ""},
            {true, 7, "COPY_64", "REG_B", "REG_RANGED_LOOP_VARIABLE", ""},
            {true, 7, "ADD_INT64", "", "", ""},
            {true, 7, "STORE_64", "$accumulator", "REG_A", ""},

            {false, 7, "RANGED_NESTED_LOOP_DESCENDING_END", "$stash[0]", "", ""},

            {true, 8, "RANGED_LOOP_ASCENDING_END", "", "", ""},
        },
        build_sample_syn_rpn(),
    };
    QSharedPointer<analyzed_code::FunctionVMInput> vm_input_ptr(vm_input);

    QSettings settings("hardcore-lang", "editor");
    editor.load_settings(settings);

    editor.set_code(source_code_str);
    editor.update_analysis(vm_input_ptr, editor.get_source_code()->changetick);

    editor.setWindowTitle(editor.tr("hc lang editor"));
    editor.show();

    auto editor_ptr = std::shared_ptr<HCLangEditor>(&editor);
    FunctionAnalyzer analyzer(editor_ptr);
    analyzer.start();
    analyzer.on_source_code_change();

    auto result = a.exec();
    analyzer.stop();
    analyzer.wait();

    return result;
}
