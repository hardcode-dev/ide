QT += core gui widgets

CONFIG += c++11

QMAKE_CXXFLAGS = -I../../hc-inst/include
QMAKE_LIBS = -L../../hc-inst/lib -lhcsyn -lhcsynrpn

SOURCES += \
    main.cpp \
    function_analyzer.cpp \
    scrollboundeditor.cpp \
    hclangeditor.cpp

HEADERS += \
    scrollboundeditor.h \
    hclangeditor.h \
    function_analyzer.h \
    ../analyzed_code.h

MOC_DIR = .moc
OBJECTS_DIR = .obj
