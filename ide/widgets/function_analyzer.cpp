#include "function_analyzer.h"
#include <unistd.h>

#include "hl_insn.h"

#include <QDebug>

extern QMap<analyzed_code::StringType, analyzed_code::ComplexityVector> default_instruction_complexities;
extern QMap<analyzed_code::StringType, analyzed_code::NestingModifier> default_instruction_nesting_modifiers;

#define add_highlight_range(INSN_NAME, line_off, source_size) {HC_SYN_INSN_ ## INSN_NAME, line_off, source_size, {}}
QMap<QString, FunctionAnalyzer::LineInfo> FunctionAnalyzer::line_instructions = {
#include "line_instructions/all.h"
};
#undef add_highlight_range

FunctionAnalyzer::FunctionAnalyzer(std::shared_ptr<HCLangEditor> editor) :
    editor(editor)
{
    connect(&*editor, &HCLangEditor::source_code_changed, this, &FunctionAnalyzer::on_source_code_change, Qt::DirectConnection);
    connect(this, &FunctionAnalyzer::analysis_finished, &*editor, &HCLangEditor::update_analysis, Qt::QueuedConnection);
    connect(this, &FunctionAnalyzer::analysis_failed, &*editor, &HCLangEditor::notify_analysis_error, Qt::QueuedConnection);
}

std::pair<bool, analyzed_code::FunctionVMInput> FunctionAnalyzer::process_code(std::shared_ptr<HCLangEditor::SourceCodeState const> src_code)
{
    analyzed_code::FunctionVMInput vm_input = {
        40,
        {
            {"outer_iteration_count", 0},
            {"accumulator", 8},
            {"x", 16},
            {"stash[1]", 24},
        },
        {},
        hc_syn_rpn_create()
    };
    auto& code = src_code->code;
    int line_index = -1;
    int char_off = 0;
    for (auto& line: code.split('\n')) {
        ++line_index;
        auto key = line.trimmed();
        int indent_off = 0;
        while (indent_off < line.length() && line[indent_off].isSpace())
            ++indent_off;
        auto maybe_line_instructions = line_instructions.find(key);
        if (maybe_line_instructions == line_instructions.end()) {
            return {false, {}};
        }
        for (auto instr: maybe_line_instructions->instructions) {
            instr.source_line += line_index;
            vm_input.instructions.push_back(instr);
            for (auto cplx: _get_complexity(instr)) {
                vm_input.complexity_source_lines.append(line_index);
                vm_input.complexities.append(cplx);
            }
        }
        for (auto syn_insn: maybe_line_instructions->highlight_ranges) {
            syn_insn.source_off += char_off + indent_off;
            hc_syn_rpn_move_instruction(&*vm_input.syn_rpn, syn_insn.type, syn_insn.source_off, syn_insn.source_size, &syn_insn.value);
        }
        char_off += line.length() + 1;
    }
    return {true, vm_input};
}

void FunctionAnalyzer::run()
{
    if (running)
        return;
    running = true;
    while (running)
    {
        decltype(this->src_code) src_code;
        {
            QMutexLocker lk(&mx);
            if (!this->src_code || this->src_code->changetick == last_processed)
                analyze_request_cond.wait(&mx);
            if (!running)
                break;
            src_code = this->src_code;
        }
        if (src_code->changetick == last_processed)
            continue;
        auto result = process_code(src_code);
        last_processed = src_code->changetick;
        if (result.first) {
            // notify the widget
            emit analysis_finished(QSharedPointer<analyzed_code::FunctionVMInput>(new analyzed_code::FunctionVMInput(result.second)), src_code->changetick);
        } else {
            // notify about error
            emit analysis_failed(src_code->changetick);
        }
    }
}

QVector<analyzed_code::Complexity> FunctionAnalyzer::_get_complexity(const analyzed_code::Instruction& instr)
{
    if (instr.instruction_type == "CALL") {
        if (instr.argument_a == "3" && instr.argument_b == "[0x118]")
            return {{"(13 + (#state - i)*36)", {1, 0}}, {"(11 + (#state - i)*19)", {0, 1}}};
    }
    switch (default_instruction_nesting_modifiers[instr.instruction_type]) {
    case analyzed_code::NestingModifier::IncreaseDepth: {
        QString multiplier = "number";
        if (instr.argument_a == "[*0x28]" && instr.argument_b == "*0x38" && instr.argument_c == "[*0x40]") {
            multiplier = "(#base10p9 - first)";
        } else if (instr.argument_a == "[0xB0]" && instr.argument_b == "0" && instr.argument_c == "[0xB8]") {
            multiplier = "(full_base1b_digits)";
        } else if (instr.argument_a == "[0x108]" && instr.argument_b == "0" && instr.argument_c == "[0x110]") {
            multiplier = "(#state)";
        } else if (instr.argument_a == "[0x130]" && instr.argument_b == "0" && instr.argument_c == "3") {
            multiplier = "(4)";
        }
        return {multiplier, default_instruction_complexities[instr.instruction_type]};
    }
    case analyzed_code::NestingModifier::DecreaseDepth:
        return {default_instruction_complexities[instr.instruction_type], analyzed_code::ComplexityElementType::BLOCK_END};
    default:
        return {default_instruction_complexities[instr.instruction_type]};
    }
}

QMap<analyzed_code::StringType, analyzed_code::ComplexityVector> default_instruction_complexities = {
    {"SET_64", {1, 0}},
    {"STORE_64", {1, 1}},
    {"LOAD_64", {1, 1}},
    {"SUBTRACT_INT64", {5, 0}},
    {"ADD_INT64", {5, 0}},
    {"MULTIPLY_INT64", {5, 0}},
    {"COPY_64", {1, 0}},
    {"LOOP_ASCENDING", {2, 0}},
    {"LOOP_END", {9, 0}},
    {"RANGED_LOOP_ASCENDING", {2, 0}},
    {"RANGED_LOOP_ASCENDING_END", {9, 0}},
    {"RANGED_LOOP_DESCENDING", {2, 0}},
    {"RANGED_LOOP_DESCENDING_END", {9, 0}},
    {"RANGED_NESTED_LOOP_ASCENDING", {4, 2}},
    {"RANGED_NESTED_LOOP_ASCENDING_END", {11, 2}},
    {"RANGED_NESTED_LOOP_DESCENDING", {4, 2}},
    {"RANGED_NESTED_LOOP_DESCENDING_END", {11, 2}},
    {"PUSH", {1, 1}},
    {"SET", {1, 1}},
    {"ARR_LENGTH", {1, 1}},
    {"MULT", {5, 3}},
    {"DIV", {5, 3}},
    {"REM", {5, 3}},
    {"ADD", {4, 3}},
    {"SUB", {4, 3}},
    {"RSHIFT", {4, 3}},
    {"BOR", {4, 3}},
    {"LESS", {4, 3}},
    {"LESS_OR_GREATER", {4, 3}},
    {"BAND", {4, 3}},
    {"BSR", {4, 3}},
    {"ARR_INDEX", {4, 3}},
    {"STRUCT_INDEX", {4, 3}},
    {"ARR_APPEND", {5, 3}},
    {"RETURN", {1, 1}},
    {"IFNZ", {2, 1}},
    {"IFNZ", {2, 1}},
};

QMap<analyzed_code::StringType, analyzed_code::NestingModifier> default_instruction_nesting_modifiers = {
    {"SET_64", analyzed_code::NestingModifier::Unchanged},
    {"STORE_64", analyzed_code::NestingModifier::Unchanged},
    {"LOAD_64", analyzed_code::NestingModifier::Unchanged},
    {"SUBTRACT_INT64", analyzed_code::NestingModifier::Unchanged},
    {"ADD_INT64", analyzed_code::NestingModifier::Unchanged},
    {"MULTIPLY_INT64", analyzed_code::NestingModifier::Unchanged},
    {"COPY_64", analyzed_code::NestingModifier::Unchanged},
    {"LOOP_ASCENDING", analyzed_code::NestingModifier::IncreaseDepth},
    {"LOOP_END", analyzed_code::NestingModifier::DecreaseDepth},
    {"RANGED_LOOP_ASCENDING", analyzed_code::NestingModifier::IncreaseDepth},
    {"RANGED_LOOP_ASCENDING_END", analyzed_code::NestingModifier::DecreaseDepth},
    {"RANGED_LOOP_DESCENDING", analyzed_code::NestingModifier::IncreaseDepth},
    {"RANGED_LOOP_DESCENDING_END", analyzed_code::NestingModifier::DecreaseDepth},
    {"RANGED_NESTED_LOOP_ASCENDING", analyzed_code::NestingModifier::IncreaseDepth},
    {"RANGED_NESTED_LOOP_ASCENDING_END", analyzed_code::NestingModifier::DecreaseDepth},
    {"RANGED_NESTED_LOOP_DESCENDING", analyzed_code::NestingModifier::IncreaseDepth},
    {"RANGED_NESTED_LOOP_DESCENDING_END", analyzed_code::NestingModifier::DecreaseDepth},
};
