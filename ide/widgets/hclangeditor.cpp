#include "hclangeditor.h"

#include "hl_insn.h"

#include <QPainterPath>
#include <QTextDocument>
#include <QStringList>
#include <QPainter>
#include <sstream>

HCLangEditor::HCLangEditor(QWidget *parent)
    : ScrollboundEditor(3, parent)
{
    static_constructor();
    connect (&*this->columns[0], SIGNAL (followModuleRequested (const QString&)), this, SIGNAL (followModuleRequested (const QString&)));
    for (int i = 1; i <= 2; ++i)
        this->columns[i]->setReadOnly(true);

    source_highlighter = QSharedPointer<SourceHighlighter>(new SourceHighlighter{columns[0]->document()});

    style_scheme = QSharedPointer<StyleScheme>(new StyleScheme());
    source_highlighter->style_scheme = style_scheme;

    connect(&*this->columns[0], &QPlainTextEdit::textChanged, this, &HCLangEditor::on_source_code_column_change);
}

void HCLangEditor::bind_vm_input(QSharedPointer<analyzed_code::FunctionVMInput> instructions)
{
    _instructions = instructions;
}

void HCLangEditor::set_code(const analyzed_code::StringType& code) {
    auto& code_doc = *this->columns[0]->document();
    code_doc.setPlainText(code);
}

void HCLangEditor::redisplay_analysis() {
    // TODO partial update? Can we splice codeblocks into a document?

    static const QString instr_indent_single_str = "\U0000258f\U000000a0\U000000a0\U00002595";  // "▏  ▕"
    // static const QString instr_indent_single_str = "    ";
    QStringList instructions_builder;
    bool first_line = true;
    int instr_nesting_depth = 0;
    for (const analyzed_code::Instruction& instr: _instructions->instructions) {
        if (!first_line) {
            instructions_builder << "\n";
        } else first_line = false;
        auto instr_nesting_modifier = _get_nesting_modifier(instr);
        if (instr_nesting_modifier == analyzed_code::NestingModifier::DecreaseDepth)
            --instr_nesting_depth;
        auto instr_indent_prefix = instr_indent_single_str.repeated(instr_nesting_depth);
        instructions_builder << instr_indent_prefix << instr.instruction_type;
        if (instr.argument_a.size()) {
            if (instr.argument_b.size()) {
                if (instr.argument_c.size()) {
                    instructions_builder << " (" << instr.argument_a << ", " << instr.argument_b <<  ", " << instr.argument_c << ")";
                } else {
                    instructions_builder << " (" << instr.argument_a << ", " << instr.argument_b << ")";
                }
            } else {
                instructions_builder << " (" << instr.argument_a << ")";
            }
        }
        if (instr_nesting_modifier == analyzed_code::NestingModifier::IncreaseDepth)
            ++instr_nesting_depth;

        source_highlighter->syn_rpn = _instructions->syn_rpn;
    }

    first_line = true;
    int compl_nesting_depth = 0;
    QStringList complexity_builder;
    for (const auto& cplx: _instructions->complexities) {
        if (!first_line) {
            complexity_builder << "\n";
        } else first_line = false;
        if (cplx.does_decrease_nesting())
            --compl_nesting_depth;
        complexity_builder << instr_indent_single_str.repeated(compl_nesting_depth) << cplx.to_string();
        if (cplx.does_increase_nesting())
            ++compl_nesting_depth;
    }

    auto& instructions_doc = *this->columns[1]->document();
    instructions_doc.clear();
    auto& complexity_doc = *this->columns[2]->document();
    complexity_doc.clear();

    instructions_doc.setPlainText(instructions_builder.join(""));
    complexity_doc.setPlainText(complexity_builder.join(""));
}

std::shared_ptr<HCLangEditor::SourceCodeState const> HCLangEditor::get_source_code()
{
    if (_changetick != _cached_srccode_state->changetick) {
        _cached_srccode_state = std::shared_ptr<SourceCodeState>(new SourceCodeState{
            .code = this->columns[0]->document()->toPlainText(),
            .changetick = _changetick,
        });
    }
    return _cached_srccode_state;
}

void HCLangEditor::on_column_update_request(ScrollboundEditor::Column*)
{
    overlay->update();  // Updating QSplitterHandler doesn't seem to redraw Overlay area above it
}

void HCLangEditor::overlay_paint_event(QPaintEvent*)
{
    draw_matching_line_decorations();
}

inline static bool line_is_instruction_before_source(std::vector<ScrollboundEditor::Line>& instructions_lines, std::vector<ScrollboundEditor::Line>::iterator& instructions_lines_it, QVector<analyzed_code::Instruction>& instructions, int source_line_index, bool nonstrict /*true means "not after"*/)
{
    if (instructions_lines_it == instructions_lines.end()) {
        return false;
    }
    int instr_line_idx = instructions_lines_it->get_line_number() - 1;
    if (instr_line_idx < 0)  // shouldn't be possible
        return true;
    if (instr_line_idx >= instructions.size())
        return false;
    int instr_source_line_index = instructions[instr_line_idx].source_line;
    if (nonstrict)
        return instr_source_line_index <= source_line_index;
    else
        return instr_source_line_index <  source_line_index;
}

inline static bool line_is_complexity_before_source(std::vector<ScrollboundEditor::Line>& complexity_lines, std::vector<ScrollboundEditor::Line>::iterator& complexity_lines_it, QVector<int>& complln_source_line, int source_line_index, bool nonstrict /*true means "not after"*/)
{
    if (complexity_lines_it == complexity_lines.end()) {
        return false;
    }
    int compl_line_idx = complexity_lines_it->get_line_number() - 1;
    if (compl_line_idx < 0)  // shouldn't be possible
        return true;
    if (compl_line_idx >= complln_source_line.size())
        return false;
    // The part above may be extracted to a separate function
    int compl_source_line_index = complln_source_line[compl_line_idx];
    if (nonstrict)
        return compl_source_line_index <= source_line_index;
    else
        return compl_source_line_index <  source_line_index;
}

#include <QDebug>
void HCLangEditor::draw_matching_line_decorations()
{
    if (
            (!columns[1]->isVisible() || columns[1]->width() <= 0) &&
            (!columns[2]->isVisible() || columns[2]->width() <= 0)
       )
        return;

    QPainter painter(&*overlay);
    painter.setRenderHints(painter.renderHints() | QPainter::Antialiasing);
    auto color = style_scheme->ui_matching_curve_color;
    if (!_is_analysis_up_to_date || !_is_code_correct)
        color.setAlphaF(color.alphaF() * .6);
    QPen pen(color);
    pen.setWidthF(1.2);
    painter.setPen(pen);

    auto       source_lines = get_visible_lines(0);
    auto instructions_lines = get_visible_lines(1, true);
    auto   complexity_lines = get_visible_lines(2, true);

    auto       source_lines_it =       source_lines.begin();
    auto instructions_lines_it = instructions_lines.begin();
    auto   complexity_lines_it =   complexity_lines.begin();

    int cursor_line_index = columns[0]->textCursor().block().blockNumber();

    while (source_lines_it != source_lines.end()) {
        int source_line_index = source_lines_it->get_line_number() - 1;
        bool is_cursor_line = source_line_index == cursor_line_index;
        bool should_highlight = _highlight_all_lines || (_highlight_cursor_line && is_cursor_line);
        // FIXME emptiness is checked for the current content, even when analysis is outdated
        //       it can cause multiple curves overlaying over the second and third columns
        //       overlaying is only visible when curves are translucent
        should_highlight = should_highlight && source_lines_it->get_block().text().trimmed().length() > 0;

        while (line_is_instruction_before_source(instructions_lines, instructions_lines_it, _instructions->instructions, source_line_index, false))
            ++instructions_lines_it;
        if (instructions_lines_it == instructions_lines.end())
            --instructions_lines_it;
        while (line_is_complexity_before_source(complexity_lines, complexity_lines_it, _instructions->complexity_source_lines, source_line_index, false))
            ++complexity_lines_it;
        if (complexity_lines_it == complexity_lines.end())
            --complexity_lines_it;

        // start
        if (should_highlight) {
            if (_highlight_with_border) {
                QPainterPath border = construct_matching_path({
            _is_code_correct && _is_analysis_up_to_date ?
                          &source_lines_it->get_block() : nullptr,
                    &instructions_lines_it->get_block(),
                      &complexity_lines_it->get_block(),
                }, false);
                painter.drawPath(border);
            }
        }

        ++instructions_lines_it;
        while (line_is_instruction_before_source(instructions_lines, instructions_lines_it, _instructions->instructions, source_line_index, true)) {
            // intermediate
            ++instructions_lines_it;
        }
        ++complexity_lines_it;
        while (line_is_complexity_before_source(complexity_lines, complexity_lines_it, _instructions->complexity_source_lines, source_line_index, true)) {
            // intermediate
            ++complexity_lines_it;
        }

        --instructions_lines_it;
        --complexity_lines_it;

        // end

        ++source_lines_it;
    }
}

void HCLangEditor::on_source_code_column_change(void)
{
    if (!_propagate_source_code_changed)
        return;
    ++_changetick;
    _is_analysis_up_to_date = false;
    emit source_code_changed();
}

void HCLangEditor::update_analysis(QSharedPointer<analyzed_code::FunctionVMInput> instr, Changetick changetick)
{
    _is_analysis_up_to_date = changetick == _changetick;
    _is_code_correct = true;
    bind_vm_input(instr);
    redisplay_analysis();
    _propagate_source_code_changed = false;
    source_highlighter->rehighlight();  // emits QPlainTextEdit::textChanged!
    _propagate_source_code_changed = true;
}

void HCLangEditor::notify_analysis_error(Changetick changetick)
{
    _is_analysis_up_to_date = changetick == _changetick;
    _is_code_correct = false;
}

void HCLangEditor::load_settings(QSettings& settings)
{
    style_scheme->load_settings(settings);
    QPalette pal;
    pal.setColor(QPalette::Base, style_scheme->syntax_normal.background().color());
    for (auto col: columns) {
        col->setPalette(pal);
    }
}

template<typename T>
inline void load_type_from_setting(T& out, const QSettings& settings, const QString& key, const T& default_value);

template<>
inline void load_type_from_setting<QColor>(QColor& out, const QSettings& settings, const QString& key, const QColor& default_value)
{
    out = settings.value(key, default_value).value<QColor>();
}

template<>
inline void load_type_from_setting<bool>(bool& out, const QSettings& settings, const QString& key, const bool& default_value)
{
    out = settings.value(key, default_value).toBool();
}

template<>
inline void load_type_from_setting<int>(int& out, const QSettings& settings, const QString& key, const int& default_value)
{
    out = settings.value(key, default_value).toInt();
}

template<>
inline void load_type_from_setting<QTextCharFormat>(QTextCharFormat& out, const QSettings& settings, const QString& key, const QTextCharFormat& default_value)
{
    auto value = default_value;
    int font_weight;  // Should we use bool bold instead?
    bool italic;
    bool underline;
    bool overline;
    bool strike;
    QColor fg;
    QColor bg;

    load_type_from_setting<int>(font_weight, settings, key + "_font_weight", value.fontWeight());
    load_type_from_setting<bool>(italic, settings, key + "_italic", value.fontItalic());
    load_type_from_setting<bool>(underline, settings, key + "_underline", value.fontUnderline());
    load_type_from_setting<bool>(overline, settings, key + "_overline", value.fontOverline());
    load_type_from_setting<bool>(strike, settings, key + "_strike", value.fontStrikeOut());
    load_type_from_setting<QColor>(fg, settings, key + "_fg", value.foreground().color());
    load_type_from_setting<QColor>(bg, settings, key + "_bg", value.background().color());

    value.setFontWeight(font_weight);
    value.setFontItalic(italic);
    value.setFontUnderline(underline);
    value.setFontOverline(overline);
    value.setFontStrikeOut(strike);
    value.setForeground(fg);
    value.setBackground(bg);

    out = value;
}

void HCLangEditor::StyleScheme::load_settings(QSettings& settings)
{
    settings.beginGroup("UI");
    load_type_from_setting<QColor>(background_editor, settings, "editor_background", QColor(0x09, 0x21, 0x28));
    load_type_from_setting<QColor>(ui_matching_curve_color, settings, "matching_curve_color", QColor(0x21, 0x4E, 0x9A));
    settings.endGroup ();

    settings.beginGroup("SyntaxHighlighting");
    QTextCharFormat default_fmt;
    default_fmt.setBackground(background_editor);

#define load_syntax_setting(name, r, g, b) \
    default_fmt.setForeground(QColor(r, g, b));\
    load_type_from_setting<QTextCharFormat>(syntax_ ## name, settings, #name, default_fmt);

    load_syntax_setting(normal, 0xDE, 0xDE, 0xDE);
    default_fmt = syntax_normal;

    load_syntax_setting(delimiter, 0xFF, 0xA0, 0x21);
    load_syntax_setting(type, 0x42, 0xDE, 0x21);
    load_syntax_setting(identifier, 0xDE, 0xDE, 0xDE); //0x72, 0xDF, 0xCF);
    load_syntax_setting(keyword, 0x72, 0xDF, 0xCF);
    load_syntax_setting(literal, 0xFF, 0x42, 0xBD);
    load_syntax_setting(operator, 0xFF, 0xFF, 0x42);
    load_syntax_setting(comment, 0x21, 0xBD, 0xDE);

    default_fmt = syntax_literal;
    load_syntax_setting(literal_string, 0xFA, 0x80, 0xAA);

#undef load_syntax_setting

    settings.endGroup();
}

inline QTextCharFormat HCLangEditor::StyleScheme::format_for_instruction_type(int type)
{
    switch (HC_SYN_INSN_INDEX(type)) {
#define CASE_FOR(INSN_NAME) case HC_SYN_INSN_INDEX_ ## INSN_NAME
    // TODO consider splitting "hc_syn_instructions.h" into multiple blocks to easily include desired blocks
    CASE_FOR (HL_TYPE):
        return syntax_type;
    CASE_FOR (LITERAL_INTEGER):
    CASE_FOR (LITERAL_FLOAT):
    CASE_FOR (LITERAL_DOUBLE):
    CASE_FOR (LITERAL_QUAD):
    CASE_FOR (HL_LITERAL):
        return syntax_literal;
    CASE_FOR (HL_LITERAL_STRING):
        return syntax_literal_string;
    CASE_FOR (DECLARED_VARIABLE_NAME):
    CASE_FOR (PLAIN_IDENTIFIER):
    CASE_FOR (EXTEND_IDENTIFIER):
        return syntax_identifier;
    CASE_FOR (HL_KEYWORD):
        return syntax_keyword;
    CASE_FOR (REFERENCE_LOCAL):
    CASE_FOR (REFERENCE_GC):
    CASE_FOR (REFERENCE_FOREST):
    CASE_FOR (FIELD_ACCESS):
    CASE_FOR (ADDITION):
    CASE_FOR (SUBTRACTION):
    CASE_FOR (MULTIPLICATION):
    CASE_FOR (DIVISION):
    CASE_FOR (ARRAY_ELEMENT_ACCESS):
    CASE_FOR (INITIALIZE):
    CASE_FOR (MUTATE):
        return syntax_operator;
    CASE_FOR (STAPLE_BLOCK_ITEMS):
    CASE_FOR (HL_DELIMITER):
        return syntax_delimiter;
    CASE_FOR (EMPTY_EXPRESSION):
    CASE_FOR (FUNCTION_ARGUMENT):
        return syntax_identifier;
    CASE_FOR (STAPLE_FUNCTION_ARGUMENT):
        return syntax_delimiter;
    CASE_FOR (FUNCTION_CALL):
    CASE_FOR (HL_OPERATOR):
        return syntax_operator;
    CASE_FOR (HL_COMMENT):
        return syntax_comment;
    default:
        return syntax_normal;
#undef CASE_FOR
    }
}

void HCLangEditor::SourceHighlighter::highlightBlock(const QString &text)
{
    if (this->syn_rpn == nullptr)
        return;
    auto syn_rpn = &*this->syn_rpn;
    auto lnlength = text.length();
    auto lnfirstchar = currentBlock().position();
    auto lnlastchar = lnfirstchar + lnlength;
    auto arr = hc_syn_rpn_get_instructions(syn_rpn);
    auto arrlength = hc_syn_rpn_get_instruction_count(syn_rpn);

    setFormat(0, lnlength, style_scheme->syntax_normal);
    // Consider saving last index in setCurrentBlockState()
    for (int i = 0; i < arrlength; ++i) {
        auto source_off = arr[i].source_off;
        auto source_last = source_off + arr[i].source_size;
        if (source_last < lnfirstchar || source_off > lnlastchar)
            continue;
        auto col = source_off - lnfirstchar;
        auto lastcol = source_last - lnfirstchar;
        if (col < 0)
            col = 0;
        if (lastcol > lnlength)
            lastcol = lnlength;
        setFormat(col, lastcol - col, style_scheme->format_for_instruction_type(arr[i].type));
    }
}
