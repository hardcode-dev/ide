#include <utility>
#include <atomic>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include "hclangeditor.h"
#include "../analyzed_code.h"

class FunctionAnalyzer : public QThread
{
    Q_OBJECT

private:
    // TODO consider making analyzers unaware of widgets and make a separate class for interaction
    struct LineInfo {
        QVector<analyzed_code::Instruction> instructions;
        QVector<hc_syn_insn_t> highlight_ranges;
        LineInfo(QVector<analyzed_code::Instruction> instructions, QVector<hc_syn_insn_t> highlight_ranges={})
                  : instructions(instructions), highlight_ranges(highlight_ranges) {};
    };
    static QMap<QString, LineInfo> line_instructions;
    std::shared_ptr<HCLangEditor> editor;
    QMutex mx;
    QWaitCondition analyze_request_cond;
    std::shared_ptr<HCLangEditor::SourceCodeState const> src_code;
    HCLangEditor::Changetick last_processed = {};
    volatile bool running = false;
    QVector<analyzed_code::Complexity> _get_complexity(const analyzed_code::Instruction& instr);

public:
    FunctionAnalyzer(std::shared_ptr<HCLangEditor> editor);

    inline void send_source_code_state(std::shared_ptr<HCLangEditor::SourceCodeState const> code)
    {
        {
            QMutexLocker lk(&mx);
            src_code = code;
        }
        analyze_request_cond.wakeAll();
    }

    inline void stop()
    {
        running = false;
        analyze_request_cond.wakeAll();
    }

public slots:
    void on_source_code_change(void)
    {
        send_source_code_state(editor->get_source_code());
    }

signals:
    void analysis_finished(QSharedPointer<analyzed_code::FunctionVMInput>, HCLangEditor::Changetick changetick);
    void analysis_failed(HCLangEditor::Changetick changetick);

protected:
    std::pair<bool, analyzed_code::FunctionVMInput> process_code(std::shared_ptr<HCLangEditor::SourceCodeState const> src_code);
    void run() override;
};
