#pragma once

#include "scrollboundeditor.h"
#include "../analyzed_code.h"

#include <memory>  // TODO use QSharedPointer when possible
#include <QSharedPointer>
#include <QSyntaxHighlighter>
#include <QTextCharFormat>
#include <QSettings>
#include <cstdint>

extern "C" {
#include "hc_syn_rpn.h"
};

extern QMap<analyzed_code::StringType, analyzed_code::NestingModifier> default_instruction_nesting_modifiers;

class HCLangEditor: public ScrollboundEditor
{
    Q_OBJECT

public:
    class SourceHighlighter;

    class StyleScheme;
    typedef uint64_t Changetick;
    struct SourceCodeState {
        QString code;
        Changetick changetick;
    };

    HCLangEditor(QWidget *parent=nullptr);

    void load_settings(QSettings&);
    void set_code(const analyzed_code::StringType& code);
    void bind_vm_input(QSharedPointer<analyzed_code::FunctionVMInput> instructions);
    void redisplay_analysis(); // when function is opened or compiler updates
                               // instruction list
    std::shared_ptr<SourceCodeState const> get_source_code();

    void on_column_update_request(ScrollboundEditor::Column* column) override;

signals:
    void followModuleRequested (const QString& flat_name);

private slots:
    void on_source_code_column_change(void);

public slots:
    void update_analysis(QSharedPointer<analyzed_code::FunctionVMInput>, Changetick changetick);
    void notify_analysis_error(Changetick changetick);

signals:
    void source_code_changed(void);

protected:
    QSharedPointer<analyzed_code::FunctionVMInput> _instructions;
    analyzed_code::InstructionNestingModifierGetter _get_nesting_modifier = [] (const analyzed_code::Instruction& instr) -> analyzed_code::NestingModifier {
        return default_instruction_nesting_modifiers[instr.instruction_type];
    };


    bool _highlight_cursor_line = false;
    bool _highlight_all_lines = true;
    // bool _highlight_with_background;  // Not implemented
    bool _highlight_with_border = true;

    void overlay_paint_event(QPaintEvent *event) override;
    void draw_matching_line_decorations();

    Changetick _changetick = 1;
    std::shared_ptr<SourceCodeState> _cached_srccode_state = std::shared_ptr<SourceCodeState>(new SourceCodeState{.code = "", .changetick = 0});
    bool _is_analysis_up_to_date = true;
    bool _is_code_correct = true;
    bool _propagate_source_code_changed = true;

    QSharedPointer<SourceHighlighter> source_highlighter;
    QSharedPointer<StyleScheme> style_scheme;

    static inline void static_constructor()
    {
        static volatile bool ran = false;
        if (ran)
            return;
        qRegisterMetaType<QSharedPointer<analyzed_code::FunctionVMInput>>("QSharedPointer<analyzed_code::FunctionVMInput>");
        qRegisterMetaType<Changetick>("HCLangEditor::Changetick");
        ran = true;
    }
};

class HCLangEditor::SourceHighlighter: public QSyntaxHighlighter
{
    Q_OBJECT
protected:
    void highlightBlock(const QString &text) override;

public:
    QSharedPointer<hc_syn_rpn_t> syn_rpn;
    QSharedPointer<StyleScheme> style_scheme;
    using QSyntaxHighlighter::QSyntaxHighlighter;
};

class HCLangEditor::StyleScheme
{
public:
    void load_settings(QSettings&);
    QTextCharFormat format_for_instruction_type(int type);

public:
    // Base text styles:
    QTextCharFormat syntax_normal;

    QTextCharFormat syntax_keyword;
    QTextCharFormat syntax_delimiter;
    QTextCharFormat syntax_type;
    QTextCharFormat syntax_identifier;
    QTextCharFormat syntax_literal;
    QTextCharFormat syntax_operator;
    QTextCharFormat syntax_comment;

    // More precise text styles:
    QTextCharFormat syntax_literal_string;

    // UI styles:
    QColor background_editor;
    QColor ui_matching_curve_color;
};
