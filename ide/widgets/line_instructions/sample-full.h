{"word outer_iteration_count = 81;",
    {{
        {true, 0, "SET_64", "REG_M", "81", ""},
        {true, 0, "STORE_64", "$outer_iteration_count", "REG_M", ""},
    }, {
        add_highlight_range(TYPENAME_WORD, 0, 4),
        add_highlight_range(PLAIN_IDENTIFIER, 5, 21),
        add_highlight_range(INITIALIZE, 27, 1),
        add_highlight_range(LITERAL_INTEGER, 29, 2),
        add_highlight_range(STAPLE_BLOCK_ITEMS, 31, 1),
    }}
},
{"mut word accumulator = 0;",
    {{
        {true, 0, "SET_64", "REG_M", "0", ""},
        {true, 0, "STORE_64", "$accumulator", "REG_M", ""},
    }, {
        add_highlight_range(TYPENAME_WORD, 0, 8),
        add_highlight_range(PLAIN_IDENTIFIER, 9, 11),
        add_highlight_range(INITIALIZE, 21, 1),
        add_highlight_range(LITERAL_INTEGER, 23, 1),
        add_highlight_range(STAPLE_BLOCK_ITEMS, 24, 1),
    }}

},
{"",
    {{
        /**/
    }}
},
{"for (word i: 0, outer_iteration_count - 1) {",
    {{
        {true, 0, "SET_64", "REG_C", "0", ""},
        {true, 0, "LOAD_64", "REG_A", "$outer_iteration_count", ""},
        {true, 0, "SET_64", "REG_B", "1", ""},
        {true, 0, "SUBTRACT_INT64", "", "", ""},
        {true, 0, "COPY_64", "REG_D", "REG_A", ""},
        {true, 0, "RANGED_LOOP_ASCENDING", "REG_C", "REG_D", ""},
    }, {
        add_highlight_range(PLAIN_IDENTIFIER, 0, 3),  // consider adding a separate element into hc_syn_instructions.h
        add_highlight_range(FUNCTION_CALL, 4, 1),  // consider adding a separate element into hc_syn_instructions.h
        add_highlight_range(TYPENAME_WORD, 5, 4),
        add_highlight_range(PLAIN_IDENTIFIER, 10, 1),
        add_highlight_range(INITIALIZE, 11, 1),
        add_highlight_range(LITERAL_INTEGER, 13, 1),
        add_highlight_range(STAPLE_FUNCTION_ARGUMENT, 14, 1),
        add_highlight_range(PLAIN_IDENTIFIER, 16, 21),
        add_highlight_range(SUBTRACTION, 38, 1),
        add_highlight_range(LITERAL_INTEGER, 40, 1),
        add_highlight_range(FUNCTION_CALL, 41, 1),  // consider adding a separate element into hc_syn_instructions.h
        add_highlight_range(STAPLE_BLOCK_ITEMS, 43, 1),  // consider adding a separate element into hc_syn_instructions.h
    }}
},
{"word x = i*2;",
    {{
        {true, 0, "COPY_64", "REG_A", "REG_RANGED_LOOP_VARIABLE", ""},
        {true, 0, "SET_64", "REG_B", "2", ""},
        {true, 0, "MULTIPLY_INT64", "", "", ""},
        {true, 0, "STORE_64", "$x", "REG_A", ""},
    }, {
        add_highlight_range(TYPENAME_WORD, 0, 4),
        add_highlight_range(PLAIN_IDENTIFIER, 5, 1),
        add_highlight_range(INITIALIZE, 7, 1),
        add_highlight_range(PLAIN_IDENTIFIER, 9, 1),
        add_highlight_range(MULTIPLICATION, 10, 1),
        add_highlight_range(LITERAL_INTEGER, 11, 1),
        add_highlight_range(STAPLE_BLOCK_ITEMS, 12, 1),
    }}
},
{"accumulator += x;",
    {{
        {true, 0, "LOAD_64", "REG_A", "$accumulator", ""},
        {true, 0, "LOAD_64", "REG_B", "$x", ""},
        {true, 0, "ADD_INT64", "", "", ""},
        {true, 0, "STORE_64", "$accumulator", "REG_A", ""},
    }, {
        add_highlight_range(PLAIN_IDENTIFIER, 0, 11),
        add_highlight_range(MUTATE, 12, 2),
        add_highlight_range(PLAIN_IDENTIFIER, 15, 1),
        add_highlight_range(STAPLE_BLOCK_ITEMS, 16, 1),
    }}

},
{"for desc (word j: 17, 5)",
    {{
        {true, 0, "SET_64", "REG_C", "17", ""},
        {true, 0, "SET_64", "REG_D", "5", ""},
        {true, 0, "RANGED_NESTED_LOOP_DESCENDING", "REG_C", "REG_D", "$stash[0]"},
    }, {
        add_highlight_range(PLAIN_IDENTIFIER, 0, 8),  // consider adding a separate element into hc_syn_instructions.h
        add_highlight_range(FUNCTION_CALL, 9, 1),  // consider adding a separate element into hc_syn_instructions.h
        add_highlight_range(TYPENAME_WORD, 10, 4),
        add_highlight_range(PLAIN_IDENTIFIER, 15, 1),
        add_highlight_range(INITIALIZE, 16, 1),
        add_highlight_range(LITERAL_INTEGER, 18, 2),
        add_highlight_range(STAPLE_FUNCTION_ARGUMENT, 20, 1),
        add_highlight_range(LITERAL_INTEGER, 22, 1),
        add_highlight_range(FUNCTION_CALL, 23, 1),  // consider adding a separate element into hc_syn_instructions.h
    }}
},
{"accumulator += j;",
    {{
        {true, 0, "LOAD_64", "REG_A", "$accumulator", ""},
        {true, 0, "COPY_64", "REG_B", "REG_RANGED_LOOP_VARIABLE", ""},
        {true, 0, "ADD_INT64", "", "", ""},
        {true, 0, "STORE_64", "$accumulator", "REG_A", ""},
        {false, 0, "RANGED_NESTED_LOOP_DESCENDING_END", "$stash[0]", "", ""},
    }, {
        add_highlight_range(PLAIN_IDENTIFIER, 0, 11),
        add_highlight_range(MUTATE, 12, 2),
        add_highlight_range(PLAIN_IDENTIFIER, 15, 1),
        add_highlight_range(STAPLE_BLOCK_ITEMS, 16, 1),
    }}

},
{"}",
    {{
        {true, 0, "RANGED_LOOP_ASCENDING_END", "", "", ""},
    }, {
        add_highlight_range(STAPLE_BLOCK_ITEMS, 0, 1),  // consider adding a separate element into hc_syn_instructions.h
    }}
},
