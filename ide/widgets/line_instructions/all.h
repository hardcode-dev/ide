{"\"\"\";",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_LITERAL_STRING, 0, 3),
    }}
},
{"--off;",
    {{
        {true, 0, "SUB", "[0x100]", "1", "[0x100]",},
    }, {
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_OPERATOR, 0, 1),
        add_highlight_range (HL_OPERATOR, 1, 1),
        add_highlight_range (HL_OPERATOR, 0, 2),
    }}
},
{"// Last entry",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_COMMENT, 0, 13),
    }}
},
{"// No entries",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_COMMENT, 0, 13),
    }}
},
{"0x20, 0x49, 0xb8, 0x06, 0xa0, 0xec, 0x3d, 0xc6, 0x77, 0x01, 0xe8, 0xe3,",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 10, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 22, 1),
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_DELIMITER, 46, 1),
        add_highlight_range (HL_DELIMITER, 52, 1),
        add_highlight_range (HL_DELIMITER, 58, 1),
        add_highlight_range (HL_DELIMITER, 64, 1),
        add_highlight_range (HL_DELIMITER, 70, 1),
        add_highlight_range (HL_LITERAL, 0, 4),
        add_highlight_range (HL_LITERAL, 6, 4),
        add_highlight_range (HL_LITERAL, 12, 4),
        add_highlight_range (HL_LITERAL, 18, 4),
        add_highlight_range (HL_LITERAL, 24, 4),
        add_highlight_range (HL_LITERAL, 30, 4),
        add_highlight_range (HL_LITERAL, 36, 4),
        add_highlight_range (HL_LITERAL, 42, 4),
        add_highlight_range (HL_LITERAL, 48, 4),
        add_highlight_range (HL_LITERAL, 54, 4),
        add_highlight_range (HL_LITERAL, 60, 4),
        add_highlight_range (HL_LITERAL, 66, 4),
        add_highlight_range (HL_LITERAL, 66, 4),
        add_highlight_range (HL_LITERAL, 12, 4),
    }}
},
{"0x6c, 0x5a, 0x69, 0x55, 0x73, 0xe3, 0x8e, 0x10, 0xb9, 0x0a, 0x09, 0xa8,",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 10, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 22, 1),
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_DELIMITER, 46, 1),
        add_highlight_range (HL_DELIMITER, 52, 1),
        add_highlight_range (HL_DELIMITER, 58, 1),
        add_highlight_range (HL_DELIMITER, 64, 1),
        add_highlight_range (HL_DELIMITER, 70, 1),
        add_highlight_range (HL_LITERAL, 30, 4),
        add_highlight_range (HL_LITERAL, 0, 4),
        add_highlight_range (HL_LITERAL, 6, 4),
        add_highlight_range (HL_LITERAL, 12, 4),
        add_highlight_range (HL_LITERAL, 18, 4),
        add_highlight_range (HL_LITERAL, 24, 4),
        add_highlight_range (HL_LITERAL, 30, 4),
        add_highlight_range (HL_LITERAL, 36, 4),
        add_highlight_range (HL_LITERAL, 42, 4),
        add_highlight_range (HL_LITERAL, 48, 4),
        add_highlight_range (HL_LITERAL, 54, 4),
        add_highlight_range (HL_LITERAL, 60, 4),
        add_highlight_range (HL_LITERAL, 66, 4),
    }}
},
{"0xcd, 0x9b, 0x1d, 0x18, 0x08, 0x74, 0x62, 0x51, 0xd7, 0x3c, 0xb8, 0x13,",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 10, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 22, 1),
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_DELIMITER, 46, 1),
        add_highlight_range (HL_DELIMITER, 52, 1),
        add_highlight_range (HL_DELIMITER, 58, 1),
        add_highlight_range (HL_DELIMITER, 64, 1),
        add_highlight_range (HL_DELIMITER, 70, 1),
        add_highlight_range (HL_LITERAL, 60, 4),
        add_highlight_range (HL_LITERAL, 0, 4),
        add_highlight_range (HL_LITERAL, 6, 4),
        add_highlight_range (HL_LITERAL, 12, 4),
        add_highlight_range (HL_LITERAL, 18, 4),
        add_highlight_range (HL_LITERAL, 24, 4),
        add_highlight_range (HL_LITERAL, 30, 4),
        add_highlight_range (HL_LITERAL, 36, 4),
        add_highlight_range (HL_LITERAL, 42, 4),
        add_highlight_range (HL_LITERAL, 48, 4),
        add_highlight_range (HL_LITERAL, 54, 4),
        add_highlight_range (HL_LITERAL, 60, 4),
        add_highlight_range (HL_LITERAL, 66, 4),
    }}
},
{"Banana\\t7\\t8430983482049824309823409238271892731728909405007700359943812098098019382102938103928",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_LITERAL_STRING, 0, 96),
    }}
},
{"Pacific oyster\\t3\\t54293840283444430930930940982034209384029841845734598733475776665323444309318731289312",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_LITERAL_STRING, 0, 105),
    }}
},
{"Pineapple juice\\t1\\t14723904823048249023847877388388320100102093823922304982309428308023984230",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_LITERAL_STRING, 0, 94),
    }}
},
{"Title\\tCount\\tPrice",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_LITERAL_STRING, 0, 19),
    }}
},
{"Tuna can\\t3\\t124522182931028273492734921740912093810938103981209182039180328039289832938",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_LITERAL_STRING, 0, 88),
    }}
},
{"base10p9[i] = n;",
    {{
        {true, 0, "PUSH", "0x50", "", "",},
        {true, 0, "SET", "[*0x48]", "*0x50", "",},
    }, {
        add_highlight_range (HL_DELIMITER, 15, 1),
        add_highlight_range (HL_DELIMITER, 8, 1),
        add_highlight_range (HL_DELIMITER, 10, 1),
        add_highlight_range (HL_OPERATOR, 12, 1),
    }}
},
{"base256.resize (off);",
    {{
        {true, 0, "ARR_RESIZE", "[*0x30]", "0x100", "",},
    }, {
        add_highlight_range (HL_DELIMITER, 20, 1),
        add_highlight_range (HL_DELIMITER, 15, 1),
        add_highlight_range (HL_DELIMITER, 19, 1),
        add_highlight_range (HL_DELIMITER, 7, 1),
    }}
},
{"base256[k] = (n >> j*8) & 0xff;",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "MULT", "[0x130]", "8", "[0x140]",},
        {true, 0, "RSHIFT", "[0x128]", "[0x140]", "[0x140]",},
        {true, 0, "BOR", "[0x140]", "255", "[0x140]",},
        {true, 0, "ARR_INDEX", "[0x30]", "[0x138]", "[0x140]",},
        {true, 0, "SET", "[*0x140]", "[0x138]", "",},
    }, {
        add_highlight_range (HL_DELIMITER, 30, 1),
        add_highlight_range (HL_DELIMITER, 7, 1),
        add_highlight_range (HL_DELIMITER, 9, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_DELIMITER, 22, 1),
        add_highlight_range (HL_OPERATOR, 11, 1),
        add_highlight_range (HL_OPERATOR, 20, 1),
        add_highlight_range (HL_OPERATOR, 16, 2),
        add_highlight_range (HL_OPERATOR, 16, 1),
        add_highlight_range (HL_OPERATOR, 17, 1),
        add_highlight_range (HL_LITERAL, 21, 1),
        add_highlight_range (HL_LITERAL, 26, 4),
    }}
},
{"bill.entry entry = must bill.parse_entry (line, off, last);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 19, 4),
        add_highlight_range (HL_DELIMITER, 58, 1),
        add_highlight_range (HL_DELIMITER, 46, 1),
        add_highlight_range (HL_DELIMITER, 51, 1),
        add_highlight_range (HL_DELIMITER, 41, 1),
        add_highlight_range (HL_DELIMITER, 57, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_OPERATOR, 17, 1),
    }}
},
{"bill.entry entry = must bill.parse_entry (line, off, sep_pos - 1);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 19, 4),
        add_highlight_range (HL_DELIMITER, 65, 1),
        add_highlight_range (HL_DELIMITER, 46, 1),
        add_highlight_range (HL_DELIMITER, 51, 1),
        add_highlight_range (HL_DELIMITER, 41, 1),
        add_highlight_range (HL_DELIMITER, 64, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_OPERATOR, 17, 1),
        add_highlight_range (HL_OPERATOR, 61, 1),
        add_highlight_range (HL_LITERAL, 63, 1),
    }}
},
{"bill.entry result;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 17, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
    }}
},
{"bill.head head = must bill.parse_head (line, off, last);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 17, 4),
        add_highlight_range (HL_DELIMITER, 55, 1),
        add_highlight_range (HL_DELIMITER, 43, 1),
        add_highlight_range (HL_DELIMITER, 48, 1),
        add_highlight_range (HL_DELIMITER, 38, 1),
        add_highlight_range (HL_DELIMITER, 54, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 26, 1),
        add_highlight_range (HL_OPERATOR, 15, 1),
    }}
},
{"bill.head head = must bill.parse_head (line, off, sep_pos);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 17, 4),
        add_highlight_range (HL_DELIMITER, 58, 1),
        add_highlight_range (HL_DELIMITER, 43, 1),
        add_highlight_range (HL_DELIMITER, 48, 1),
        add_highlight_range (HL_DELIMITER, 38, 1),
        add_highlight_range (HL_DELIMITER, 57, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 26, 1),
        add_highlight_range (HL_OPERATOR, 15, 1),
    }}
},
{"bill.head result;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
    }}
},
{"break;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 5),
        add_highlight_range (HL_DELIMITER, 5, 1),
    }}
},
{"carry = n & 0xffffffff;",
    {{
        {true, 0, "BAND", "*0x50", "4294967295", "[*0x40]",},
    }, {
        add_highlight_range (HL_DELIMITER, 22, 1),
        add_highlight_range (HL_OPERATOR, 6, 1),
        add_highlight_range (HL_LITERAL, 12, 10),
    }}
},
{"carry = sum >> 8;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_OPERATOR, 6, 1),
        add_highlight_range (HL_OPERATOR, 12, 2),
        add_highlight_range (HL_OPERATOR, 12, 1),
        add_highlight_range (HL_OPERATOR, 13, 1),
        add_highlight_range (HL_LITERAL, 15, 1),
    }}
},
{"for (index (a_value) a_idx asc a_first_nz, #a_value - 1) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 27, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 41, 1),
        add_highlight_range (HL_DELIMITER, 57, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 19, 1),
        add_highlight_range (HL_DELIMITER, 55, 1),
        add_highlight_range (HL_OPERATOR, 52, 1),
        add_highlight_range (HL_OPERATOR, 43, 1),
        add_highlight_range (HL_LITERAL, 54, 1),
    }}
},
{"for (index (base10) i asc first, last) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 22, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 31, 1),
        add_highlight_range (HL_DELIMITER, 39, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 18, 1),
        add_highlight_range (HL_DELIMITER, 37, 1),
    }}
},
{"for (index (base10p9) i asc first, #base10p9 - 1) {",
    {{
        {true, 0, "STRUCT_INDEX", "[*0x18]", "1", "[*0x28]",},
        {true, 0, "STRUCT_INDEX", "[*0x18]", "0", "[*0x30]",},
        {true, 0, "PUSH", "*0x38", "", "",},
        {true, 0, "ARR_LENGTH", "[*0x30]", "*0x38", "",},
        {true, 0, "LOOP_ASCENDING","[*0x28]", "*0x38", "[*0x40]",},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 24, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 33, 1),
        add_highlight_range (HL_DELIMITER, 50, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 20, 1),
        add_highlight_range (HL_DELIMITER, 48, 1),
        add_highlight_range (HL_OPERATOR, 45, 1),
        add_highlight_range (HL_OPERATOR, 35, 1),
        add_highlight_range (HL_LITERAL, 47, 1),
    }}
},
{"for (index (line) i asc off, last) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 20, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 27, 1),
        add_highlight_range (HL_DELIMITER, 35, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 33, 1),
    }}
},
{"for (index (ret) i asc 0, #ret - 1) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 19, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 24, 1),
        add_highlight_range (HL_DELIMITER, 36, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 15, 1),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_OPERATOR, 31, 1),
        add_highlight_range (HL_OPERATOR, 26, 1),
        add_highlight_range (HL_LITERAL, 23, 1),
        add_highlight_range (HL_LITERAL, 33, 1),
    }}
},
{"for (index (s) i asc 0, #s - 1)",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 17, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 22, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_DELIMITER, 30, 1),
        add_highlight_range (HL_OPERATOR, 27, 1),
        add_highlight_range (HL_OPERATOR, 24, 1),
        add_highlight_range (HL_LITERAL, 21, 1),
        add_highlight_range (HL_LITERAL, 29, 1),
    }}
},
{"for (index (s) i asc first, last)",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 17, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 26, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_DELIMITER, 32, 1),
    }}
},
{"for (index (state) i asc 0, #state - 1) {",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "SUB", "[0x50]", "1", "[0x110]",},
        {true, 0, "LOOP_ASCENDING","[0x108]", "0", "[0x110]"},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 21, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 26, 1),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 17, 1),
        add_highlight_range (HL_DELIMITER, 38, 1),
        add_highlight_range (HL_OPERATOR, 35, 1),
        add_highlight_range (HL_OPERATOR, 28, 1),
        add_highlight_range (HL_LITERAL, 25, 1),
        add_highlight_range (HL_LITERAL, 37, 1),
    }}
},
{"for (uword i asc 0, full_base1b_digits - 1) {",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "SUB", "[0x40]", "1", "[0xC0]",},
        {true, 0, "LOOP_ASCENDING","[0xB0]", "0", "[0xB8]"},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 13, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 18, 1),
        add_highlight_range (HL_DELIMITER, 44, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 42, 1),
        add_highlight_range (HL_OPERATOR, 39, 1),
        add_highlight_range (HL_LITERAL, 17, 1),
        add_highlight_range (HL_LITERAL, 41, 1),
    }}
},
{"for (uword j asc 0, 3) {",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "LOOP_ASCENDING","[0x130]", "0", "3"},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 13, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 18, 1),
        add_highlight_range (HL_DELIMITER, 23, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 21, 1),
        add_highlight_range (HL_LITERAL, 17, 1),
        add_highlight_range (HL_LITERAL, 20, 1),
    }}
},
{"for (word i asc #a, #value - 2) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 12, 3),
        add_highlight_range (HL_TYPE, 5, 4),
        add_highlight_range (HL_DELIMITER, 18, 1),
        add_highlight_range (HL_DELIMITER, 32, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 30, 1),
        add_highlight_range (HL_OPERATOR, 27, 1),
        add_highlight_range (HL_OPERATOR, 16, 1),
        add_highlight_range (HL_OPERATOR, 20, 1),
        add_highlight_range (HL_LITERAL, 29, 1),
    }}
},
{"for (word:ranged (0, #a - 1) i asc 0, #a - 1) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 31, 3),
        add_highlight_range (HL_TYPE, 5, 4),
        add_highlight_range (HL_TYPE, 10, 6),
        add_highlight_range (HL_DELIMITER, 19, 1),
        add_highlight_range (HL_DELIMITER, 36, 1),
        add_highlight_range (HL_DELIMITER, 46, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 17, 1),
        add_highlight_range (HL_DELIMITER, 27, 1),
        add_highlight_range (HL_DELIMITER, 44, 1),
        add_highlight_range (HL_OPERATOR, 24, 1),
        add_highlight_range (HL_OPERATOR, 41, 1),
        add_highlight_range (HL_OPERATOR, 21, 1),
        add_highlight_range (HL_OPERATOR, 38, 1),
        add_highlight_range (HL_LITERAL, 18, 1),
        add_highlight_range (HL_LITERAL, 35, 1),
        add_highlight_range (HL_LITERAL, 26, 1),
        add_highlight_range (HL_LITERAL, 43, 1),
    }}
},
{"if (!test_calc_sum_tsv ())",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 2),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 23, 1),
        add_highlight_range (HL_DELIMITER, 24, 1),
        add_highlight_range (HL_DELIMITER, 25, 1),
    }}
},
{"if (#value < #a + 1)",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 2),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 19, 1),
        add_highlight_range (HL_OPERATOR, 16, 1),
        add_highlight_range (HL_OPERATOR, 4, 1),
        add_highlight_range (HL_OPERATOR, 13, 1),
        add_highlight_range (HL_OPERATOR, 11, 1),
        add_highlight_range (HL_LITERAL, 18, 1),
    }}
},
{"if (a_value[a_idx] != b_value[b_idx])",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 2),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 29, 1),
        add_highlight_range (HL_DELIMITER, 17, 1),
        add_highlight_range (HL_DELIMITER, 35, 1),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 36, 1),
        add_highlight_range (HL_OPERATOR, 20, 1),
    }}
},
{"if (base10p9 < 0) {",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "LESS", "[0xA0]", "0", "[0xA8]"},
        {true, 0, "IFNZ", "[0xA8]", "", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 2),
        add_highlight_range (HL_DELIMITER, 18, 1),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_OPERATOR, 13, 1),
        add_highlight_range (HL_LITERAL, 15, 1),
    }}
},
{"if (base10p9 < 0) {⁠",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "LESS", "[0xF0]", "0", "[0xF8]"},
        {true, 0, "IFNZ", "[0xF8]", "", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 2),
        add_highlight_range (HL_DELIMITER, 18, 1),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_OPERATOR, 13, 1),
        add_highlight_range (HL_LITERAL, 15, 1),
    }}
},
{"if (c < '0' || c > '9')",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 2),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 22, 1),
        add_highlight_range (HL_OPERATOR, 17, 1),
        add_highlight_range (HL_OPERATOR, 6, 1),
        add_highlight_range (HL_LITERAL_STRING, 8, 3),
        add_highlight_range (HL_LITERAL_STRING, 19, 3),
        add_highlight_range (HL_LITERAL_STRING, 8, 3),
        add_highlight_range (HL_LITERAL, 9, 1),
        add_highlight_range (HL_LITERAL, 20, 1),
    }}
},
{"if (heading_base1b_digits != 0) {",
    {{
        {true, 0, "IFNZ", "[0x38]", "", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 2),
        add_highlight_range (HL_DELIMITER, 32, 1),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 30, 1),
        add_highlight_range (HL_OPERATOR, 27, 1),
        add_highlight_range (HL_LITERAL, 29, 1),
    }}
},
{"if (off >= #line)",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 2),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_OPERATOR, 9, 1),
        add_highlight_range (HL_OPERATOR, 11, 1),
        add_highlight_range (HL_OPERATOR, 8, 1),
        add_highlight_range (HL_OPERATOR, 8, 2),
    }}
},
{"if (s[i] != 0)",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 2),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 7, 1),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_OPERATOR, 10, 1),
        add_highlight_range (HL_LITERAL, 12, 1),
    }}
},
{"if (s[i] == char)",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 2),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 7, 1),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_OPERATOR, 9, 1),
        add_highlight_range (HL_OPERATOR, 10, 1),
    }}
},
{"index (b_value) b_idx = a_idx - a_first_nz + b_first_nz;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 55, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 14, 1),
        add_highlight_range (HL_OPERATOR, 22, 1),
        add_highlight_range (HL_OPERATOR, 43, 1),
        add_highlight_range (HL_OPERATOR, 30, 1),
    }}
},
{"index (base10) first = must off;",
    {{
        {true, 0, "SUB", "[*0x8]+0x8", "1", "[0x70]",},
        {true, 0, "LESS_OR_GREATER", "[0x68]", "[0x70]", "[0x70]"},
        {true, 0, "IFNZ", "[0x70]", "", ""},
        {false, 0, "ENDIF", "", "", "",},
        {true, 0, "SET", "[0x70]", "0x30", "",},
    }, {
        add_highlight_range (HL_KEYWORD, 23, 4),
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 31, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_OPERATOR, 21, 1),
    }}
},
{"index (base10) first = must off;⁠",
    {{
        {true, 0, "SUB", "[*0x8]+0x8", "1", "[0xC0]",},
        {true, 0, "LESS_OR_GREATER", "[0x68]", "[0xC0]", "[0xC0]"},
        {true, 0, "IFNZ", "[0xC0]", "", ""},
        {false, 0, "ENDIF", "", "", "",},
        {true, 0, "SET", "[0xC0]", "0x30", "",},
    }, {
        add_highlight_range (HL_KEYWORD, 23, 4),
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 31, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_OPERATOR, 21, 1),
    }}
},
{"index (base10) last = must off + 8;",
    {{
        {true, 0, "ADD", "[0x68]", "1", "[0xC8]",},
        {true, 0, "SUB", "[*0x8]+0x8", "1", "[0xC8]",},
        {true, 0, "LESS_OR_GREATER", "[0xC8]", "[0x80]", "[0x80]"},
        {true, 0, "IFNZ", "[0x80]", "", ""},
        {false, 0, "ENDIF", "", "", "",},
    }, {
        add_highlight_range (HL_KEYWORD, 22, 4),
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_OPERATOR, 20, 1),
        add_highlight_range (HL_OPERATOR, 31, 1),
        add_highlight_range (HL_LITERAL, 33, 1),
    }}
},
{"index (base10) last = must off + heading_base1b_digits - 1;",
    {{
        {true, 0, "ADD", "[0x68]", "[0x38]", "[0x78]",},
        {true, 0, "SUB", "[0x78]", "1", "[0x78]",},
        {true, 0, "SUB", "[*0x8]+0x8", "1", "[0x78]",},
        {true, 0, "LESS_OR_GREATER", "[0x78]", "[0x80]", "[0x80]"},
        {true, 0, "IFNZ", "[0x80]", "", ""},
        {false, 0, "ENDIF", "", "", "",},
    }, {
        add_highlight_range (HL_KEYWORD, 22, 4),
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 58, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_OPERATOR, 20, 1),
        add_highlight_range (HL_OPERATOR, 31, 1),
        add_highlight_range (HL_OPERATOR, 55, 1),
        add_highlight_range (HL_LITERAL, 57, 1),
    }}
},
{"index (base256) k = must off;",
    {{
        {true, 0, "SUB", "[*0x30]+0x8", "1", "[0x138]",},
        {true, 0, "LESS_OR_GREATER", "[0x100]", "[0x138]", "[0x138]"},
        {true, 0, "IFNZ", "[0x138]", "", ""},
        {false, 0, "ENDIF", "", "", "",},
        {true, 0, "SET", "[0x138]", "0x138", "",},
    }, {
        add_highlight_range (HL_KEYWORD, 20, 4),
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 14, 1),
        add_highlight_range (HL_OPERATOR, 18, 1),
    }}
},
{"index (line) last = must #line - 1;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 20, 4),
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_OPERATOR, 18, 1),
        add_highlight_range (HL_OPERATOR, 31, 1),
        add_highlight_range (HL_OPERATOR, 25, 1),
        add_highlight_range (HL_LITERAL, 33, 1),
    }}
},
{"index (line) sep_pos = must string.find (line, '\\t', off, last);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 23, 4),
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 63, 1),
        add_highlight_range (HL_DELIMITER, 45, 1),
        add_highlight_range (HL_DELIMITER, 51, 1),
        add_highlight_range (HL_DELIMITER, 56, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 62, 1),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_OPERATOR, 21, 1),
        add_highlight_range (HL_LITERAL_STRING, 47, 4),
        add_highlight_range (HL_LITERAL_STRING, 47, 4),
    }}
},
{"index (line) sub_last = must sep_pos - 1;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 24, 4),
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_OPERATOR, 22, 1),
        add_highlight_range (HL_OPERATOR, 37, 1),
        add_highlight_range (HL_LITERAL, 39, 1),
    }}
},
{"index (ret) j = i + first;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 25, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 10, 1),
        add_highlight_range (HL_OPERATOR, 14, 1),
        add_highlight_range (HL_OPERATOR, 18, 1),
    }}
},
{"let ((#a_value - a_first_nz) == (#b_value - b_first_nz)) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_DELIMITER, 57, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 32, 1),
        add_highlight_range (HL_DELIMITER, 27, 1),
        add_highlight_range (HL_DELIMITER, 54, 1),
        add_highlight_range (HL_DELIMITER, 55, 1),
        add_highlight_range (HL_OPERATOR, 29, 1),
        add_highlight_range (HL_OPERATOR, 30, 1),
        add_highlight_range (HL_OPERATOR, 15, 1),
        add_highlight_range (HL_OPERATOR, 42, 1),
        add_highlight_range (HL_OPERATOR, 6, 1),
        add_highlight_range (HL_OPERATOR, 33, 1),
    }}
},
{"let (index (a) a_start = string.find_non_zero (a)) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 51, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 46, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_DELIMITER, 48, 1),
        add_highlight_range (HL_DELIMITER, 49, 1),
        add_highlight_range (HL_DELIMITER, 31, 1),
        add_highlight_range (HL_OPERATOR, 23, 1),
    }}
},
{"let (index (a_value) a_first_nz = string.find_non_zero (a_value)) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 66, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 55, 1),
        add_highlight_range (HL_DELIMITER, 19, 1),
        add_highlight_range (HL_DELIMITER, 63, 1),
        add_highlight_range (HL_DELIMITER, 64, 1),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_OPERATOR, 32, 1),
    }}
},
{"let (index (b_value) b_first_nz = string.find_non_zero (b_value)) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 66, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 55, 1),
        add_highlight_range (HL_DELIMITER, 19, 1),
        add_highlight_range (HL_DELIMITER, 63, 1),
        add_highlight_range (HL_DELIMITER, 64, 1),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_OPERATOR, 32, 1),
    }}
},
{"let (index (line) i = string.find (line, '\\t', off, last))",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 39, 1),
        add_highlight_range (HL_DELIMITER, 45, 1),
        add_highlight_range (HL_DELIMITER, 50, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 56, 1),
        add_highlight_range (HL_DELIMITER, 57, 1),
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_OPERATOR, 20, 1),
        add_highlight_range (HL_LITERAL_STRING, 41, 4),
        add_highlight_range (HL_LITERAL_STRING, 41, 4),
    }}
},
{"let (index (line) sep_pos = string.find (line, '\\n', off, last)) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 45, 1),
        add_highlight_range (HL_DELIMITER, 51, 1),
        add_highlight_range (HL_DELIMITER, 56, 1),
        add_highlight_range (HL_DELIMITER, 65, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 62, 1),
        add_highlight_range (HL_DELIMITER, 63, 1),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_OPERATOR, 26, 1),
        add_highlight_range (HL_LITERAL_STRING, 47, 4),
    }}
},
{"let (index (value) i = 0)",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 17, 1),
        add_highlight_range (HL_DELIMITER, 24, 1),
        add_highlight_range (HL_OPERATOR, 21, 1),
        add_highlight_range (HL_LITERAL, 23, 1),
    }}
},
{"let (index (value) value_i = #value - 1 - i) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 5, 5),
        add_highlight_range (HL_DELIMITER, 45, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 17, 1),
        add_highlight_range (HL_DELIMITER, 43, 1),
        add_highlight_range (HL_OPERATOR, 27, 1),
        add_highlight_range (HL_OPERATOR, 36, 1),
        add_highlight_range (HL_OPERATOR, 40, 1),
        add_highlight_range (HL_OPERATOR, 29, 1),
        add_highlight_range (HL_LITERAL, 38, 1),
    }}
},
{"let (off := sep_pos + 1) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_DELIMITER, 25, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 23, 1),
        add_highlight_range (HL_OPERATOR, 9, 2),
        add_highlight_range (HL_OPERATOR, 10, 1),
        add_highlight_range (HL_OPERATOR, 20, 1),
        add_highlight_range (HL_LITERAL, 22, 1),
    }}
},
{"let (types.varuint sum = bill.calc_sum_tsv (tsv, #tsv, sum)) {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_DELIMITER, 47, 1),
        add_highlight_range (HL_DELIMITER, 53, 1),
        add_highlight_range (HL_DELIMITER, 61, 1),
        add_highlight_range (HL_DELIMITER, 4, 1),
        add_highlight_range (HL_DELIMITER, 43, 1),
        add_highlight_range (HL_DELIMITER, 58, 1),
        add_highlight_range (HL_DELIMITER, 59, 1),
        add_highlight_range (HL_DELIMITER, 10, 1),
        add_highlight_range (HL_DELIMITER, 29, 1),
        add_highlight_range (HL_OPERATOR, 23, 1),
        add_highlight_range (HL_OPERATOR, 49, 1),
    }}
},
{"long base10p9 = .base10_xn_to_base10p9 (base10, first, last);",
    {{
        {true, 0, "PUSH", "[0x10]", "", "",},
        {true, 0, "PUSH", "[0x70]", "", "",},
        {true, 0, "PUSH", "[0x78]", "", "",},
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "CALL", "4", "[0x88]", "",},
    }, {
        add_highlight_range (HL_TYPE, 0, 4),
        add_highlight_range (HL_DELIMITER, 60, 1),
        add_highlight_range (HL_DELIMITER, 46, 1),
        add_highlight_range (HL_DELIMITER, 53, 1),
        add_highlight_range (HL_DELIMITER, 39, 1),
        add_highlight_range (HL_DELIMITER, 59, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_OPERATOR, 14, 1),
    }}
},
{"long base10p9 = .base10_xn_to_base10p9 (base10, first, last);⁠",
    {{
        {true, 0, "PUSH", "[0x10]", "", "",},
        {true, 0, "PUSH", "[0xC0]", "", "",},
        {true, 0, "PUSH", "[0xD0]", "", "",},
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "CALL", "4", "[0xD8]", "",},
    }, {
        add_highlight_range (HL_TYPE, 0, 4),
        add_highlight_range (HL_DELIMITER, 60, 1),
        add_highlight_range (HL_DELIMITER, 46, 1),
        add_highlight_range (HL_DELIMITER, 53, 1),
        add_highlight_range (HL_DELIMITER, 39, 1),
        add_highlight_range (HL_DELIMITER, 59, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_OPERATOR, 14, 1),
    }}
},
{"long carry = 0;",
    {{
        {true, 0, "PUSH", "0", "", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 4),
        add_highlight_range (HL_DELIMITER, 14, 1),
        add_highlight_range (HL_OPERATOR, 11, 1),
        add_highlight_range (HL_LITERAL, 13, 1),
    }}
},
{"long n = .divide_by_256p4 (state, i);",
    {{
        {true, 0, "PUSH", "0x50", "", "",},
        {true, 0, "PUSH", "[0x108]", "", "",},
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "CALL", "3", "[0x118]", "",},
    }, {
        add_highlight_range (HL_TYPE, 0, 4),
        add_highlight_range (HL_DELIMITER, 36, 1),
        add_highlight_range (HL_DELIMITER, 32, 1),
        add_highlight_range (HL_DELIMITER, 26, 1),
        add_highlight_range (HL_DELIMITER, 35, 1),
        add_highlight_range (HL_DELIMITER, 9, 1),
        add_highlight_range (HL_OPERATOR, 7, 1),
    }}
},
{"long n = 0;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 4),
        add_highlight_range (HL_DELIMITER, 10, 1),
        add_highlight_range (HL_OPERATOR, 7, 1),
        add_highlight_range (HL_LITERAL, 9, 1),
    }}
},
{"long n = base10p9[i];",
    {{
        {true, 0, "ARR_INDEX", "[*0x30]", "[*0x40]", "[*0x48]",},
        {true, 0, "PUSH", "*0x50", "", "",},
        {true, 0, "SET", "*0x50", "[0x48]", "",},
    }, {
        add_highlight_range (HL_TYPE, 0, 4),
        add_highlight_range (HL_DELIMITER, 20, 1),
        add_highlight_range (HL_DELIMITER, 17, 1),
        add_highlight_range (HL_DELIMITER, 19, 1),
        add_highlight_range (HL_OPERATOR, 7, 1),
    }}
},
{"mut index (line) off = must 0;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_KEYWORD, 23, 4),
        add_highlight_range (HL_TYPE, 4, 5),
        add_highlight_range (HL_DELIMITER, 29, 1),
        add_highlight_range (HL_DELIMITER, 10, 1),
        add_highlight_range (HL_DELIMITER, 15, 1),
        add_highlight_range (HL_OPERATOR, 21, 1),
        add_highlight_range (HL_LITERAL, 28, 1),
    }}
},
{"mut long[] state;",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "PUSH", "0", "", "",},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 4, 4),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 8, 1),
        add_highlight_range (HL_DELIMITER, 9, 1),
    }}
},
{"mut types.varuint sum = types.varuint ();",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_DELIMITER, 38, 1),
        add_highlight_range (HL_DELIMITER, 39, 1),
        add_highlight_range (HL_DELIMITER, 9, 1),
        add_highlight_range (HL_DELIMITER, 29, 1),
        add_highlight_range (HL_OPERATOR, 22, 1),
    }}
},
{"mut ubyte[]& base256 = result.value;",
    {{
        {true, 0, "SET", "*0x18", "0x30", "",},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 4, 5),
        add_highlight_range (HL_DELIMITER, 35, 1),
        add_highlight_range (HL_DELIMITER, 9, 1),
        add_highlight_range (HL_DELIMITER, 10, 1),
        add_highlight_range (HL_DELIMITER, 29, 1),
        add_highlight_range (HL_OPERATOR, 21, 1),
    }}
},
{"mut uword off = 0;",
    {{
        {true, 0, "PUSH", "0", "", "",},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 4, 5),
        add_highlight_range (HL_DELIMITER, 17, 1),
        add_highlight_range (HL_OPERATOR, 14, 1),
        add_highlight_range (HL_LITERAL, 16, 1),
    }}
},
{"mut uword off = total_base1b_digits*4;",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "MULT", "[0x48]", "4", "[0x100]",},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 3),
        add_highlight_range (HL_TYPE, 4, 5),
        add_highlight_range (HL_DELIMITER, 37, 1),
        add_highlight_range (HL_OPERATOR, 14, 1),
        add_highlight_range (HL_OPERATOR, 35, 1),
        add_highlight_range (HL_LITERAL, 36, 1),
    }}
},
{"n += carry*1000000000;",
    {{
        {true, 0, "MULT", "[*0x20]", "1000000000", "[*0x20]",},
        {true, 0, "ADD", "[*0x20]", "*0x50", "*0x50",},
    }, {
        add_highlight_range (HL_DELIMITER, 21, 1),
        add_highlight_range (HL_OPERATOR, 3, 1),
        add_highlight_range (HL_OPERATOR, 2, 1),
        add_highlight_range (HL_OPERATOR, 2, 2),
        add_highlight_range (HL_OPERATOR, 10, 1),
        add_highlight_range (HL_LITERAL, 11, 10),
    }}
},
{"n = n*10 + (c - '0');",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 20, 1),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_DELIMITER, 19, 1),
        add_highlight_range (HL_OPERATOR, 2, 1),
        add_highlight_range (HL_OPERATOR, 9, 1),
        add_highlight_range (HL_OPERATOR, 14, 1),
        add_highlight_range (HL_OPERATOR, 5, 1),
        add_highlight_range (HL_LITERAL_STRING, 16, 3),
        add_highlight_range (HL_LITERAL_STRING, 16, 3),
        add_highlight_range (HL_LITERAL, 17, 1),
        add_highlight_range (HL_LITERAL, 6, 2),
    }}
},
{"n >>= 32;",
    {{
        {true, 0, "BSR", "*0x50", "32", "[*0x50]",},
    }, {
        add_highlight_range (HL_DELIMITER, 8, 1),
        add_highlight_range (HL_OPERATOR, 4, 1),
        add_highlight_range (HL_OPERATOR, 2, 2),
        add_highlight_range (HL_OPERATOR, 2, 1),
        add_highlight_range (HL_OPERATOR, 3, 1),
        add_highlight_range (HL_OPERATOR, 3, 2),
        add_highlight_range (HL_LITERAL, 6, 2),
    }}
},
{"off += 9;",
    {{
        {true, 0, "ADD", "[0x68]", "9", "[0x68]",},
    }, {
        add_highlight_range (HL_DELIMITER, 8, 1),
        add_highlight_range (HL_OPERATOR, 5, 1),
        add_highlight_range (HL_OPERATOR, 4, 1),
        add_highlight_range (HL_OPERATOR, 4, 2),
        add_highlight_range (HL_LITERAL, 7, 1),
    }}
},
{"off += heading_base1b_digits;",
    {{
        {true, 0, "ADD", "[0x68]", "[0x38]", "[0x68]",},
    }, {
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_OPERATOR, 5, 1),
        add_highlight_range (HL_OPERATOR, 4, 1),
        add_highlight_range (HL_OPERATOR, 4, 2),
    }}
},
{"off := must sep_pos + 1;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 7, 4),
        add_highlight_range (HL_DELIMITER, 23, 1),
        add_highlight_range (HL_OPERATOR, 4, 2),
        add_highlight_range (HL_OPERATOR, 5, 1),
        add_highlight_range (HL_OPERATOR, 20, 1),
        add_highlight_range (HL_LITERAL, 22, 1),
    }}
},
{"result.count = must types.varuint.parse_base10 (line, off, sub_last);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 15, 4),
        add_highlight_range (HL_DELIMITER, 68, 1),
        add_highlight_range (HL_DELIMITER, 52, 1),
        add_highlight_range (HL_DELIMITER, 57, 1),
        add_highlight_range (HL_DELIMITER, 47, 1),
        add_highlight_range (HL_DELIMITER, 67, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 25, 1),
        add_highlight_range (HL_DELIMITER, 33, 1),
        add_highlight_range (HL_OPERATOR, 13, 1),
    }}
},
{"result.count = string.substr (line, off, sub_last);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 50, 1),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_DELIMITER, 39, 1),
        add_highlight_range (HL_DELIMITER, 29, 1),
        add_highlight_range (HL_DELIMITER, 49, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 21, 1),
        add_highlight_range (HL_OPERATOR, 13, 1),
    }}
},
{"result.price = must types.varuint.parse_base10 (line, off, last);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 15, 4),
        add_highlight_range (HL_DELIMITER, 64, 1),
        add_highlight_range (HL_DELIMITER, 52, 1),
        add_highlight_range (HL_DELIMITER, 57, 1),
        add_highlight_range (HL_DELIMITER, 47, 1),
        add_highlight_range (HL_DELIMITER, 63, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 25, 1),
        add_highlight_range (HL_DELIMITER, 33, 1),
        add_highlight_range (HL_OPERATOR, 13, 1),
    }}
},
{"result.price = string.substr (line, off, last);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 46, 1),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_DELIMITER, 39, 1),
        add_highlight_range (HL_DELIMITER, 29, 1),
        add_highlight_range (HL_DELIMITER, 45, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 21, 1),
        add_highlight_range (HL_OPERATOR, 13, 1),
    }}
},
{"result.title = string.substr (line, off, sub_last);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 50, 1),
        add_highlight_range (HL_DELIMITER, 34, 1),
        add_highlight_range (HL_DELIMITER, 39, 1),
        add_highlight_range (HL_DELIMITER, 29, 1),
        add_highlight_range (HL_DELIMITER, 49, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 21, 1),
        add_highlight_range (HL_OPERATOR, 13, 1),
    }}
},
{"ret[i] = s[j];",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 10, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 12, 1),
        add_highlight_range (HL_OPERATOR, 7, 1),
    }}
},
{"return -1;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 9, 1),
        add_highlight_range (HL_OPERATOR, 7, 1),
        add_highlight_range (HL_LITERAL, 8, 1),
    }}
},
{"return 0;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 8, 1),
        add_highlight_range (HL_LITERAL, 7, 1),
    }}
},
{"return 1;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 8, 1),
        add_highlight_range (HL_LITERAL, 7, 1),
    }}
},
{"return carry;",
    {{
        {true, 0, "SET", "[*0x20]", "-1", ""},
        {true, 0, "RETURN","", "",""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 12, 1),
    }}
},
{"return false;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 12, 1),
        add_highlight_range (HL_LITERAL, 7, 5),
    }}
},
{"return i;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 8, 1),
    }}
},
{"return n;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 8, 1),
    }}
},
{"return null;",
    {{
        {true, 0, "SET", "[*0x8]", "0", ""},
        {true, 0, "RETURN","", "",""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_LITERAL, 7, 4),
    }}
},
{"return result;",
    {{
        {true, 0, "SET", "[0x18]", "[*0x10]", "",},
        {true, 0, "SET", "[0x20]", "[*0x10]+0x8", "",},
        {true, 0, "SET", "[0x28]", "[*0x10]+0x10", "",},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 13, 1),
    }}
},
{"return ret;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 10, 1),
    }}
},
{"return sum;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 10, 1),
    }}
},
{"return true;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 11, 1),
        add_highlight_range (HL_LITERAL, 7, 4),
    }}
},
{"return types.varuint.equals (sum, expected_value);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 0, 6),
        add_highlight_range (HL_DELIMITER, 49, 1),
        add_highlight_range (HL_DELIMITER, 32, 1),
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_DELIMITER, 48, 1),
        add_highlight_range (HL_DELIMITER, 12, 1),
        add_highlight_range (HL_DELIMITER, 20, 1),
    }}
},
{"state <- base10p9;",
    {{
        {true, 0, "ARR_APPEND", "[0xA0]", "[0x50]", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 17, 1),
        add_highlight_range (HL_OPERATOR, 7, 1),
        add_highlight_range (HL_OPERATOR, 6, 2),
        add_highlight_range (HL_OPERATOR, 6, 1),
    }}
},
{"state <- base10p9;⁠",
    {{
        {true, 0, "ARR_APPEND", "[0xF0]", "[0x50]", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 17, 1),
        add_highlight_range (HL_OPERATOR, 7, 1),
        add_highlight_range (HL_OPERATOR, 6, 2),
        add_highlight_range (HL_OPERATOR, 6, 1),
    }}
},
{"sum.add (entry.price);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 21, 1),
        add_highlight_range (HL_DELIMITER, 8, 1),
        add_highlight_range (HL_DELIMITER, 20, 1),
        add_highlight_range (HL_DELIMITER, 3, 1),
        add_highlight_range (HL_DELIMITER, 14, 1),
    }}
},
{"types.varuint expected_value = {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 31, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_OPERATOR, 29, 1),
    }}
},
{"types.varuint result;",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "PUSH", "0", "", "",},
    }, {
        add_highlight_range (HL_DELIMITER, 20, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
    }}
},
{"ubyte c = base10[i];",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 19, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 18, 1),
        add_highlight_range (HL_OPERATOR, 8, 1),
    }}
},
{"ubyte[] ret = (last - first + 1);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 32, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 14, 1),
        add_highlight_range (HL_DELIMITER, 31, 1),
        add_highlight_range (HL_OPERATOR, 12, 1),
        add_highlight_range (HL_OPERATOR, 28, 1),
        add_highlight_range (HL_OPERATOR, 20, 1),
        add_highlight_range (HL_LITERAL, 30, 1),
    }}
},
{"ubyte[] tsv = \"\"\"",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_OPERATOR, 12, 1),
        add_highlight_range (HL_LITERAL_STRING, 14, 3),
    }}
},
{"ubyte[]& a = addition.value;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 27, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 21, 1),
        add_highlight_range (HL_OPERATOR, 11, 1),
    }}
},
{"ubyte[]& a_value = a.value;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 26, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 20, 1),
        add_highlight_range (HL_OPERATOR, 17, 1),
    }}
},
{"ubyte[]& b_value = b.value;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 26, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 6, 1),
        add_highlight_range (HL_DELIMITER, 20, 1),
        add_highlight_range (HL_OPERATOR, 17, 1),
    }}
},
{"uword full_base1b_digits = #base10/9;",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "DIV", "[*0x8]+0x8", "9", "[0x40]",},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 36, 1),
        add_highlight_range (HL_OPERATOR, 25, 1),
        add_highlight_range (HL_OPERATOR, 34, 1),
        add_highlight_range (HL_OPERATOR, 27, 1),
        add_highlight_range (HL_LITERAL, 35, 1),
    }}
},
{"uword heading_base1b_digits = #base10%9;",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "REM", "[*0x8]+0x8", "9", "[0x38]",},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 39, 1),
        add_highlight_range (HL_OPERATOR, 28, 1),
        add_highlight_range (HL_OPERATOR, 37, 1),
        add_highlight_range (HL_OPERATOR, 30, 1),
        add_highlight_range (HL_LITERAL, 38, 1),
    }}
},
{"uword total_base1b_digits = (#base10 + 8)/9;",
    {{
        {true, 0, "PUSH", "0", "", "",},
        {true, 0, "ADD", "[*0x8]+0x8", "8", "[0x48]",},
        {true, 0, "DIV", "[0x48]", "9", "[0x48]",},
    }, {
        add_highlight_range (HL_TYPE, 0, 5),
        add_highlight_range (HL_DELIMITER, 43, 1),
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_OPERATOR, 26, 1),
        add_highlight_range (HL_OPERATOR, 37, 1),
        add_highlight_range (HL_OPERATOR, 41, 1),
        add_highlight_range (HL_OPERATOR, 29, 1),
        add_highlight_range (HL_LITERAL, 39, 1),
        add_highlight_range (HL_LITERAL, 42, 1),
    }}
},
{"value = {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 8, 1),
        add_highlight_range (HL_OPERATOR, 6, 1),
    }}
},
{"value.resize (#a + 1);",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 21, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_DELIMITER, 20, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_OPERATOR, 17, 1),
        add_highlight_range (HL_OPERATOR, 14, 1),
        add_highlight_range (HL_LITERAL, 19, 1),
    }}
},
{"value[i] = carry;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 7, 1),
        add_highlight_range (HL_OPERATOR, 9, 1),
    }}
},
{"value[value_i] := sum & 0xff;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 28, 1),
        add_highlight_range (HL_DELIMITER, 5, 1),
        add_highlight_range (HL_DELIMITER, 13, 1),
        add_highlight_range (HL_OPERATOR, 15, 2),
        add_highlight_range (HL_OPERATOR, 16, 1),
        add_highlight_range (HL_LITERAL, 24, 4),
    }}
},
{"word sum = value[value_i] + a[#a - 1 - i] + carry;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 4),
        add_highlight_range (HL_DELIMITER, 49, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 29, 1),
        add_highlight_range (HL_DELIMITER, 24, 1),
        add_highlight_range (HL_DELIMITER, 40, 1),
        add_highlight_range (HL_OPERATOR, 9, 1),
        add_highlight_range (HL_OPERATOR, 26, 1),
        add_highlight_range (HL_OPERATOR, 42, 1),
        add_highlight_range (HL_OPERATOR, 33, 1),
        add_highlight_range (HL_OPERATOR, 37, 1),
        add_highlight_range (HL_OPERATOR, 30, 1),
        add_highlight_range (HL_LITERAL, 35, 1),
    }}
},
{"word sum = value[value_i] + carry;",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_TYPE, 0, 4),
        add_highlight_range (HL_DELIMITER, 33, 1),
        add_highlight_range (HL_DELIMITER, 16, 1),
        add_highlight_range (HL_DELIMITER, 24, 1),
        add_highlight_range (HL_OPERATOR, 9, 1),
        add_highlight_range (HL_OPERATOR, 26, 1),
    }}
},
{"{",
    {{
    }, {
        add_highlight_range (HL_DELIMITER, 0, 1),
    }}
},
{"}",
    {{
    }, {
        add_highlight_range (HL_DELIMITER, 0, 1),
    }}
},
{"} else {",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_KEYWORD, 2, 4),
        add_highlight_range (HL_DELIMITER, 7, 1),
        add_highlight_range (HL_DELIMITER, 0, 1),
    }}
},
{"},",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 1, 1),
        add_highlight_range (HL_DELIMITER, 0, 1),
    }}
},
{"};",
    {{
        {true, 0, "______SET_64", "REG_M", "81", ""},
    }, {
        add_highlight_range (HL_DELIMITER, 1, 1),
        add_highlight_range (HL_DELIMITER, 0, 1),
    }}
},
{"}⁠",
    {{
        {false, 0, "LOOP_END", "", "", "",},
    }, {
        add_highlight_range (HL_DELIMITER, 0, 1),
    }}
},
{"}⁠⁠",
    {{
        {false, 0, "ENDIF", "", "", "",},
    }, {
        add_highlight_range (HL_DELIMITER, 0, 1),
    }}
},
{"",
    {{
    }}
},
