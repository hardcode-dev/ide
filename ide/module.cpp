#include "module.h"


Module::Module (hc_module_t* hc_module)
{
    hc_module_source_t* source = hc_module_source (hc_module);
    if (source->_class.len)
        _class = QString::fromUtf8 ((const char*) source->_class.data, source->_class.len);
    if (source->function_return_type.len) {
        function_return_type = QString::fromUtf8 ((const char*) source->function_return_type.data, source->function_return_type.len);
        function_parameters = QString::fromUtf8 ((const char*) source->function_parameters.data, source->function_parameters.len);
        function_body = QString::fromUtf8 ((const char*) source->function_body.data, source->function_body.len);
        function_class = QString::fromUtf8 ((const char*) source->function_class.data, source->function_class.len);
    }
}
Module::Module ()
{
}
