#ifndef MODULETREEWIDGETITEM_H
#define MODULETREEWIDGETITEM_H

#include <QEnterEvent>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QScrollArea>
#include <QTreeWidgetItem>
#include <QWidget>

class ModuleTreeWidgetItem : public QWidget
{
    Q_OBJECT
public:
    explicit ModuleTreeWidgetItem(const QString& file_name, QTreeWidgetItem* this_item,
                                  const QColor& c_under = QColor(47,140,198), const QColor& c_current = QColor(47,140,198),
                                  const QColor& c_default = QColor(255,255,255), const QString& bc_current = "background-color: rgb(47,140,198);",
                                  const QString& bc_under = "background-color: rgb(47,140,198);", const QString& bc_default = "background-color: rgb(255,255,255);", QWidget *parent = nullptr);
    void setButtonVisibility(bool visible);
    void resizeLabel(int size);
    int getLabelSize();
    int getButtonsWidgetWidth();
    void changeCurrentItem(bool is_current);

protected:
    virtual void enterEvent(QEvent *event);
    virtual void leaveEvent(QEvent *event);

public slots:
    void addFunction();
    void editFunction();
    void deleteFunction();

signals:
    void addModule(QTreeWidgetItem* item);
    void deleteModule(QTreeWidgetItem* item);
    void editModule(QTreeWidgetItem* item);

private:
    int label_size;
    QScrollArea* scroll;
    QWidget* frame;
    QLabel* label;
    QWidget* widget_buttons;
    QPushButton* pb_add;
    QPushButton* pb_delete;
    QPushButton* pb_edit;
    bool is_current_item;
    QTreeWidgetItem* item;
    QColor color_under_mouse;
    QColor color_current_item;
    QColor color_default;
    QString color_background_under_mouse;
    QString color_background_under_current_item;
    QString color_background_default;


};

#endif // MODULETREEWIDGETITEM_H
