#include "moduletreewidgetitem.h"
#include "qdebug.h"

#include <QBoxLayout>
#include <QLabel>

ModuleTreeWidgetItem::ModuleTreeWidgetItem(const QString& file_name, QTreeWidgetItem* this_item,
                                           const QColor& c_under, const QColor& c_current,
                                           const QColor& c_default, const QString& bc_current,
                                           const QString& bc_under, const QString& bc_default, QWidget *parent)
    : QWidget{parent}
{
    QHBoxLayout* layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);

    color_under_mouse = c_under;
    color_current_item = c_current;
    color_default = c_default;
    color_background_under_mouse = bc_current;
    color_background_under_current_item = bc_under;
    color_background_default = bc_default;
    item = this_item;
    item->setBackground(0, QBrush(color_default));
    {
        is_current_item = false;
        {
            label = new QLabel(file_name);
            label->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
            label_size = label->fontMetrics().boundingRect(label->text()).size().width();
            label->setStyleSheet(color_background_default);
        }
        {
            scroll = new QScrollArea;
            scroll->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
            scroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
            scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
            scroll->setWidget(label);
            scroll->setStyleSheet(color_background_default);
            scroll->setMaximumHeight(label->fontMetrics().height());
            scroll->setFrameShape(QFrame::NoFrame);
            layout->addWidget(scroll, 0, Qt::AlignLeft);
        }
        widget_buttons = new QWidget;
        widget_buttons->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Fixed);
        QHBoxLayout* layout_buttons = new QHBoxLayout;
        widget_buttons->setLayout(layout_buttons);
        QSizePolicy sp_retain = widget_buttons->sizePolicy();
        sp_retain.setRetainSizeWhenHidden(true);
        widget_buttons->setSizePolicy(sp_retain);
        widget_buttons->setVisible(false);
        {
            pb_add = new QPushButton(QIcon(":/icons/add.png"), "");
            bool c = connect(pb_add, SIGNAL(clicked(bool)), this, SLOT(addFunction()));assert(c);
            pb_add->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
            pb_add->setVisible(true);
            pb_add->setStyleSheet("QPushButton { border: none; }");
            layout_buttons->addWidget(pb_add, 0, Qt::AlignLeft);
        }
        {
            pb_edit = new QPushButton(QIcon(":/icons/edit_item.png"), "");
            bool c = connect(pb_edit, SIGNAL(clicked(bool)), this, SLOT(editFunction()));assert(c);
            pb_edit->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
            pb_edit->setVisible(true);
            pb_edit->setStyleSheet("QPushButton { border: none; }");
            layout_buttons->addWidget(pb_edit, 0, Qt::AlignLeft);
        }
        {
            pb_delete = new QPushButton(QIcon::fromTheme(":/icons/delete.png"), "");
            bool c = connect(pb_delete, SIGNAL(clicked(bool)), this, SLOT(deleteFunction()));assert(c);
            pb_delete->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
            pb_delete->setVisible(true);
            pb_delete->setStyleSheet("QPushButton { border: none; }");
            layout_buttons->addWidget(pb_delete, 0, Qt::AlignLeft);
            //pb_delete->setStyleSheet( "*{border-image: url(:/icons/normal);} :hover{ border-image: url(:/icons/hover);}");
        }
        layout->addWidget(widget_buttons, 0, Qt::AlignLeft);
        layout_buttons->addStretch();
        layout->addStretch();
    }
    setLayout(layout);


}

void ModuleTreeWidgetItem::setButtonVisibility(bool visible) {
    widget_buttons->setVisible(visible);
    if (visible) {
        QColor color(color_under_mouse);
        item->setBackground(0, QBrush(color));
        scroll->setStyleSheet(color_background_under_mouse);
        label->setStyleSheet(color_background_under_mouse);
    } else {
        QColor color(color_default);
        item->setBackground(0, QBrush(color));
        scroll->setStyleSheet(color_background_default);
        label->setStyleSheet(color_background_default);
    }
}

void ModuleTreeWidgetItem::resizeLabel(int size) {
    if (size >= 50) {
        scroll->setFixedWidth(size);
    }
}

int ModuleTreeWidgetItem::getButtonsWidgetWidth() {
    return widget_buttons->width();
}

int ModuleTreeWidgetItem::getLabelSize(){
    return label_size;
}

void ModuleTreeWidgetItem::enterEvent(QEvent *event) {
    if (!is_current_item) {
        setButtonVisibility(true);
    }
}

void ModuleTreeWidgetItem::leaveEvent(QEvent *event) {
    if (!is_current_item) {
        setButtonVisibility(false);
    }
}

void ModuleTreeWidgetItem::changeCurrentItem(bool is_current) {
    is_current_item = is_current;
}

void ModuleTreeWidgetItem::addFunction() {
    emit addModule(item);
}

void ModuleTreeWidgetItem::editFunction() {
    emit editModule(item);
}

void ModuleTreeWidgetItem::deleteFunction() {
    emit deleteModule(item);
}
