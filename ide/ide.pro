QGRPC_CONFIG = client

QT += core gui widgets

CONFIG += c++17
CONFIG += qt
CONFIG += debug_and_release
#CONFIG += sanitizer sanitize_address

QMAKE_CXXFLAGS = -I../hc-inst/include
QMAKE_LIBS = -L../hc-inst/lib -lhcsnapshot -lhcmodule -lhcsynrpn -lhcsemrpn

SOURCES += \
    mainwindowprev.cpp \
    module.cpp \
    application.cpp \
    moduleeditpage.cpp \
    moduletabwidget.cpp \
    moduletreewidget.cpp \
    main.cpp \
    moduletreewidgetitem.cpp \
    standart-tree-item.cpp \
    widgets/function_analyzer.cpp \
    widgets/scrollboundeditor.cpp \
    widgets/hclangeditor.cpp


HEADERS += \
    mainwindowprev.h \
    module.h \
    application.h \
    moduleeditpage.h \
    moduletabwidget.h \
    moduletreewidget.h \
    moduletreewidgetitem.h \
    standart-tree-item.h \
    widgets/function_analyzer.h \
    widgets/scrollboundeditor.h \
    widgets/hclangeditor.h

RESOURCES += ide.qrc

MOC_DIR = .moc
OBJECTS_DIR = .obj
RCC_DIR = .qrc
