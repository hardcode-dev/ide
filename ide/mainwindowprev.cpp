#include "mainwindowprev.h"

#include "moduletreewidget.h"
#include "moduletabwidget.h"

#include <QHBoxLayout>
#include <QPushButton>
#include <QSplitter>
#include <QTextEdit>
#include <QLabel>
#include <QDebug>


MainWindow::MainWindow (QWidget* parent)
    : QWidget (parent)
{
    QVBoxLayout* layout = new QVBoxLayout;
    {
        QHBoxLayout* top_bar_layout = new QHBoxLayout;
        {
            QPushButton* button = new QPushButton ("TSV parsing demo", this);
            button->setFlat (true);
            top_bar_layout->addWidget (button);
        }
        {
            QLabel* label = new QLabel (">>", this);
            label->setTextFormat (Qt::PlainText);
            top_bar_layout->addWidget (label);
        }
        {
            QPushButton* button = new QPushButton ("Branch #0 \"master\"", this);
            button->setFlat (true);
            top_bar_layout->addWidget (button);
        }
        top_bar_layout->addStretch (1);
        {
            QPushButton* button = new QPushButton (this);
            button->setFlat (true);
            button->setIcon (QIcon (":/icons/profile.png"));
            top_bar_layout->addWidget (button);
        }
        layout->addLayout (top_bar_layout);
    }
    {
        QSplitter* splitter = new QSplitter (this);
        {
            module_tree_widget = new ModuleTreeWidget ();
            connect (module_tree_widget, SIGNAL (moduleActivated (const QStringList&, const QSharedPointer<Module>&)),
                     this, SLOT (editModule (const QStringList&, const QSharedPointer<Module>&)));
            splitter->addWidget (module_tree_widget);
        }

        {
            module_tab_widget = new ModuleTabWidget;
            connect (module_tab_widget, SIGNAL (followModuleRequested (const QStringList&)), module_tree_widget, SLOT (tryEditModule (const QStringList&)));
            splitter->addWidget (module_tab_widget);
        }

        connect (module_tab_widget, SIGNAL (lastClosed ()), module_tree_widget, SLOT (setFocus ()));

        layout->addWidget (splitter, 1);
    }
    setLayout (layout);
    module_tree_widget->setFocus ();
}

void MainWindow::editModule (const QStringList& full_name, const QSharedPointer<Module>& module)
{
    if (module)
        module_tab_widget->editModule (full_name, *module);
}
void MainWindow::loadSnapshot (hc_snapshot_t* snapshot)
{
    module_tree_widget->loadSnapshot (snapshot);
}
