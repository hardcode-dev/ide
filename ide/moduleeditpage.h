#pragma once

#include "module.h"

#include <QWidget>
#include <memory>

class HCLangEditor;
class FunctionAnalyzer;

class ModuleEditPage: public QWidget
{
    Q_OBJECT

public:
    ModuleEditPage (const QStringList& module_name, const Module& module, QWidget* parent = nullptr);
    ModuleEditPage (QWidget* parent = nullptr);
    ~ModuleEditPage ();

signals:
    void discardRequested ();
    void followModuleRequested (const QStringList& flat_name);

private slots:
    void tryFollowModule (const QString& flat_name);
    void toggleAnalysisWidgetVisibility ();

private:
    void build ();

private:
    std::shared_ptr<HCLangEditor> function_body_edit;
    std::shared_ptr<FunctionAnalyzer> function_analyzer;
    QStringList module_name;
    Module module;
    QWidget* analysis_widget;
};
