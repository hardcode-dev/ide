#pragma once

#include <QDialog>

class QLineEdit;


class LoginDialog: public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog (const QString& endpoint = "", const QString& login = "", const QString& password = "", QWidget *parent = nullptr);

signals:
    void loginRequested (const QString& endpoint, const QString& login, const QString& password);

private slots:
    void loginRequestCallback ();

private:
    QLineEdit* endpoint_line_edit;
    QLineEdit* login_line_edit;
    QLineEdit* password_line_edit;
};
