#pragma once

#include <QString>
#include <QVector>
#include <iostream>
#include <functional>
#include <sstream>
extern "C" {
#include "hc_syn_rpn.h"
};

namespace analyzed_code {
    typedef QString StringType;

    typedef int ComplexityValueNumeric;
    typedef ComplexityValueNumeric ComplexityValue;  // can be changed to polynomial / mathematical vector for undefined instruction complexity

    // based on reference-info/analysis-mock
    struct Instruction
    {
        bool at_line; // true: instruction corresponds to specified; false: instruction corresponds to a place inbetween specified line and a line right next to it
        int source_line;
        StringType instruction_type;
        StringType argument_a;
        StringType argument_b;
        StringType argument_c;
    };

    struct Alias
    {
        StringType name;
        int local_stack_address;
    };

    enum class NestingModifier
    {
        Unchanged = 0,
        IncreaseDepth = 1,
        DecreaseDepth = 2,
    };

    struct Complexity;
    struct FunctionVMInput
    {
        int function_stack_size = 0;
        QVector<Alias> aliases;
        QVector<Instruction> instructions;
        QSharedPointer<hc_syn_rpn_t> syn_rpn;
        QVector<Complexity> complexities = {};
        QVector<int> complexity_source_lines = {};  // Should we move it inside struct Complexity?

        FunctionVMInput() {}
        FunctionVMInput(int function_stack_size, QVector<Alias> aliases, QVector<Instruction> instructions, hc_syn_rpn_t* syn_rpn = nullptr) :
            function_stack_size(function_stack_size), aliases(aliases), instructions(instructions), syn_rpn(syn_rpn, &hc_syn_rpn_destroy) {}
        FunctionVMInput(int function_stack_size, QVector<Alias> aliases, QVector<Instruction> instructions, QSharedPointer<hc_syn_rpn_t> syn_rpn) :
            function_stack_size(function_stack_size), aliases(aliases), instructions(instructions), syn_rpn(syn_rpn) {}
    };

    struct ComplexityVector
    {
        ComplexityValueNumeric computational;
        ComplexityValueNumeric memory_transfer;

        inline ComplexityVector operator+(ComplexityVector const &other)
        {
            return {
                .computational = computational + other.computational,
                .memory_transfer = memory_transfer + other.memory_transfer,
            };
        }
    };

    inline ComplexityVector operator*(ComplexityVector const& self, ComplexityValueNumeric const &coef)
    {
        return {
            .computational = self.computational * coef,
            .memory_transfer = self.memory_transfer * coef,
        };
    }

    inline ComplexityVector operator*(ComplexityValueNumeric const &coef, ComplexityVector const &self)
    {
        return self * coef;
    }

    inline std::ostream& operator<<(std::ostream &out, ComplexityVector const &c)
    {
        if (c.computational && c.memory_transfer)
            return out << c.computational << " + " << c.memory_transfer << "mt";
        if (c.memory_transfer)
            return out << c.memory_transfer << "mt";
        if (c.computational)
            return out << c.computational;
        return out;
    }

    struct ComplexityMultiplier
    {
        StringType display_value;
        // Add evaluatable if needed
    };

    inline std::ostream& operator<<(std::ostream &out, ComplexityMultiplier const &c)
    {
        return out << c.display_value.toStdString();
    }

    enum class ComplexityElementType
    {
        NORMAL,
        BLOCK_END,
        MAX_START,
        MAX_ALT,
    };

    struct Complexity
    {
        inline Complexity() {}
        inline Complexity(const ComplexityVector& vec) : vector(vec) {}
        inline Complexity(const ComplexityMultiplier& mul) : has_multiplier(true), is_block_start(true), multiplier(mul) {}
        inline Complexity(const ComplexityMultiplier& mul, const ComplexityVector& vec) : has_multiplier(true), vector(vec), multiplier(mul) {}
        inline Complexity(const StringType& mul) : has_multiplier(true), is_block_start(true), multiplier({mul}) {}
        inline Complexity(const StringType& mul, const ComplexityVector& vec) : has_multiplier(true), vector(vec), multiplier({mul}) {}
        inline Complexity(const ComplexityElementType& eltype) : type(eltype) {}
        ComplexityElementType type = ComplexityElementType::NORMAL;
        bool has_multiplier = false, is_block_start = false;
        ComplexityVector vector;
        ComplexityMultiplier multiplier;

        inline StringType to_string() const;
        inline bool does_decrease_nesting() const
        {
            switch (type) {
            case ComplexityElementType::BLOCK_END:
            case ComplexityElementType::MAX_ALT:
                return true;
            default:
                return false;
            }
        }
        inline bool does_increase_nesting() const
        {
            switch (type) {
            case ComplexityElementType::MAX_START:
            case ComplexityElementType::MAX_ALT:
                return true;
            case ComplexityElementType::BLOCK_END:
                return false;
            default:
                return is_block_start;
            }
        }
    };

    inline std::ostream& operator<<(std::ostream &out, Complexity const &c)
    {
        switch(c.type) {
        case ComplexityElementType::BLOCK_END:
            return out << "}";
        case ComplexityElementType::MAX_START:
            return out << "max {";
        case ComplexityElementType::MAX_ALT:
            return out << "} or {";
        default:
            if (c.has_multiplier)
                out << c.multiplier << " * ";
            if (c.is_block_start)
                return out << "{";
            else
                return out << c.vector;
        }
    }

    inline StringType Complexity::to_string() const
    {
        std::ostringstream s{};
        s << *this;
        // return s.str();
        return QString::fromStdString(s.str());
    }

    typedef std::function<QVector<Complexity> (const Instruction&)> InstructionComplexityGetter;  // Consider returning std::shared_ptr<Complexity> or Complexity& to avoid copies
    typedef std::function<NestingModifier (const Instruction&)> InstructionNestingModifierGetter;
}
