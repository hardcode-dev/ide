#include "hcideclient.h"

#include <grpcpp/grpcpp.h>


static void register_metatypes ()
{
    qRegisterMetaType<grpc::Status> ();
    qRegisterMetaType<QSharedPointer<google::protobuf::Message>> ();
}


HcIdeClient::HcIdeClient (const QString& endpoint)
{
    static std::once_flag metatype_call_once_flag;
    std::call_once (metatype_call_once_flag, register_metatypes);

    context = std::make_shared<grpc::ClientContext> ();
    channel = grpc::CreateChannel (endpoint.toStdString (), grpc::InsecureChannelCredentials ());
    authorization_stub = HardCodeStorage::Authorization::NewStub (channel);
    storage_stub = HardCodeStorage::Storage::NewStub (channel);
    repository_control_stub = HardCodeStorage::RepositoryControl::NewStub (channel);
}
HcIdeClient::~HcIdeClient ()
{
    context->TryCancel ();
    for (std::vector<std::pair<std::thread, std::shared_ptr<std::atomic<bool>>>>::iterator it = working_threads.begin (); it != working_threads.end (); ++it)
        it->first.join ();
}
