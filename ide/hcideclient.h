#pragma once

#include "service.grpc.pb.h"

#include <QObject>
#include <QSharedPointer>
#include <vector>
#include <thread>


Q_DECLARE_METATYPE (grpc::Status);
Q_DECLARE_METATYPE (QSharedPointer<google::protobuf::Message>);


class HcIdeClient: public QObject
{
    Q_OBJECT

public:
    HcIdeClient (const QString& endpoint);
    ~HcIdeClient ();

    template<typename Service, typename Request, typename Response, typename Method>
    void startRequest (Method method, const std::shared_ptr<Request> request, QObject *receiver, const char *member)
    {
        for (std::vector<std::pair<std::thread, std::shared_ptr<std::atomic<bool>>>>::iterator it = working_threads.begin (); it != working_threads.end (); ) {
            if (*it->second) {
                it->first.join ();
                it = working_threads.erase (it);
            } else {
                ++it;
            }
        }
        std::shared_ptr<typename Service::Stub> stub;
        if constexpr (std::is_same<Service, HardCodeStorage::Authorization> ()) {
            stub = authorization_stub;
        } else if constexpr (std::is_same<Service, HardCodeStorage::Storage> ()) {
            stub = storage_stub;
        } else if constexpr (std::is_same<Service, HardCodeStorage::RepositoryControl> ()) {
            stub = repository_control_stub;
        } else {
            return;
        }
        std::shared_ptr<std::atomic<bool>> finished = std::make_shared<std::atomic<bool>> (false);
        std::thread thread ([] (std::shared_ptr<std::atomic<bool>> finished, std::shared_ptr<grpc::ClientContext> context, std::shared_ptr<typename Service::Stub> stub,
                                Method method, const std::shared_ptr<Request> request, QObject *receiver, const char *member) {
            QSharedPointer<Response> response (new Response);
            grpc::Status status = (*stub.*method) (&*context, *request, &*response);
            QMetaObject::invokeMethod (receiver, member, Qt::QueuedConnection, Q_ARG (grpc::Status, status), Q_ARG (QSharedPointer<google::protobuf::Message>, response));
            *finished = true;
        }, finished, context, stub, method, request, receiver, member);
        working_threads.push_back ({std::move (thread), finished});
    }

private:
    std::shared_ptr<grpc::ClientContext> context;
    std::shared_ptr<grpc::Channel> channel;
    std::shared_ptr<HardCodeStorage::Authorization::Stub> authorization_stub;
    std::shared_ptr<HardCodeStorage::Storage::Stub> storage_stub;
    std::shared_ptr<HardCodeStorage::RepositoryControl::Stub> repository_control_stub;

    std::vector<std::pair<std::thread, std::shared_ptr<std::atomic<bool>>>> working_threads;
};
