#ifndef SNAPSHOTMANAGER_H
#define SNAPSHOTMANAGER_H

#include "entities.pb.h"

#include <vector>
#include <unordered_map>



struct Module
{
    Module() = default;

    Module(const std::string& new_path, const HardCodeStorage::ModuleContent& new_modification, bool new_created = true,
           bool new_deleted = false, const std::string& new_time = "");

    Module(const std::string& new_path,bool new_created = false,
           bool new_deleted = true, const std::string& new_time = "");

    void setContent(HardCodeStorage::ModuleContent& new_modification);

    void setProperties(const std::string& new_path, const HardCodeStorage::ModuleContent& new_modification,
                        bool new_created = true, bool new_deleted = false, const std::string& new_time = "");

    void setProperties(const std::string& new_path, bool new_created = false,
                        bool new_deleted = true, const std::string& new_time = "");


    std::string full_path;
    HardCodeStorage::ModuleContent content;
    bool deleted;
    bool created;
    std::string time_version;
};

class SnapshotManager
{
public:
    SnapshotManager() = default;

    void clear();

    bool changeSnapshot(const HardCodeStorage::Commit& commit);

    bool addModule(const HardCodeStorage::Modification& new_module);

    Module& operator[](const std::string& path);

    Module& atModule(const std::string& path);

    std::unordered_map<std::string, Module>::iterator begin();

    std::unordered_map<std::string, Module>::iterator end();

    void printModules();


private:
    std::unordered_map<std::string, Module> modules;
};

#endif // SNAPSHOTMANAGER_H
