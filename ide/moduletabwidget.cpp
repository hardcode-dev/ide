#include "moduletabwidget.h"

#include "moduleeditpage.h"


ModuleTabWidget::ModuleTabWidget (QWidget* parent)
    : QTabWidget (parent)
{
    setTabsClosable (true);
    connect (this, SIGNAL (tabCloseRequested (int)), this, SLOT (removePage (int)));
    hide ();
}

void ModuleTabWidget::editModule (const QStringList& full_name, const Module& module)
{
    ModuleEditPage* module_page = new ModuleEditPage (full_name, module);
    connect (module_page, SIGNAL (discardRequested ()), this, SLOT (removePage ()));
    connect (module_page, SIGNAL (followModuleRequested (const QStringList&)), this, SIGNAL (followModuleRequested (const QStringList&)));
    setCurrentIndex (addTab (module_page, full_name.join (".")));
    module_page->setFocus ();
    show ();
}
void ModuleTabWidget::removePage (int index)
{
    if (index < 0)
        return;
    QWidget* page = widget (index);
    removeTab (index);
    delete page;
    if (!count ()) {
        hide ();
        emit lastClosed ();
    }
}
void ModuleTabWidget::removePage ()
{
    QWidget* page = (QWidget*) sender ();
    if (!page)
        return;
    removePage (indexOf (page));
}
