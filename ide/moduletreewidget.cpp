#include "moduletreewidget.h"

#include "application.h"
#include "moduletreewidgetitem.h"
extern "C" {
#include "hc_module_map.h"
}

#include <QKeyEvent>
#include <QStack>
#include <QDebug>


ModuleTreeWidget::ModuleTreeWidget (QWidget* parent)
    : QTreeWidget (parent)
{
    //Application::setMonospaceFont (this);
    setExpandsOnDoubleClick (false);
    setColumnCount (1);
    setHeaderLabels ({"Module"});
    setIndentation(24);
    bool c = connect(this, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)), this, SLOT(itemChanged(QTreeWidgetItem*,QTreeWidgetItem*)));assert(c);
    c = connect (this, SIGNAL (activated (const QModelIndex&)), this, SLOT (moduleActivatedCallback (const QModelIndex&)));assert(c);
    c = connect(this, SIGNAL(itemExpanded(QTreeWidgetItem*)), this, SLOT(updateLabelsSize(QTreeWidgetItem*)));assert(c);
    c = connect(this, SIGNAL(itemCollapsed(QTreeWidgetItem*)), this, SLOT(updateLabelsSize(QTreeWidgetItem*)));assert(c);
    c = connect(this, SIGNAL(resizeLabel()), this, SLOT(onWidgetResize()));assert(c);
}

void ModuleTreeWidget::loadSnapshot (hc_snapshot_t* snapshot)
{
    hc_module_t* module = hc_snapshot_root (snapshot);
    QStringList module_name;
    ModuleCallbackUserData module_callback_user_data {
        ._this = this,
        .module_name = module_name,
    };

    int ret = hc_module_map_foreach (hc_module_children (module), hcModuleCallback, &module_callback_user_data);
}
int ModuleTreeWidget::hcModuleCallback (const char *name, hc_module_t *module, void *user_data)
{
    ModuleCallbackUserData* module_callback_user_data = (ModuleCallbackUserData*) user_data;

    QStringList& module_name = module_callback_user_data->module_name;
    module_callback_user_data->_this->addModule (module_name, name, QSharedPointer<Module> (new Module (module)));
    module_name << name;
    int ret = hc_module_map_foreach (hc_module_children (module), hcModuleCallback, user_data);
    module_name.removeLast ();
    return 0;
}
bool ModuleTreeWidget::addModule (const QStringList& parent, const QString& name, const QSharedPointer<Module>& module)
{
    QTreeWidgetItem* parent_item = nullptr;
    for (const QString& parent_element: parent) {
        QTreeWidgetItem* current_parent_item = parent_item ? parent_item : invisibleRootItem ();
        bool found = false;
        for (int i = 0; i < current_parent_item->childCount (); ++i) {
            QTreeWidgetItem* item = current_parent_item->child (i);
            QString item_module_name = item->data (0, int (ModuleField::Name)).toString ();
            if (item_module_name == parent_element) {
                parent_item = item;
                found = true;
                break;
            }
        }
        if (!found)
            return false;
    }

    if (!parent_item)
        parent_item = invisibleRootItem ();

    QTreeWidgetItem* item = new QTreeWidgetItem ();
    item->setData (0, int (ModuleField::Name), QVariant (name));
    QStringList full_name = parent;
    full_name << name;
    item->setData (0, int (ModuleField::FullName), QVariant (full_name));
    item->setData (0, int (ModuleField::Content), QVariant::fromValue (module));
    parent_item->insertChild (0, item);
    parent_item->sortChildren (0, Qt::AscendingOrder);

    ModuleTreeWidgetItem* wid = new ModuleTreeWidgetItem(name, item,
                                                          current_theme_color_item, current_theme_color_item,
                                                          current_theme_color_standart, current_theme_color_background_item,
                                                          current_theme_color_background_item, current_theme_color_background_standart);
    bool c = connect(wid, SIGNAL(addModule(QTreeWidgetItem*)), this, SLOT(addModule(QTreeWidgetItem*)));assert(c);
    c = connect(wid, SIGNAL(editModule(QTreeWidgetItem*)), this, SLOT(editModule(QTreeWidgetItem*)));assert(c);
    c = connect(wid, SIGNAL(deleteModule(QTreeWidgetItem*)), this, SLOT(deleteModule(QTreeWidgetItem*)));assert(c);
    setItemWidget(item, 0, wid);

    return true;
}
void ModuleTreeWidget::tryEditModule (const QStringList& expected_full_name)
{
    QTreeWidgetItem* item = invisibleRootItem ();
    for (const QString& name_element: expected_full_name) {
        bool found = false;
        for (int i = 0; i < item->childCount (); ++i) {
            QTreeWidgetItem* child = item->child (i);
            QString item_module_name = child->data (0, int (ModuleField::Name)).toString ();
            if (item_module_name == name_element) {
                item = child;
                found = true;
                break;
            }
        }
        if (!found)
            return;
    }

    const QStringList full_name = item->data (0, int (ModuleField::FullName)).value<QStringList> ();
    const QSharedPointer<Module> module = item->data (0, int (ModuleField::Content)).value<QSharedPointer<Module>> ();
    emit moduleActivated (full_name, module);
}
void ModuleTreeWidget::keyPressEvent (QKeyEvent *event)
{
    QModelIndex current_index = currentIndex ();
    switch (event->key ()) {
    case Qt::Key_QuoteLeft: {
        collapseAll ();
    } break;
    case Qt::Key_1: {
        collapseAllAt (current_index);
    } break;
    case Qt::Key_2: {
        expandOneLevelAt (current_index);
    } break;
    case Qt::Key_3: {
        expandAllAt (current_index);
    } break;
    case Qt::Key_4: {
        expandAll ();
    } break;
    case Qt::Key_Enter: {
        QTreeWidgetItem* item = itemFromIndex (current_index);
        if (item)
            emit itemActivated (item, 0);
    } break;
    }
    QTreeWidget::keyPressEvent (event);
}
void ModuleTreeWidget::moduleActivatedCallback (const QModelIndex& index)
{
    QTreeWidgetItem* item = itemFromIndex (index);
    if (!item)
        return;
    const QStringList full_name = item->data (0, int (ModuleField::FullName)).value<QStringList> ();
    const QSharedPointer<Module> module = item->data (0, int (ModuleField::Content)).value<QSharedPointer<Module>> ();
    emit moduleActivated (full_name, module);
}
void ModuleTreeWidget::collapseAll ()
{
    QStack<QTreeWidgetItem*> items;
    for (int i = 0; i < topLevelItemCount (); ++i) {
        QTreeWidgetItem* item = topLevelItem (i);
        items.push (item);
    }
    while (!items.empty ()) {
        QTreeWidgetItem* parent_item = items.pop ();
        collapseItem (parent_item);
        for (int i = 0; i < parent_item->childCount (); ++i) {
            QTreeWidgetItem* item = parent_item->child (i);
            items.push (item);
        }
    }
}
void ModuleTreeWidget::collapseAllAt (const QModelIndex &index)
{
    QTreeWidgetItem* item = itemFromIndex (index);
    if (!item)
        return;
    QStack<QTreeWidgetItem*> items;
    items.push (item);
    while (!items.empty ()) {
        QTreeWidgetItem* parent_item = items.pop ();
        collapseItem (parent_item);
        for (int i = 0; i < parent_item->childCount (); ++i) {
            QTreeWidgetItem* item = parent_item->child (i);
            items.push (item);
        }
    }
}
void ModuleTreeWidget::expandOneLevelAt (const QModelIndex &index)
{
    QTreeWidgetItem* parent_item = itemFromIndex (index);
    if (!parent_item)
        return;
    expandItem (parent_item);
    QStack<QTreeWidgetItem*> items;
    for (int i = 0; i < parent_item->childCount (); ++i) {
        QTreeWidgetItem* item = parent_item->child (i);
        items.push (item);
    }
    while (!items.empty ()) {
        QTreeWidgetItem* parent_item = items.pop ();
        collapseItem (parent_item);
        for (int i = 0; i < parent_item->childCount (); ++i) {
            QTreeWidgetItem* item = parent_item->child (i);
            items.push (item);
        }
    }
}
void ModuleTreeWidget::expandAllAt (const QModelIndex &index)
{
    QTreeWidgetItem* item = itemFromIndex (index);
    if (!item)
        return;
    QStack<QTreeWidgetItem*> items;
    items.push (item);
    while (!items.empty ()) {
        QTreeWidgetItem* parent_item = items.pop ();
        expandItem (parent_item);
        for (int i = 0; i < parent_item->childCount (); ++i) {
            QTreeWidgetItem* item = parent_item->child (i);
            items.push (item);
        }
    }
}
void ModuleTreeWidget::expandAll ()
{
    QStack<QTreeWidgetItem*> items;
    for (int i = 0; i < topLevelItemCount (); ++i) {
        QTreeWidgetItem* item = topLevelItem (i);
        items.push (item);
    }
    while (!items.empty ()) {
        QTreeWidgetItem* parent_item = items.pop ();
        expandItem (parent_item);
        for (int i = 0; i < parent_item->childCount (); ++i) {
            QTreeWidgetItem* item = parent_item->child (i);
            items.push (item);
        }
    }
}

void ModuleTreeWidget::resizeEvent(QResizeEvent *event) {
     QTreeWidget::resizeEvent (event);
     emit(resizeLabel());
}

void ModuleTreeWidget::itemChanged(QTreeWidgetItem* cur,QTreeWidgetItem* prev)
{
    if (prev) {
        ModuleTreeWidgetItem* pr = qobject_cast<ModuleTreeWidgetItem*>(this->itemWidget(prev, 0));
        if (pr) {
            pr->changeCurrentItem(false);
            pr->setButtonVisibility(false);
        }
    }
    if (cur) {
        ModuleTreeWidgetItem* cr = qobject_cast<ModuleTreeWidgetItem*>(this->itemWidget(cur, 0));
        if (cr) {
            cr->changeCurrentItem(true);
            cr->setButtonVisibility(true);
        }
    }
}

void ModuleTreeWidget::addModule(QTreeWidgetItem* item) {
    qWarning() << "TODO add function";
}

void ModuleTreeWidget::editModule(QTreeWidgetItem* item) {
    const QStringList full_name = item->data (0, int (ModuleField::FullName)).value<QStringList> ();
    const QSharedPointer<Module> module = item->data (0, int (ModuleField::Content)).value<QSharedPointer<Module>> ();
    emit moduleActivated (full_name, module);
}

void ModuleTreeWidget::deleteModule(QTreeWidgetItem* item) {
    qWarning() << "TODO delete function";
}

void ModuleTreeWidget::updateAllLabel() {
    int max_size = 0;
    findMaxLabelSize(invisibleRootItem(), max_size, -1);
    resizeAllOpenItems(invisibleRootItem(), max_size, -1);
}

void ModuleTreeWidget::updateLabelsSize(QTreeWidgetItem* item){
    updateAllLabel();
}

void ModuleTreeWidget::onWidgetResize() {
    updateAllLabel();
}

void ModuleTreeWidget::resizeAllOpenItems(QTreeWidgetItem* item, int max_size, int level){
    if (item) {
        ModuleTreeWidgetItem* cur_item = qobject_cast<ModuleTreeWidgetItem*>(itemWidget(item, 0));
        if (cur_item) {
            cur_item->resizeLabel(max_size - (level * 24));
            if (item->isExpanded()) {
                for (int i = 0; i < item->childCount(); ++i) {
                    resizeAllOpenItems(item->child(i), max_size, level+1);
                }
            }
        }
        else {
            if (item->isExpanded() or item == invisibleRootItem()) {
                for (int i = 0; i < item->childCount(); ++i) {
                    resizeAllOpenItems(item->child(i), max_size, level+1);
                }
            }
        }
    }
}

void ModuleTreeWidget::findMaxLabelSize(QTreeWidgetItem* item, int& max_size, int level){
    if (item) {
        ModuleTreeWidgetItem* cur_item = qobject_cast<ModuleTreeWidgetItem*>(itemWidget(item, 0));
        if (cur_item) {
            if (cur_item->getLabelSize() + level * 24 > max_size) {
                max_size = cur_item->getLabelSize() + level * 24;
                if (max_size > width() - cur_item->getButtonsWidgetWidth() - 48) {
                    max_size = width() - cur_item->getButtonsWidgetWidth() - 48;//cur_item->getButtonsWidgetWidth();
                }
            }
            if (item->isExpanded()) {
                for (int i = 0; i < item->childCount(); ++i) {
                      findMaxLabelSize(item->child(i), max_size, level + 1);
                }
            }
        } else {
            if (item->isExpanded() or item == invisibleRootItem()) {
                for (int i = 0; i < item->childCount(); ++i) {
                    findMaxLabelSize(item->child(i), max_size, level + 1);
                }
            }
        }
    }
}
