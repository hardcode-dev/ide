@function_return_type@
optional index (s)
@@

@function_parameters@
ubyte[]& s
@@

@function_body@
for (index (s) i asc 0, #s - 1)
    if (s[i] != 0)
        return i;
return null;
@@
