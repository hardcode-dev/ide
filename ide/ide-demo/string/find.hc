@function_return_type@
optional index (s)
@@

@function_parameters@
ubyte[]& s, ubyte& char, index (s)& first, index (s)& last
@@

@function_body@
for (index (s) i asc first, last)
    if (s[i] == char)
        return i;
return null;
@@
