@function_return_type@
ubyte[]
@@

@function_parameters@
ubyte[]& s, index (s)& first, index (s)& last
@@

@function_body@
ubyte[] ret = (last - first + 1);
for (index (ret) i asc 0, #ret - 1) {
    index (ret) j = i + first;
    ret[i] = s[j];
}
return ret;
@@
