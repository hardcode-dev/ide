@function_return_type@
optional bill.head
@@

@function_parameters@
ubyte[]& line, index (line)& first, index (line)& last
@@

@function_body@
bill.head result;
mut index (line) off = must 0;
index (line) last = must #line - 1;
{
    index (line) sep_pos = must string.find (line, '\t', off, last);
    index (line) sub_last = must sep_pos - 1;
    result.title = string.substr (line, off, sub_last);
    off := must sep_pos + 1;
}
{
    index (line) sep_pos = must string.find (line, '\t', off, last);
    index (line) sub_last = must sep_pos - 1;
    result.count = string.substr (line, off, sub_last);
    off := must sep_pos + 1;
}
{
    let (index (line) i = string.find (line, '\t', off, last))
        return null;
    result.price = string.substr (line, off, last);
}
return result;
@@
