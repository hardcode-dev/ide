@function_return_type@
optional bill.entry
@@

@function_parameters@
ubyte[]& line, index (line)& first, index (line)& last
@@

@function_body@
bill.entry result;
mut index (line) off = must 0;
index (line) last = must #line - 1;
{
    index (line) sep_pos = must string.find (line, '\t', off, last);
    index (line) sub_last = must sep_pos - 1;
    result.title = string.substr (line, off, sub_last);
    off := must sep_pos + 1;
}
{
    index (line) sep_pos = must string.find (line, '\t', off, last);
    index (line) sub_last = must sep_pos - 1;
    result.count = must types.varuint.parse_base10 (line, off, sub_last);
    off := must sep_pos + 1;
}
{
    let (index (line) i = string.find (line, '\t', off, last))
        return null;
    result.price = must types.varuint.parse_base10 (line, off, last);
}
return result;
@@
