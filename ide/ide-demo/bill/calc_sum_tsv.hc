@function_return_type@
optional types.varuint
@@

@function_parameters@
ubyte[]& line
@@

@function_body@
mut types.varuint sum = types.varuint ();
mut index (line) off = must 0;
index (line) last = must #line - 1;
{
    let (index (line) sep_pos = string.find (line, '\n', off, last)) {
        bill.head head = must bill.parse_head (line, off, sep_pos);
        off := must sep_pos + 1;
    } else {
        // No entries
        bill.head head = must bill.parse_head (line, off, last);
        return sum;
    }
}
for (index (line) i asc off, last) {
    if (off >= #line)
        break;
    let (index (line) sep_pos = string.find (line, '\n', off, last)) {
        bill.entry entry = must bill.parse_entry (line, off, sep_pos - 1);
        sum.add (entry.price);
        let (off := sep_pos + 1) {
        } else {
            break;
        }
    } else {
        // Last entry
        bill.entry entry = must bill.parse_entry (line, off, last);
        sum.add (entry.price);
        break;
    }
}
return sum;
@@
