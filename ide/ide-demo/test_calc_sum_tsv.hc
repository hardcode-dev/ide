@function_return_type@
bool
@@

@function_parameters@
@@

@function_body@
ubyte[] tsv = """
Title\tCount\tPrice
Banana\t7\t8430983482049824309823409238271892731728909405007700359943812098098019382102938103928
Tuna can\t3\t124522182931028273492734921740912093810938103981209182039180328039289832938
Pineapple juice\t1\t14723904823048249023847877388388320100102093823922304982309428308023984230
Pacific oyster\t3\t54293840283444430930930940982034209384029841845734598733475776665323444309318731289312
""";

let (types.varuint sum = bill.calc_sum_tsv (tsv, #tsv, sum)) {
    types.varuint expected_value = {
        value = {
            0x20, 0x49, 0xb8, 0x06, 0xa0, 0xec, 0x3d, 0xc6, 0x77, 0x01, 0xe8, 0xe3,
            0xcd, 0x9b, 0x1d, 0x18, 0x08, 0x74, 0x62, 0x51, 0xd7, 0x3c, 0xb8, 0x13,
            0x6c, 0x5a, 0x69, 0x55, 0x73, 0xe3, 0x8e, 0x10, 0xb9, 0x0a, 0x09, 0xa8,
        },
    };

    return types.varuint.equals (sum, expected_value);
} else {
    return false;
}
@@
