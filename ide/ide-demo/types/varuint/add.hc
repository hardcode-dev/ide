@function_class@
mut method
@@

@function_return_type@
void
@@

@function_parameters@
types.varuint& addition
@@

@function_body@
ubyte[]& a = addition.value;
let (index (a) a_start = string.find_non_zero (a)) {
    if (#value < #a + 1)
        value.resize (#a + 1);
    long carry = 0;
    for (word:ranged (0, #a - 1) i asc 0, #a - 1) {
        let (index (value) value_i = #value - 1 - i) {
            word sum = value[value_i] + a[#a - 1 - i] + carry;
            carry = sum >> 8;
            value[value_i] := sum & 0xff;
        }
    }
    for (word i asc #a, #value - 2) {
        let (index (value) value_i = #value - 1 - i) {
            word sum = value[value_i] + carry;
            carry = sum >> 8;
            value[value_i] := sum & 0xff;
        }
    }
    let (index (value) i = 0)
        value[i] = carry;
}
@@
