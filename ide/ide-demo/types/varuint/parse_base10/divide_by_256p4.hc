@function_return_type@
long
@@

@function_parameters@
long[]& base10p9, index (base10p9)& first
@@

@function_body@
long carry = 0;
for (index (base10p9) i asc first, #base10p9 - 1) {
    long n = base10p9[i];
    n += carry*1000000000;
    carry = n & 0xffffffff;
    n >>= 32;
    base10p9[i] = n;
}⁠
return carry;
@@
