@function_return_type@
long
@@

@function_parameters@
ubyte[]& base10, index (base10)& first, index (base10)& last
@@

@function_body@
long n = 0;
for (index (base10) i asc first, last) {
    ubyte c = base10[i];
    if (c < '0' || c > '9')
        return -1;
    n = n*10 + (c - '0');
}
return n;
@@
