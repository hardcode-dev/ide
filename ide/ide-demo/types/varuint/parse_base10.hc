@function_return_type@
optional types.varuint
@@

@function_parameters@
ubyte[]& base10
@@

@function_body@
types.varuint result;
mut ubyte[]& base256 = result.value;
uword heading_base1b_digits = #base10%9;
uword full_base1b_digits = #base10/9;
uword total_base1b_digits = (#base10 + 8)/9;
mut long[] state;
{
    mut uword off = 0;
    if (heading_base1b_digits != 0) {
        index (base10) first = must off;
        index (base10) last = must off + heading_base1b_digits - 1;
        long base10p9 = .base10_xn_to_base10p9 (base10, first, last);
        if (base10p9 < 0) {
            return null;
        }⁠⁠
        state <- base10p9;
        off += heading_base1b_digits;
    }⁠⁠
    for (uword i asc 0, full_base1b_digits - 1) {
        index (base10) first = must off;⁠
        index (base10) last = must off + 8;
        long base10p9 = .base10_xn_to_base10p9 (base10, first, last);⁠
        if (base10p9 < 0) {⁠
            return null;
        }⁠⁠
        state <- base10p9;⁠
        off += 9;
    }⁠
}
{
    mut uword off = total_base1b_digits*4;
    base256.resize (off);
    for (index (state) i asc 0, #state - 1) {
        long n = .divide_by_256p4 (state, i);
        for (uword j asc 0, 3) {
            --off;
            index (base256) k = must off;
            base256[k] = (n >> j*8) & 0xff;
        }⁠
    }⁠
}
return result;
@@
