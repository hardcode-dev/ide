@function_return_type@
bool
@@

@function_parameters@
types.varuint& a, types.varuint& b
@@

@function_body@
ubyte[]& a_value = a.value;
ubyte[]& b_value = b.value;
let (index (a_value) a_first_nz = string.find_non_zero (a_value)) {
    let (index (b_value) b_first_nz = string.find_non_zero (b_value)) {
        let ((#a_value - a_first_nz) == (#b_value - b_first_nz)) {
            for (index (a_value) a_idx asc a_first_nz, #a_value - 1) {
                index (b_value) b_idx = a_idx - a_first_nz + b_first_nz;
                if (a_value[a_idx] != b_value[b_idx])
                    return false;
            }
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
} else {
    let (index (b_value) b_first_nz = string.find_non_zero (b_value)) {
        return false;
    } else {
        return true;
    }
}
@@
