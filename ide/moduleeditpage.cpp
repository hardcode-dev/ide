#include "moduleeditpage.h"

#include "application.h"
#include "widgets/hclangeditor.h"
#include "widgets/function_analyzer.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QTextEdit>
#include <QPlainTextEdit>
#include <QPushButton>


ModuleEditPage::ModuleEditPage (const QStringList& module_name, const Module& module, QWidget* parent)
    : QWidget (parent), module_name (module_name), module (module)
{
    build ();
}
ModuleEditPage::ModuleEditPage (QWidget* parent)
    : QWidget (parent)
{
    build ();
}
ModuleEditPage::~ModuleEditPage ()
{
    if (function_analyzer) {
        function_analyzer->stop ();
        function_analyzer->wait ();
    }
}

void ModuleEditPage::build ()
{
    QVBoxLayout* layout = new QVBoxLayout;
    {
        QHBoxLayout* hlayout = new QHBoxLayout;
        {
            QStringList module_name_escaped;
            for (const QString& module_name_element: module_name)
                module_name_escaped << module_name_element.toHtmlEscaped ();
            QLabel* label = new QLabel ("<b>" + module_name_escaped.join (".") + "</b>", this);
            Application::setMonospaceFont (label);
            hlayout->addWidget (label);
        }
        {
            QPushButton* button = new QPushButton (this);
            button->setIcon (QIcon (":/icons/edit.png"));
            button->setFlat (true);
            hlayout->addWidget (button);
        }
        hlayout->addStretch (1);
        layout->addLayout (hlayout);
    }
    if (module._class.size () || module.function_return_type.size ()) {
        if (module._class.size ()) {
            layout->addWidget (new QLabel ("<hr />", this));
            layout->addWidget (new QLabel ("Class:", this));
            {
                QPlainTextEdit* class_text_edit = new QPlainTextEdit (module._class, this);
                Application::setMonospaceFont (class_text_edit);
                layout->addWidget (class_text_edit, 1);
            }
        }
        if (module.function_return_type.size ()) {
            layout->addWidget (new QLabel ("<hr />", this));
            bool is_method = module.function_class == "method" || module.function_class == "mut method";
            QPushButton* analysis_button;
            {
                QHBoxLayout* hlayout = new QHBoxLayout;
                QLabel* label = new QLabel (is_method ? "Method:" : "Function:", this);
                hlayout->addWidget (label);
                hlayout->addStretch (1);
                QPushButton* button = new QPushButton (this);
                button->setIcon (QIcon (":/icons/analysis.png"));
                button->setFlat (true);
                hlayout->addWidget (button);
                layout->addLayout (hlayout);
                analysis_button = button;
            }
            {
                analysis_widget = new QWidget (this);
                {
                    QGridLayout* grid = new QGridLayout;
                    grid->addWidget (new QLabel ("Complexity:", this), 0, 0);
                    QLineEdit* complexity_line_edit = new QLineEdit (this);
                    Application::setMonospaceFont (complexity_line_edit);
                    complexity_line_edit->setReadOnly (true);
                    if (module_name.join (".") == "types.varuint.parse_base10.divide_by_256p4") {
                        complexity_line_edit->setText ("⦗13 + (#base10p9 - first)*36⦘ + ⦗11 + (#base10p9 - first)*19⦘mt");
                    }
                    grid->addWidget (complexity_line_edit, 0, 1);
                    grid->addWidget (new QLabel ("Ordnung:", this), 1, 0);
                    QLineEdit* ordnung_line_edit = new QLineEdit (this);
                    ordnung_line_edit->setReadOnly (true);
                    Application::setMonospaceFont (ordnung_line_edit);
                    if (module_name.join (".") == "types.varuint.parse_base10.divide_by_256p4") {
                        ordnung_line_edit->setText ("O ⦗#base10p9 - first⦘ + O ⦗#base10p9 - first⦘mt");
                    }
                    grid->addWidget (ordnung_line_edit, 1, 1);
                    analysis_widget->setLayout (grid);
                }
                connect (analysis_button, SIGNAL (clicked ()), this, SLOT (toggleAnalysisWidgetVisibility ()));
                analysis_widget->setVisible (false);
                layout->addWidget (analysis_widget);
            }
            {
                QHBoxLayout* hlayout = new QHBoxLayout;
                {
                    QLineEdit* line_edit = new QLineEdit (module.function_return_type, this);
                    Application::setMonospaceFont (line_edit);
                    hlayout->addWidget (line_edit, 1);
                }
                hlayout->addWidget (new QLabel ("(", this));
                {
                    QLineEdit* line_edit = new QLineEdit (module.function_parameters, this);
                    Application::setMonospaceFont (line_edit);
                    hlayout->addWidget (line_edit, 3);
                }
                hlayout->addWidget (new QLabel (")", this));
                if (module.function_class == "mut method") {
                    hlayout->addWidget (new QLabel ("mut", this));
                }
                layout->addLayout (hlayout);
            }
            {
                function_body_edit = std::shared_ptr<HCLangEditor> (new HCLangEditor (this));
                function_body_edit->set_code (module.function_body);

                const analyzed_code::StringType source_code_str = R"BANANA(word outer_iteration_count = 81;
mut word accumulator = 0;

for (word i: 0, outer_iteration_count - 1) {
    word x = i*2;
    accumulator += x;
    for desc (word j: 17, 5)
        accumulator += j;
}
)BANANA";

                QSettings settings ("hardcore-lang", "editor");
                function_body_edit->load_settings (settings);

                function_analyzer = std::shared_ptr<FunctionAnalyzer> (new FunctionAnalyzer (function_body_edit));
                function_analyzer->start ();
                function_analyzer->on_source_code_change();

                Application::setMonospaceFont (&*function_body_edit);

                connect (&*function_body_edit, SIGNAL (followModuleRequested (const QString&)), this, SLOT (tryFollowModule (const QString&)));
                layout->addWidget (&*function_body_edit, 1000000);
            }
        }
    } else {
        {
            QLabel* label = new QLabel ("<br /><i>Empty module</i><br />", this);
            layout->addWidget (label);
        }
        layout->addStretch (1);
    }
    {
        QHBoxLayout* hlayout = new QHBoxLayout;
        {
            QPushButton* button = new QPushButton ("&Discard", this);
            connect (button, SIGNAL (clicked ()), this, SIGNAL (discardRequested ()));
            hlayout->addWidget (button);
        }
        hlayout->addStretch (1);
        {
            QPushButton* button = new QPushButton ("&Save", this);
            hlayout->addWidget (button);
        }
        {
            QPushButton* button = new QPushButton ("A&ccept", this);
            hlayout->addWidget (button);
        }
        layout->addLayout (hlayout);
    }
    setLayout (layout);
}
void ModuleEditPage::tryFollowModule (const QString& flat_name)
{
    QStringList target_name;
    if (flat_name.size () && flat_name[0] == '.') {
        target_name = module_name;
        QStringList suffix = flat_name.mid (1).split (".");
        target_name << suffix;
    } else {
        target_name = flat_name.split (".");
    }
    emit followModuleRequested (target_name);
}
void ModuleEditPage::toggleAnalysisWidgetVisibility ()
{
    analysis_widget->setVisible (!analysis_widget->isVisible ());
}
