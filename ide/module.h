#pragma once

extern "C" {
#include "hc_module.h"
}

#include <QString>
#include <QSharedPointer>
#include <QObject>

struct Module
{
    Module (hc_module_t* hc_module);
    Module ();

    QString _class;
    QString function_class;
    QString function_return_type;
    QString function_parameters;
    QString function_body;
};

Q_DECLARE_METATYPE (QSharedPointer<Module>)
