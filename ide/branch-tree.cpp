#include "branch-tree.h"
#include "qdebug.h"

BranchNode::BranchNode(uint32_t branch_id, uint32_t commit_id) {
    this->branch_id = branch_id;
    this->parent_commit_id = commit_id;
}

std::shared_ptr<BranchNode> BranchTree::findBranch(uint32_t branch_id) {
    std::queue<std::shared_ptr<BranchNode>> q;
    q.push(root);
    while (!q.empty()) {
        std::shared_ptr<BranchNode> current(q.front());
        if (current->branch_id == branch_id) {
            return current;
        }
        q.pop();
        foreach (const std::shared_ptr<BranchNode>& branch, current->childs) {
            if (branch->branch_id == branch_id) {
                return current;
            }
            q.push(branch);
        }
    }
    return  std::shared_ptr<BranchNode>();
}

std::shared_ptr<BranchNode> BranchTree::findNodeWhileBuilding(uint32_t branch_id) {
    foreach (const std::shared_ptr<BranchNode>& branch, builded_nodes) {
        if (branch->branch_id == branch_id) {
            return branch;
        }
    }
    return  std::shared_ptr<BranchNode>();
}

BranchTree::BranchTree(const ::google::protobuf::RepeatedPtrField< ::HardCodeStorage::BranchIdWithParent >& branches) {
    buildTree(branches);
}

void BranchTree::buildTree(const ::google::protobuf::RepeatedPtrField< ::HardCodeStorage::BranchIdWithParent >& branches) {
    builded_nodes.clear();
    current = nullptr;
    root = nullptr;
    foreach(const HardCodeStorage::BranchIdWithParent& branch, branches){
        if (branch.has_parent()) {
            std::shared_ptr<BranchNode> parent = findNodeWhileBuilding(branch.parent().branch_id());
            if (parent) {
                std::shared_ptr<BranchNode> current (new BranchNode(branch.branch_id(), branch.parent().commit_id()));
                current->parent = parent;
                builded_nodes.push_back(current);
                parent->childs.push_back(current);
            } else {
                qWarning() << branch.branch_id() << " " << branch.parent().branch_id();
                parent = std::make_shared<BranchNode>(branch.parent().branch_id(), branch.parent().commit_id());
                std::shared_ptr<BranchNode> current (new BranchNode(branch.branch_id(), branch.parent().commit_id()));
                current->parent = parent;
                builded_nodes.push_back(current);
                parent->childs.push_back(current);
                builded_nodes.push_back(parent);
            }
        } else {
            if (builded_nodes.empty()) {
                std::shared_ptr<BranchNode> current (new BranchNode(branch.branch_id(), branch.parent().commit_id()));
                builded_nodes.push_back(current);
                root = current;
            }
            else {
                root = findNodeWhileBuilding(branch.branch_id());
            }
        }
    }
    current = root;
}

std::shared_ptr<BranchNode> BranchTree::getRoot() {
    return root;
}

std::shared_ptr<BranchNode> BranchTree::getCurrent() {
    return current;
}

std::weak_ptr<BranchNode> BranchTree::getParent() {
    return current->parent;
}

std::vector<std::shared_ptr<BranchNode>>& BranchTree::getChilds() {
    return current->childs;
}

bool BranchTree::goChildBranch(uint32_t branch_id) {
    foreach (std::shared_ptr<BranchNode> child, current->childs) {
        if (child->branch_id == branch_id) {
            current = child;
            return true;
        }
    }
    return false;
}

bool BranchTree::goParentBranch() {
    if (!current->parent.expired()) {
        current = current->parent.lock();
        return true;
    }
    return false;
}

bool BranchTree::goRootBranch() {
    if (root) {
        current = root;
        return true;
    }
    return false;
}

void BranchTree::setCurrentBranch(std::shared_ptr<BranchNode> new_current) {
    current = new_current;
}

void BranchTree::addChildForCurrent(uint32_t branch_id, uint32_t parent_commit) {
    std::shared_ptr<BranchNode> new_branch(new BranchNode(branch_id, parent_commit));
    new_branch->parent = current;
    current->childs.push_back(new_branch);

}
