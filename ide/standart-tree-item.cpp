#include "standart-tree-item.h"

StandartTreeItem::StandartTreeItem(QString txt, uint32_t font_size, bool set_bold, QColor color, QString new_type)
{
    QFont fnt = QFont();
    fnt.setBold(set_bold);
    fnt.setPointSize(font_size);

    this->setEditable(false);
    this->setForeground(color);
    this->setFont(fnt);
    this->setText(txt);

    type = new_type;
}
