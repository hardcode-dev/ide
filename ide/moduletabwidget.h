#pragma once

#include "module.h"

#include <QTabWidget>


class ModuleTabWidget: public QTabWidget
{
    Q_OBJECT

public:
    ModuleTabWidget (QWidget* parent = nullptr);

    void editModule (const QStringList& full_name, const Module& module);

signals:
    void lastClosed ();
    void followModuleRequested (const QStringList& full_name);

private slots:
    void removePage (int index);
    void removePage ();
};
