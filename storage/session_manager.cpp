#include "session_manager.h"

#include <mutex>
#include <time.h>
#include <mutex>


SessionManager::SessionManager (const std::unordered_map<std::string, std::string>& users)
    : users (users)
{
    token_generator.seed (time (NULL));
}

std::optional<std::string> SessionManager::Authorize (const std::string& login, const std::string& password)
{
    std::unique_lock lock (mutex);
    std::unordered_map<std::string, std::string>::const_iterator user_it = users.find (login);
    if (user_it == users.end () || user_it->second != password)
        return {};

    uint64_t token_uint64[2] = {
        token_generator (),
        token_generator (),
    };
    std::string token ((const char*) token_uint64, sizeof (token_uint64));
    sessions.insert ({token, std::make_shared<const Session> (login)});
    return token;
}
std::shared_ptr<const Session> SessionManager::FindSession (const std::string& token)
{
    std::shared_lock lock (mutex);
    std::unordered_map<std::string, std::shared_ptr<const Session>>::const_iterator it = sessions.find (token);
    if (it == sessions.end ())
        return {};
    return it->second;
}
