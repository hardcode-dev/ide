#pragma once

#include <memory>
#include <grpc++/grpc++.h>

#include "service.grpc.pb.h"

#include "session_manager.h"

class SessionManager;
class Storage;


class StorageServiceImpl final: public HardCodeStorage::Storage::Service
{
public:
    StorageServiceImpl (std::shared_ptr<SessionManager> session_manager, std::shared_ptr<Storage> storage);

    grpc::Status GetRepositoryList (grpc::ServerContext* context, const HardCodeStorage::GetRepositoryListRequest* request, HardCodeStorage::GetRepositoryListResponse* response) override;
    grpc::Status CreateRepository (grpc::ServerContext* context, const HardCodeStorage::CreateRepositoryRequest* request, HardCodeStorage::CreateRepositoryResponse* response) override;

private:
    std::shared_ptr<SessionManager> session_manager;
    std::shared_ptr<Storage> storage;
};
