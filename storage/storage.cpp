#include "storage.h"


Storage::Storage ()
{
    GetRepositoriesToServer();
}

void Storage::SortStringByNumbers(QStringList& list) {
    std::sort(list.begin(), list.end(), [](QString& s1, QString& s2){
        if (s1.size() == s2.size()) {
                return s1 < s2;
        }
        else {
            return s1.size() < s2.size();
        }
    });
}

void Storage::FilesError(const QString& text) {
    qWarning() << text;
}

void Storage::FilesError(const QString& text, int fd) {
    qWarning() << text;
    ::close(fd);
}

uint32_t Storage::GenerateRepositoryId(){
    static uint32_t rep_id = 0;
    return rep_id++;
}

bool Storage::CreateRepsoitoryDir(const std::string& name) {
    if (!QDir("Reps").exists()) {
        if(!QDir().mkdir("Reps")){
            FilesError("Reps create directory error");
            return false;
        }
    }
    if (!QDir("Reps/" + QString::fromStdString(name)).exists()) {
        if (QDir().mkdir("Reps/" + QString::fromStdString(name))) {
            return true;
        }
        FilesError("Repository create directory error");
    }
    return false;
}

bool Storage::CreateBranchDir(uint32_t rep_id, uint32_t parent_branch_id, uint32_t parent_commit_id, bool is_root) {
    HardCodeStorage::Repository* rep = GetRepositoryFromId(rep_id);
    if (rep){
        if (!QDir("Reps").exists()) {
            if (!QDir().mkdir("Reps")) {
                FilesError("Reps create directory error");
                return false;
            }
        }
        if (!QDir("Reps/" + QString::fromStdString(rep->name()) + "/" + QString::number(rep->branches_size() - 1)).exists()) {
            if (QDir().mkdir("Reps/" + QString::fromStdString(rep->name()) + "/" + QString::number(rep->branches_size() - 1))) {
                if (!is_root) {
                    HardCodeStorage::BranchWithCommit parent;
                    parent.set_branch_id(parent_branch_id);
                    parent.set_commit_id(parent_commit_id);
                    std::string s, path_to_dir = "Reps/" + rep->name() + "/" + std::to_string(rep->branches_size() - 1),
                            path_to_file = "Reps/" + rep->name() + "/" + std::to_string(rep->branches_size() - 1) + "/parent";
                    parent.SerializeToString(&s);

                    char path[PATH_MAX];
                    const char *cs = s.c_str(), *cpath_to_dir = path_to_dir.c_str(), *cpath_to_file = path_to_file.c_str();
                    int fd = open(cpath_to_dir, O_TMPFILE | O_RDWR, S_IRUSR | S_IWUSR);
                    if (fd < 0) {
                        FilesError("fd error", fd);
                        return false;
                    }
                    size_t size = strlen(cs);
                    int written = write(fd, cs, size);
                    if (written <= 0) {
                        FilesError("write error", fd);
                        return false;
                    }
                    if (snprintf(path, PATH_MAX,  "/proc/self/fd/%d", fd) <= 0) {
                        FilesError("snprintf error", fd);
                        return false;
                    }
                    if (linkat(AT_FDCWD, path, AT_FDCWD, cpath_to_file, AT_SYMLINK_FOLLOW) < 0) {
                        FilesError("linkat error", fd);
                        return false;
                    }
                    ::close(fd);
                }
                return true;
            }
        }
    }
    return false;
}

bool Storage::CreateCommitFile(uint32_t rep_id, uint32_t branch_id, std::string& serialize_modifications){
    for (HardCodeStorage::Repository& rep : repositories) {
        if (rep.id() == rep_id) {
            for (HardCodeStorage::Branch& branch : *rep.mutable_branches()) {
                if (branch.id() == branch_id) {
                    if (!QDir("Reps").exists()) {
                        if(!QDir().mkdir("Reps")) {
                            FilesError("Reps create directory error");
                            return false;
                        }
                    }
                    if (!QDir("Reps/" + QString::fromStdString(rep.name())).exists()) {
                        if (!QDir().mkdir("Reps/" + QString::fromStdString(rep.name()))) {
                            FilesError("Repositories create directory error");
                            return false;
                        }
                    }
                    if (!QDir("Reps/" + QString::fromStdString(rep.name()) + "/" + QString::number(branch.id())).exists()) {
                        if (!QDir().mkdir("Reps/" + QString::fromStdString(rep.name()) + "/" + QString::number(branch.id()))) {
                            FilesError("Branch create directory error");
                            return false;
                        }
                    }

                    std::string path_to_dir = "Reps/" + (rep.name()) + "/" + std::to_string(branch.id()),
                            path_to_file = "Reps/" + (rep.name()) + "/" + std::to_string(branch.id()) + "/" + std::to_string(branch.commits_size() - 1);

                    char path[PATH_MAX];
                    const char *cs = serialize_modifications.c_str(), *cpath_to_dir = path_to_dir.c_str(), *cpath_to_file = path_to_file.c_str();
                    int fd = open(cpath_to_dir, O_TMPFILE | O_RDWR, S_IRUSR | S_IWUSR);
                    if (fd < 0) {
                        FilesError("fd error", fd);
                        return false;
                    }
                    size_t size = strlen(cs);
                    int written = write(fd, cs, size);
                    if (written <= 0) {
                        FilesError("Write create directory error", fd);
                        return false;
                    }
                    if (snprintf(path, PATH_MAX,  "/proc/self/fd/%d", fd) <= 0) {
                        FilesError("sprintf error", fd);
                        return false;
                    }
                    if (linkat(AT_FDCWD, path, AT_FDCWD, cpath_to_file, AT_SYMLINK_FOLLOW) < 0) {
                        FilesError("linkat error", fd);
                        return false;
                    }
                    ::close(fd);
                    return true;

                }
            }
        }
    }
    return false;
}

uint32_t Storage::GenerateBranchId(uint32_t sz) {
    return ++sz;
}

void Storage::GetRepositoriesToServer() {
    std::vector<HardCodeStorage::Repository> reps;
    QDir reps_dir("Reps", "", QDir::Name, QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
    QStringList reps_list = reps_dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks,  QDir::Name);
    for (int i = 0; i < reps_list.count(); ++i) {
        QString rep_name("Reps/" + reps_list[i]);
//        qWarning() << rep_name;
        QDir branches_dir(rep_name, "", QDir::Time | QDir::Reversed, QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
        QStringList branches_list = branches_dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot | QDir::NoSymLinks);
        SortStringByNumbers(branches_list);
        std::vector<HardCodeStorage::Branch> branches;
        for (int j = 0; j < branches_list.count(); ++j) {
            QString branch_name(rep_name + "/" + branches_list[j]);
//            qWarning() << "\t\t" << branch_name;
            QDir commits_dir(branch_name, "", QDir::Time | QDir::Reversed, QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
            QStringList commits_list = commits_dir.entryList(QDir::Files | QDir::NoDotAndDotDot | QDir::NoSymLinks);
            SortStringByNumbers(commits_list);
            std::vector<HardCodeStorage::Commit> commits;
            HardCodeStorage::Branch b;
            //bool is_root = true;
            for (int z = 0; z < commits_list.count(); ++z) {
//                qWarning() << "\t\t\t\t" << commits_list[z];
                QString commit_name(branch_name + "/" + commits_list[z]);
                QFile f(commit_name);
                if (f.open(QIODevice::ReadOnly)) {
                    QDir com_path(commit_name);
                    if (com_path.dirName() == "parent") {
                        //is_root = false;
                        HardCodeStorage::BranchWithCommit* parent = b.mutable_parent();
                        QTextStream stream(&f);
                        parent->ParseFromString(stream.readAll().toStdString());
                    } else {
                        HardCodeStorage::Commit commit;
                        commit.set_id(com_path.dirName().toInt());
                        HardCodeStorage::Modification* a = commit.add_type();
                        QTextStream stream(&f);
                        a->ParseFromString(stream.readAll().toStdString());
                        commits.push_back(commit);
                    }
                }
            }
            b.set_id(branches_list[j].toInt());
            google::protobuf::RepeatedPtrField<HardCodeStorage::Commit> data(commits.begin(), commits.end());
            b.mutable_commits()->Swap(&data);
            branches.push_back(b);
        }
        HardCodeStorage::Repository rep;
        QDir rep_path(rep_name);
        rep.set_name(rep_path.dirName().toStdString());
        rep.set_id(GenerateRepositoryId());
        google::protobuf::RepeatedPtrField<HardCodeStorage::Branch> data(branches.begin(), branches.end());
        rep.mutable_branches()->Swap(&data);
        reps.push_back(rep);
    }
    this->repositories = reps;
}

HardCodeStorage::Branch* Storage::GetBranchFromId(const std::string& repository_name, uint32_t branch_id) {
    HardCodeStorage::Repository* rep = GetRepositoryFromName(repository_name);
    if (rep) {
        for (HardCodeStorage::Branch& branch : *rep->mutable_branches()) {
            if (branch.id() == branch_id) {
                return &branch;
            }
        }
    }
    return nullptr;
}

HardCodeStorage::Commit* Storage::GetCommitFromId(const std::string& repository_name, uint32_t branch_id, uint32_t commit_id) {
    HardCodeStorage::Branch* branch = GetBranchFromId(repository_name, branch_id);
    if (branch) {
        for (HardCodeStorage::Commit& commit : *branch->mutable_commits()) {
            if (commit.id() == commit_id) {
                return &commit;
            }
        }
    }
    return nullptr;
}

HardCodeStorage::Repository* Storage::GetRepositoryFromId(uint32_t id){
    for (HardCodeStorage::Repository& rep : repositories) {
        if (rep.id() == id) {
            return &rep;
        }
    }
    return nullptr;
}

HardCodeStorage::Repository* Storage::GetRepositoryFromName(const std::string& name){
    for (HardCodeStorage::Repository& rep : repositories) {
        if (rep.name() == name) {
            return &rep;
        }
    }
    return nullptr;
}
