#pragma once

#include <string>

struct Session
{
    Session (const std::string login)
        : login (login)
    {
    }

    std::string login;
};
