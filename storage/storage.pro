QGRPC_CONFIG = server

QT += core

CONFIG += c++17
CONFIG += qt
CONFIG += debug_and_release

GRPC += ../api/entities.proto
GRPC += ../api/service.proto

SOURCES += \
    main.cpp \
    session_manager.cpp \
    storage.cpp \
    authorization_service.cpp \
    storage_service.cpp \
    repository_control_service.cpp

HEADERS += \
    session_manager.h \
    storage.h \
    authorization_service.h \
    storage_service.h \
    repository_control_service.h

MOC_DIR = .moc
OBJECTS_DIR = .obj

INC_GRPC = ../qgrpc/grpc.pri
!include($${INC_GRPC}) {
    error("$$TARGET: File not found: $${INC_GRPC}")
}
