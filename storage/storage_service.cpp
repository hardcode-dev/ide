#include "storage_service.h"

#include "session_manager.h"
#include "storage.h"

#include <cassert>
#include <thread>


StorageServiceImpl::StorageServiceImpl (std::shared_ptr<SessionManager> session_manager, std::shared_ptr<Storage> storage)
    : session_manager (session_manager), storage (storage)
{
}

grpc::Status StorageServiceImpl::GetRepositoryList (grpc::ServerContext* context, const HardCodeStorage::GetRepositoryListRequest* request, HardCodeStorage::GetRepositoryListResponse* response)
{
    for (const HardCodeStorage::Repository& repository: storage->repositories)
        response->mutable_repositories ()->add_names (repository.name ());
    return grpc::Status::OK;
}
grpc::Status StorageServiceImpl::CreateRepository (grpc::ServerContext* context, const HardCodeStorage::CreateRepositoryRequest* request, HardCodeStorage::CreateRepositoryResponse* response)
{
    std::shared_ptr<const Session> session = session_manager->FindSession (request->token ().value ());
    if (!session) {
        response->mutable_status ()->mutable_error ()->set_message ("Session not found: please relogin");
        return grpc::Status::OK;
    }

    for (HardCodeStorage::Repository& repository: storage->repositories) {
        if (repository.name () == request->repository_name ()) {
            response->mutable_status ()->mutable_error ()->set_message ("Name already exists");
            return grpc::Status::OK;
        }
    }

    HardCodeStorage::Repository rep;
    rep.set_name (request->repository_name ()); // TODO: Validate name
    rep.set_id (storage->GenerateRepositoryId ());
    storage->CreateRepsoitoryDir (rep.name ());
    HardCodeStorage::Branch* branch = rep.add_branches ();
    branch->set_id (rep.branches_size () - 1);
    storage->repositories.push_back (rep);
    storage->CreateBranchDir (rep.id (), 0, 0, true);

    return grpc::Status::OK;
}
