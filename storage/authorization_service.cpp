#include "authorization_service.h"

#include "session_manager.h"

#include <cassert>
#include <thread>


AuthorizationServiceImpl::AuthorizationServiceImpl (std::shared_ptr<SessionManager> session_manager, std::shared_ptr<Storage> storage)
    : session_manager (session_manager), storage (storage)
{
}

grpc::Status AuthorizationServiceImpl::Authorize (grpc::ServerContext* context, const HardCodeStorage::AuthorizeRequest* request, HardCodeStorage::AuthorizeResponse* response)
{
    const HardCodeStorage::UserInfo& user_info = request->user_info (); // TODO: Check for message presence
    std::optional<std::string> session_token = session_manager->Authorize (user_info.login (), user_info.password ());
    if (!session_token.has_value ()) {
        response->mutable_error ()->set_message ("Invalid login or password");
        return grpc::Status::OK;
    }
    response->mutable_user_token ()->set_value (*session_token);
    return grpc::Status::OK;
}
