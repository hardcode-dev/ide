#pragma once

#include <unordered_map>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <QDirIterator>
#include <QtDebug>
#include <QDir>

#include "service.qgrpc.server.h"


class Storage
{
public:
    Storage ();

    bool AddRepository (const std::string& name);
    // TODO: Don't use protobuf structures
    HardCodeStorage::Commit* GetCommitFromId (const std::string& repsitory_name, uint32_t branch_id, uint32_t commit_id);
    HardCodeStorage::Branch* GetBranchFromId (const std::string& repository_name, uint32_t branch_id);
    HardCodeStorage::Repository* GetRepositoryFromId (uint32_t id);
    HardCodeStorage::Repository* GetRepositoryFromName (const std::string& name);

    void GetRepositoriesToServer ();
    bool CreateRepsoitoryDir (const std::string& rep_name);
    bool CreateBranchDir (uint32_t rep_id, uint32_t parent_branch_dir, uint32_t parent_commit_id, bool is_root);
    bool CreateCommitFile (uint32_t rep_id, uint32_t branch_id, std::string& serialize_modifications);

    void FilesError (const QString& text);
    void FilesError (const QString& text, int fd);

    uint32_t GenerateRepositoryId ();
    uint32_t GenerateBranchId (uint32_t sz);

    void SortStringByNumbers (QStringList& list);

//private:
    std::vector<HardCodeStorage::Repository> repositories;
};
