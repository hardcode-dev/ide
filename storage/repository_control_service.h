#pragma once

#include <memory>
#include <grpc++/grpc++.h>

#include "service.grpc.pb.h"

class SessionManager;
class Storage;


class RepositoryControlServiceImpl final: public HardCodeStorage::RepositoryControl::Service
{
public:
    RepositoryControlServiceImpl (std::shared_ptr<SessionManager> session_manager, std::shared_ptr<Storage> storage);

    grpc::Status GetBranchList (grpc::ServerContext* context, const HardCodeStorage::GetBranchListRequest* request, HardCodeStorage::GetBranchListResponse* response) override;
    grpc::Status CreateEmptyBranch (grpc::ServerContext* context, const HardCodeStorage::CreateEmptyBranchRequest* request, HardCodeStorage::CreateEmptyBranchResponse* response) override;
    grpc::Status AddCommit (grpc::ServerContext* context, const HardCodeStorage::AddCommitRequest* request, HardCodeStorage::AddCommitResponse* response) override;
    grpc::Status GetCommitList (grpc::ServerContext* context, const HardCodeStorage::GetCommitListRequest* request, HardCodeStorage::GetCommitListResponse* response) override;
    grpc::Status GetCommit (grpc::ServerContext* context, const HardCodeStorage::GetCommitRequest* request, HardCodeStorage::GetCommitResponse* response) override;

private:
    std::shared_ptr<SessionManager> session_manager;
    std::shared_ptr<Storage> storage;
};
