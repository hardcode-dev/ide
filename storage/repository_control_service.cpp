#include "repository_control_service.h"

#include "session_manager.h"
#include "storage.h"


RepositoryControlServiceImpl::RepositoryControlServiceImpl (std::shared_ptr<SessionManager> session_manager, std::shared_ptr<Storage> storage)
    : session_manager (session_manager), storage (storage)
{
}

grpc::Status RepositoryControlServiceImpl::GetBranchList (grpc::ServerContext* context, const HardCodeStorage::GetBranchListRequest* request, HardCodeStorage::GetBranchListResponse* response)
{
    std::shared_ptr<const Session> session = session_manager->FindSession (request->token ().value ());
    if (!session) {
        // TODO
        // response->mutable_status ()->mutable_error ()->set_message ("Can't authorize");
        // response->mutable_status ()->mutable_error ()->set_code (401);
        return grpc::Status::OK;
    }

    HardCodeStorage::Repository* rep = storage->GetRepositoryFromName (request->repository_name ());
    if (!rep) {
        // TODO
        // response->mutable_status ()->mutable_error ()->set_message ("Failed to find repository '" + request->repository_name () + "'");
        return grpc::Status::OK;
    }

    for (const HardCodeStorage::Branch& branch: rep->branches ()) {
        response->mutable_branches ()->add_branches ();
        HardCodeStorage::BranchIdWithParent* branch_info = response->mutable_branches ()->add_branches ();
        branch_info->set_branch_id (branch.id ());
        if (branch.has_parent ()) {
            HardCodeStorage::BranchWithCommit* parent = branch_info->mutable_parent ();
            parent->set_branch_id (branch.parent ().branch_id ());
            parent->set_commit_id (branch.parent ().commit_id ());
        }
    }

    return grpc::Status::OK;
}
grpc::Status RepositoryControlServiceImpl::CreateEmptyBranch (grpc::ServerContext* context, const HardCodeStorage::CreateEmptyBranchRequest* request, HardCodeStorage::CreateEmptyBranchResponse* response)
{
    std::shared_ptr<const Session> session = session_manager->FindSession (request->token ().value ());
    if (!session) {
        // TODO
        // response->mutable_status ()->mutable_error ()->set_message ("Session not found: please relogin");
        return grpc::Status::OK;
    }
    HardCodeStorage::Repository* rep = storage->GetRepositoryFromName (request->repository_name ());
    if (!rep) {
        // TODO
        // response->mutable_status ()->mutable_error ()->set_message ("Failed to find repository '" + request->repository_name () + "'");
        return grpc::Status::OK;
    }

    HardCodeStorage::Branch* new_branch = rep->add_branches ();
    new_branch->set_id (rep->branches_size () - 1);
    HardCodeStorage::BranchWithCommit* parent = new_branch->mutable_parent ();
    parent->set_branch_id (request->branch_commit ().branch_id ());
    parent->set_commit_id (request->branch_commit ().commit_id ());
    response->set_branch_id (new_branch->id ());
    storage->CreateBranchDir (rep->id (), request->branch_commit ().branch_id (), request->branch_commit ().commit_id (), false);

    return grpc::Status::OK;
}
grpc::Status RepositoryControlServiceImpl::AddCommit (grpc::ServerContext* context, const HardCodeStorage::AddCommitRequest* request, HardCodeStorage::AddCommitResponse* response)
{
    std::shared_ptr<const Session> session = session_manager->FindSession (request->token ().value ());
    if (!session) {
        response->mutable_error ()->set_message ("Can't authorize");
        return grpc::Status::OK;
    }

    for (HardCodeStorage::Repository& rep: storage->repositories) {
        if (request->repository_name () == rep.name ()) {
            for (HardCodeStorage::Branch& branch: *rep.mutable_branches ()) {
                if (branch.id() == request->branch_commit ().branch_id ()) {
                    HardCodeStorage::Commit* new_commit = branch.add_commits ();
                    new_commit->set_id (branch.commits_size ());
                    new_commit->mutable_type ()->CopyFrom (request->modifications ());
                    response->set_commit_id (new_commit->id ());
                    std::string s;
                    request->modifications (0).SerializeToString (&s);
                    storage->CreateCommitFile (rep.id (), branch.id (), s);
                }
            }
            break;
        }
    }

    return grpc::Status::OK;
}
grpc::Status RepositoryControlServiceImpl::GetCommitList (grpc::ServerContext* context, const HardCodeStorage::GetCommitListRequest* request, HardCodeStorage::GetCommitListResponse* response)
{
    std::shared_ptr<const Session> session = session_manager->FindSession (request->token ().value ());
    if (!session) {
        response->mutable_error ()->set_message ("Session not found: please relogin");
        return grpc::Status::OK;
    }

    HardCodeStorage::Branch* branch = storage->GetBranchFromId (request->repository_name(), request->branch_id ());
    if (!branch) {
        response->mutable_error ()->set_message ("Branch not found");
        return grpc::Status::OK;
    }

    if (!request->has_until_commit_id ())
        for (const HardCodeStorage::Commit& commit: branch->commits ()) {
            response->mutable_commit_ids ()->add_commit_ids (commit.id ());
    } else {
        uint32_t id = 0;
        for (const HardCodeStorage::Commit& commit = branch->commits (id); id <= request->until_commit_id ().commit_id (); ++id)
            response->mutable_commit_ids ()->add_commit_ids (branch->commits (id).id ());
    }
    response->set_branch_id (branch->id ());

    return grpc::Status::OK;
}
grpc::Status RepositoryControlServiceImpl::GetCommit (grpc::ServerContext* context, const HardCodeStorage::GetCommitRequest* request, HardCodeStorage::GetCommitResponse* response)
{
    std::shared_ptr<const Session> session = session_manager->FindSession (request->token ().value ());
    if (!session) {
        response->mutable_error ()->set_message ("Session not found: please relogin");
        return grpc::Status::OK;
    }

    HardCodeStorage::Commit* commit = storage->GetCommitFromId (request->repository_name (), request->branch_with_commit ().branch_id (),
                                                                request->branch_with_commit ().commit_id ());
    if (!commit) {
        response->mutable_error ()->set_message ("Commit not found");
        return grpc::Status::OK;
    }

    HardCodeStorage::Commit* res_commit = response->mutable_commit ();
    res_commit->CopyFrom (*commit);

    return grpc::Status::OK;
}
