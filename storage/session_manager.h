#pragma once

#include "session.h"

#include <memory>
#include <optional>
#include <unordered_map>
#include <random>
#include <shared_mutex>


class SessionManager
{
public:
    SessionManager (const std::unordered_map<std::string, std::string>& users);

    std::optional<std::string> Authorize (const std::string& login, const std::string& password);
    std::shared_ptr<const Session> FindSession (const std::string& token);

private:
    const std::unordered_map<std::string, std::string> users;
    std::shared_mutex mutex;
    std::mt19937_64 token_generator;
    std::unordered_map<std::string, std::shared_ptr<const Session>> sessions;
};
