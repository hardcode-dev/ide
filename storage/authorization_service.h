#pragma once

#include <memory>
#include <grpc++/grpc++.h>

#include "service.grpc.pb.h"

class SessionManager;
class Storage;


class AuthorizationServiceImpl final: public HardCodeStorage::Authorization::Service
{
public:
    AuthorizationServiceImpl (std::shared_ptr<SessionManager> session_manager, std::shared_ptr<Storage> storage);

    grpc::Status Authorize (grpc::ServerContext* context, const HardCodeStorage::AuthorizeRequest* request, HardCodeStorage::AuthorizeResponse* response) override;

private:
    std::shared_ptr<SessionManager> session_manager;
    std::shared_ptr<Storage> storage;
};
