#include "session_manager.h"
#include "storage.h"
#include "authorization_service.h"
#include "storage_service.h"
#include "repository_control_service.h"


int main (int argc, char* argv[])
{
    std::unordered_map<std::string, std::string> users = {
        {"faster", "fdsavcxz"},
    };
    std::shared_ptr<SessionManager> session_manager = std::make_shared<SessionManager> (users);
    std::shared_ptr<Storage> storage = std::make_shared<Storage> ();

    AuthorizationServiceImpl authorization_service (session_manager, storage);
    StorageServiceImpl storage_service (session_manager, storage);
    RepositoryControlServiceImpl repository_control_service (session_manager, storage);

    grpc::ServerBuilder builder;
    builder.AddListeningPort (std::string ("127.0.0.1:50051"), grpc::InsecureServerCredentials());
    builder.RegisterService (&authorization_service);
    builder.RegisterService (&storage_service);
    builder.RegisterService (&repository_control_service);

    std::unique_ptr<grpc::Server> server = builder.BuildAndStart ();
    server->Wait ();

    return 0;
}
