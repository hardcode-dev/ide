IDE
===

## Download and build hc language

```bash
git submodule update --init --recursive
mkdir hc-inst && cd hc/hardcode && ./bootstrap && ./configure --prefix=`readlink -f ../../hc-inst` && make install
```

## Download and build gRPC and protoc

```bash
cd qgrpc/thirdparty && make
```

### Or use OS-wide installation

Make sure that `protoc` and `grpc_cpp_plugin` are installed into `/usr/bin`:

```bash
sudo apt install protobuf-compiler protobuf-compiler-grpc
```

And add argument `"CONFIG+=systemwide_grpc"` to further `qmake` invocations.

## Build source code storage (server)

```bash
cd storage && qmake && make
```

## Build IDE (client)

```bash
cd ide && qmake && make
```

## Running demo

```bash
./ide ide-demo
```

## Troubleshooting

If g++ shows a error like this:

```
/usr/include/c++/9/cstdlib:75:15: fatal error: stdlib.h: No such file or directory
   75 | #include_next <stdlib.h>
      |               ^~~~~~~~~~
compilation terminated.
```

Add argument `"QMAKE_CFLAGS_ISYSTEM=-I"` to further `qmake` invocations and rerun `qmake`.
